﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResolutionSettingsScript : MonoBehaviour
{
    private int width;
    private int height;
    private bool fullscreen;
    private int refreshrate;

    [SerializeField]
    private void SetResolution(int w, int h)
    {
        this.width = w;
        this.height = h;
    }

    [SerializeField]
    private void SetFullscreen(bool fullscreen)
    {
        this.fullscreen = fullscreen;
    }

    [SerializeField]
    private void SetrefreshRate(int refreashrate)
    {
        this.refreshrate = refreashrate;
    }

    [SerializeField]
    private void SetSettings()
    {
        Screen.SetResolution(width, height, fullscreen, refreshrate);
    }
}
