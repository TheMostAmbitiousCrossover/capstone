﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnergyBarSystem : MonoBehaviour
{

    private float maxEnergy;
    private float currentEnergy;
    private float regenPerSecond;
    //[SerializeField] private Text energyText;
    [SerializeField] private Slider energySlider;

    [SerializeField] private float energyRatio;

    public Image energyFillImg;
    public Color energyFillColor;
    public Image energyGlowImg;
    public Color energyGlowColor;
    public Image energyBackgroundImg;
    public Color energyBackgroundColor;
    public Color energyBackgroundGlowColor;

    private bool blinking;

    void Start()
    {
        energyFillImg.color = energyFillColor;
        energyRatio = (currentEnergy / maxEnergy);
        maxEnergy = 50f;
        currentEnergy = maxEnergy;
        energySlider.minValue = 0;
        regenPerSecond = 1f;
    }

    // to update the value of the UI slider in unity editor 
    private void UpdateEnergySlider()
    {
        energyRatio = (currentEnergy / maxEnergy);
        energySlider.maxValue = maxEnergy;
        energySlider.value = currentEnergy;
    }

    //  update the color of the background
    public void UpdateEnergyVisual()
    {
        if (!blinking)
        {
            energyBackgroundImg.color = energyBackgroundColor;
        }

        energyGlowImg.color = energyGlowColor;
        energyFillImg.color = energyFillColor;
        energyFillImg.fillAmount = energyRatio;
        energyGlowImg.fillAmount = energyRatio;
    }

    //  setting the glow
    public IEnumerator Glow(float blinkAmount, float timeBetweenFlash)
    {
        blinking = true;
        for (int i = 0; i < blinkAmount; i++)
        {
            if (energyBackgroundImg.color == energyBackgroundColor)
            {
                energyBackgroundImg.color = Color.Lerp(energyBackgroundImg.color, energyBackgroundGlowColor, 1);
            }
            else
            {
                energyBackgroundImg.color = Color.Lerp(energyBackgroundGlowColor, energyBackgroundColor, 1);
            }         
            yield return new WaitForSeconds(timeBetweenFlash);      
        }
        energyBackgroundImg.color = Color.Lerp(energyBackgroundGlowColor, energyBackgroundColor, 1);
        blinking = false;
    }

    //  to increase the maximum number of energy
    public void IncreaseMaxEnergy(float additionalEnergy)
    {
        maxEnergy += additionalEnergy;
    }

    //  increase a flat value of enerygy            ------ drinking potion?
    public void IncreaseEnergy(float energyRegen)
    {
        currentEnergy += energyRegen;

        if (currentEnergy > maxEnergy)
            currentEnergy = maxEnergy;
    }

    //  set the regen amount per delta time
    //  called in update for not with regenpersecond passing through 
    public void RegenEnergy(float energyRegen)
    {
        float energygain = energyRegen * Time.fixedDeltaTime;
        currentEnergy += energygain;

        if (currentEnergy > maxEnergy)
            currentEnergy = maxEnergy;
    }

    //  increase the regenpersecond value
    public void IncreaseRegenPerSecond(float regenValue)
    {
        regenPerSecond += regenValue;
    }

    //  decrease regenpersecond value
    public void DecreaseRegenPerSecond(float regenValue)
    {
        regenPerSecond -= regenValue;
    }

    //  set how much energy is used and check if you have enough energy
    public void DecreaseEnergy(float energyUsed)
    {
        if (energyUsed > currentEnergy)
        {
            StartCoroutine(Glow(4,0.05f));
        }
        else
        {
            currentEnergy -= energyUsed;           
        }
    }

    void FixedUpdate()
    {
        UpdateEnergySlider();
        UpdateEnergyVisual();
        RegenEnergy(regenPerSecond);
    }

}
