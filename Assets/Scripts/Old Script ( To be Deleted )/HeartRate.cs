﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeartRate : MonoBehaviour {
    [SerializeField] Transform HeartBeatTransform;
    [SerializeField] float heartRate;
    public float health;
    float mapHealthToBPM;

    public float waveScale;

    // Use this for initialization
    void Start () {
        health = 100; //get it from other script
        waveScale = 10f;
    }
  
    // Update is called once per frame
    void Update () {

        mapHealthToBPM = Remap(health,1,100,200,60);
        heartRate = mapHealthToBPM;

        HeartBeatTransform.localScale = new Vector3(Remap(heartRate, 200, 60, 4, waveScale), waveScale, waveScale);

        if (health <= 0)
        {
            health = 0;
            heartRate = 0; 
            //flatLine
           
            
        }
  
        if(health >= 100)
        {
            heartRate = 60;
            HeartBeatTransform.localScale = Vector3.one * waveScale;
        }

  
        
    }
    public float Remap(float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }
}
