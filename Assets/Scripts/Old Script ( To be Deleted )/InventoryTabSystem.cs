﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryTabSystem : MonoBehaviour
{
    //  canvas
    public Transform mainCanvas;
    public Transform nothingCanvas;
    public Transform charcterCanvas;
    public Transform inventoryCanvas;

    // for the Button
    public Button characterButton;
    public Button nothingButton;
    public Button inventoryButton;

    void Start()
    {
        ResetColor();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            
            InventoryTab();
            
        }
        else if (Input.GetKeyDown(KeyCode.P))
        {
            ResetColor();
            CharacterTab();
            SetButtonColor(characterButton);
        }
        else if (Input.GetKeyDown(KeyCode.Escape))
        {
            ResetColor();
            SettingTab();
            SetButtonColor(nothingButton);  
        }
    }

    //  check if the tab is active
    private bool CheckTab(Transform tab)
    {
        return tab.gameObject.activeInHierarchy == true;
    }

    //  reset the two selected canvas activity back to false
    private void ResetCanvas(Transform tab1, Transform tab2)
    {
        tab1.gameObject.SetActive(false);
        tab2.gameObject.SetActive(false);
    }

    //  check the main canvas to turn it off or to leave it on
    private void CheckMainCanvas()
    {
        bool inventory = CheckTab(inventoryCanvas);
        bool character = CheckTab(charcterCanvas);
        bool nothing = CheckTab(nothingCanvas);

        if (mainCanvas.gameObject.activeInHierarchy == false)
        {
            mainCanvas.gameObject.SetActive(true);
        }
        else if (inventory == false && character == false && nothing == false)
        {
            mainCanvas.gameObject.SetActive(false);
        }
    }

    //  for the inventory
    private void InventoryTab()
    {
        ResetColor();

        ResetCanvas(charcterCanvas, nothingCanvas);

        inventoryCanvas.gameObject.SetActive(inventoryCanvas.gameObject.activeInHierarchy == false);

        CheckMainCanvas();

        SetButtonColor(inventoryButton);
    }

    //  for the character
    private void CharacterTab()
    {
        ResetColor();

        ResetCanvas(inventoryCanvas, nothingCanvas);

        charcterCanvas.gameObject.SetActive(charcterCanvas.gameObject.activeInHierarchy == false);

        CheckMainCanvas();
    }

    //  for the settigns
    private void SettingTab()
    {
        ResetColor();

        ResetCanvas(inventoryCanvas, charcterCanvas);

        nothingCanvas.gameObject.SetActive(nothingCanvas.gameObject.activeInHierarchy == false);

        CheckMainCanvas();
    }

    //  reset the color of the buttons
    public void ResetColor()
    {
        ColorBlock characterColorBlock = characterButton.colors;
        ColorBlock inventoryColorBlock = inventoryButton.colors;
        ColorBlock settingColorBlock = nothingButton.colors;

        characterColorBlock.normalColor = Color.gray;
        inventoryColorBlock.normalColor = Color.gray;
        settingColorBlock.normalColor = Color.gray;

        characterButton.colors = characterColorBlock;
        inventoryButton.colors = inventoryColorBlock;
        nothingButton.colors = settingColorBlock;

    }

    //  set the color of the selected button
    public void SetButtonColor(Button selectedButton)
    {
        ColorBlock selectedColorBlock = selectedButton.colors;

        selectedColorBlock.normalColor = Color.white;
        selectedColorBlock.highlightedColor = Color.white;

        selectedButton.colors = selectedColorBlock;

    }

}
