﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthBarSystem : MonoBehaviour
{

    private float maxHealth;
    private float currentHealth;
    private float regenPerSecond;
    [SerializeField] private Text healthText;
    [SerializeField] private Slider healthSlider;

    void Start()
    {
        maxHealth = 100f;
        currentHealth = maxHealth;
        //healthSlider.wholeNumbers = true;
        healthSlider.minValue = 0;
        regenPerSecond = 1f;
        //UpdateHealthSlider();
    }

    //  to update the value of the UI Slider in unity Editor
    private void UpdateHealthSlider()
    {

        healthSlider.maxValue = maxHealth;
        healthSlider.value = currentHealth;
        healthText.text = (Mathf.FloorToInt(currentHealth).ToString() + "  /  " + Mathf.FloorToInt(maxHealth).ToString());
        
    }

    //  increase the maximum health
    public void IncreaseMaxHealth(float additionalHealth) 
    {
        maxHealth += additionalHealth;
        currentHealth += additionalHealth;
    }

    //  decrease the current health         --  taking damage
    public void DecreaseCurrentHealth(float damageTaken)
    {
        currentHealth -= damageTaken;

        if (currentHealth < 0)
            currentHealth = 0;
    }

    //  increase the current health         -- drinking poition?
    public void IncreaseCurrentHealth(float healingDone)
    {
        currentHealth += healingDone;

        if (currentHealth > maxHealth)
            currentHealth = maxHealth;
    }

    //  regen hp over delta time called in update 
    public void RegenHealth(float healthRegen)
    {
        float healthgain = healthRegen * Time.fixedDeltaTime;
        currentHealth += healthgain;

        if (currentHealth > maxHealth)
            currentHealth = maxHealth;
    }

    // increease the regen persecond 
    public void IncreaseRegenPerSecond(float regen)
    {
        regenPerSecond += regen;
    }

    //  decrease the regen per second
    public void DecreaseRegenPerSecond(float regen)
    {
        regenPerSecond -= regen;
    }

	// Update is called once per frame
	void FixedUpdate ()
	{
	    UpdateHealthSlider();

	    RegenHealth(regenPerSecond);
    }


}
