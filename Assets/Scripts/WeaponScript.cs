﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponScript : MonoBehaviour
{
	public GameObject bulletPrefab;
	public GameObject lasePrefab;
	[System.Serializable]
	public class Weapon
	{
		[TabGroup("Damage")]
		public float minDmg;
		[TabGroup("Damage")]
		public float maxDmg;
		[TabGroup("Damage")]
		public float weaponDamage;
		[HideInInspector]
		public float initialWeaponDamage;
		[TabGroup("Damage")]
		public float weaponChargeMod;
		[TabGroup("Type")]
		public PlayerCombat.WeaponType weaponType;
		[TabGroup("Speed")]
		public float attackspeed;
		[TabGroup("Range")]
		public float range;
		[TabGroup("Range")]
		public float knockBack;
		[TabGroup("Range")]
		public float swingDistance;
		[TabGroup("Speed")]
		public float fireRate;
		[Range(0,1f), TabGroup("Range")]
		public float spread;
		[TabGroup("Range")]
		public int bulletPershot;

		// Added by sc_
		[TabGroup("Scaling")]
		public float strengthScaling;
		[TabGroup("Scaling")]
		public float dexerityScaling;
		[TabGroup("Scaling")]
		public float enduranceScaling;
		[TabGroup("Scaling")]
		public float agilityScaling;
	}

	public Weapon weapon = new Weapon();

	// Use this for initialization
	void Start()
	{
  
	}

	// Update is called once per frame
	void Update()
	{

	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.layer == LayerMask.NameToLayer("Enemy") && other.isTrigger == false)
		{
			EnemyScript enemyScript = other.GetComponent<EnemyScript>();

			//Debug.Log("Collide");
			Vector3 difference = (other.transform.position - this.transform.position).normalized;
			other.attachedRigidbody.AddForce(difference * weapon.knockBack * enemyScript.enemy.impairmentWeakness, ForceMode2D.Impulse);

			//call do damage function 

			if (other.GetComponent<EnemyScript>() != null)
			{
				other.GetComponent<EnemyScript>().DamageTaken(weapon);
			}
			else
			{
				//Debug.Log("its a WALL");
			}
		}
	}
}