﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetInactiveScript : MonoBehaviour {

    public float time = 1f;
    IEnumerator SetInactive()
    {
        yield return new WaitForSeconds(time);
        gameObject.SetActive(false);
    }

    void OnEnable()
    {
        StartCoroutine(SetInactive());
    }
}
