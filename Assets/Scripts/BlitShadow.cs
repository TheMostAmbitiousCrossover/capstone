﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlitShadow : MonoBehaviour {

	public Camera ObjCam;
	public Camera ShadowCam;
	public Transform target;

	public RenderTexture source;
	public RenderTexture dest;
	public Material mat;

	private Vector4 pos;

	private float cameraSize;
	private float widthScale;
	private float heightScale;

	private const float baseScale = 5;
	private const float baseScaleX = 1;
	private const float baseScaleY = 1.7f;
	
	// Use this for initialization
	void Start () {
		Reset();
	}
	
	public void Reset()
	{
		float scale = baseScale / ShadowCam.orthographicSize;
		widthScale = baseScaleX * scale;
		heightScale = baseScaleY * scale;

		cameraSize = ObjCam.orthographicSize / ShadowCam.orthographicSize;
		RenderTexture.active = dest;
		GL.Clear(false, true, Color.clear);
	}

	// Update is called once per frame
	void Update () {
		pos = target.position / cameraSize;
		pos.x *= widthScale;
		pos.y *= heightScale;
		mat.SetVector("_Position", pos);
		Graphics.Blit(source, dest, mat);
	}
}
