﻿using Sirenix.OdinInspector;
using Sirenix.Utilities;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

// Enum so we can assign different things to different tiles
public enum Tile
{
	Unused = ' ',
	Floor = '.',
	Corridor = ',',
	Wall = '#',
	Door = '-',
	Upstairs = '<',
	Downstairs = '>',
	ClosedChest = '[',
	OpenChest = ']',
	LeftTable = 'l',
	RightTable = 'r',
	Fountain = '~',
	BossWall = 'w',
	BossFloor = 'f'
}

public enum Direction
{
	North,
	South,
	West,
	East,
	Length = 4
};

// Class that contains the data about the world
[System.Serializable]
public class Level
{
	// Above and Below LevelID's
	public int up, down;
	public int width, height;
	// Upstairs idx - Saves us from finding it every time
	public Vector2Int upstairs;

	[SerializeField, TabGroup("Tiles"), 
		TableMatrix(DrawElementMethod = "DrawLevel", SquareCells = true, ResizableColumns = true)]
	public Tile[,] tiles;

	// These are just for the creation stage of the level
	[SerializeField, TabGroup("Rects"), ListDrawerSettings(ShowIndexLabels = true)]
	public List<RectInt> rooms;
	[SerializeField, TabGroup("Rects"), ListDrawerSettings(ShowIndexLabels = true)]
	public List<RectInt> corridors;
	[SerializeField, TabGroup("Rects"), ListDrawerSettings(ShowIndexLabels = true)]
	public List<RectInt> exits;


	private static Tile DrawLevel(Rect rect, Tile value)
	{
		Color color = Color.black;

		switch(value)
		{
			case Tile.Unused:
				color = new Color(0, 0, 0, 0.5f);
				break;
			case Tile.Floor:
				color = new Color(0.10f, 0.33f, 0.55f);
				break;
			case Tile.Wall:
				color = new Color(0.88f, 1f, 0.99f);
				break;
			case Tile.Door:
				color = new Color(0.2f, 0.16f, 0.18f);
				break;
			case Tile.Downstairs:
				color = new Color(0.03f, 0.41f, 0.45f);
				break;
			case Tile.Upstairs:
				color = new Color(0.45f, 0.16f, 0.03f);
				break;
			case Tile.BossFloor:
				color = new Color(0.19f, 0.07f, 0.07f);
				break;
			case Tile.BossWall:
				color = new Color(0.61f, 0.2f, 0.2f);
				break;
			case Tile.Fountain:
				color = new Color(0.84f, 0.89f, 0.89f);
				break;
			case Tile.LeftTable:
			case Tile.RightTable:
				color = new Color(0.55f, 0.1f, 0.1f);
				break;
		}

		
		EditorGUI.DrawRect(rect.Padding(0), color);
		return value;
	}

	private void InitTiles()
	{
		for (int y = 0; y < height; ++y)
		{
			for (int x = 0; x < width; ++x)
			{
				tiles[x, y] = Tile.Unused;
			}
		}
	}

	private void InitLists()
	{
		rooms = new List<RectInt>();
		corridors = new List<RectInt>();
		exits = new List<RectInt>();
	}

	public Level()
	{
		width = height = 0;
		up = down = -1;
		tiles = new Tile[width, height];
		InitTiles();
		InitLists();
	}

	public Level(int _width, int _height)
	{
		width = _width;
		height = _height;
		up = down = -1;
		tiles = new Tile[width, height];
		InitTiles();
		InitLists();
	}
}

public class LevelGeneration : SerializedMonoBehaviour
{
	// The tilemap to store tiles on
	// 0 - No collision
	// 1 - Static collision
	// 2 - Dynamic collision
	// 3 - Lights 
	// 4 - Enemy
	[SerializeField, TabGroup("Layers"),
		ListDrawerSettings(ShowIndexLabels = true, 
		ListElementLabelName = "name")]
	public List<GameObject> layers;

	// Tileset to paint with
	// 0 - Floor
	// 1 - Wall
	// 2 - Door
	// 0 - Upstairs
	// 3 - Downstairs
	[SerializeField, TabGroup("Tiles"),
	ListDrawerSettings(ShowIndexLabels = true,
	ListElementLabelName = "name")]
	public List<GameObject> tiles;

	// Wall Tileset
	[SerializeField, TabGroup("Tiles"),
	ListDrawerSettings(ShowIndexLabels = true,
	ListElementLabelName = "name")]
	public List<Sprite> wallTiles;
	[SerializeField, TabGroup("Tiles"),
	ListDrawerSettings(ShowIndexLabels = true,
	ListElementLabelName = "name")]
	public List<Sprite> bossWallTiles;
	[SerializeField, TabGroup("Tiles"),
	ListDrawerSettings(ShowIndexLabels = true,
	ListElementLabelName = "name")]
	public List<Sprite> otherSprites;

	// Spawners to spawn
	// 0 - Lights
	// 1 - Enemy 1
	// 2 - Enemy 2
	// 3 - Enemy 3
	[SerializeField, TabGroup("Spawn"),
	ListDrawerSettings(ShowIndexLabels = true,
	ListElementLabelName = "name")]
	public List<GameObject> spawnables;

	[SerializeField, TabGroup("Spawn")]
	public GameObject boss1;
	[SerializeField, TabGroup("Spawn")]
	public GameObject boss2;
	[SerializeField, TabGroup("Spawn"),
	ListDrawerSettings(ShowIndexLabels = true,
	ListElementLabelName = "name")]
	public List<GameObject> bossRandom;

	[SerializeField, TabGroup("Spawn")]
	public GameObject ball;

	[SerializeField, TabGroup("Spawn")]
	public GameObject bossDoorTrigger;

	// Map to choose which layer each tile will go on
	private static readonly Dictionary<Tile, int> tilemapMap =
	new Dictionary<Tile, int>
	{
		{Tile.Unused,		0 },
		{Tile.Floor,		0 },
		{Tile.Corridor,		0 },
		{Tile.Wall,			1 },
		{Tile.Door,		    2 },
		{Tile.Upstairs,		1 },
		{Tile.Downstairs,	1 },
		{Tile.ClosedChest,	1 },
		{Tile.OpenChest,	1 },
		{Tile.LeftTable,    1 },
		{Tile.RightTable,   1 },
		{Tile.Fountain,     1 },
		{Tile.BossWall,     1 },
		{Tile.BossFloor,    0 }
	};

	// Map to choose which sprite each tile will use
	private static readonly Dictionary<Tile, int> tilebaseMap =
	new Dictionary<Tile, int>
	{
		{Tile.Floor,        0 },
		{Tile.Wall,         1 },
		{Tile.Door,         2 },
		{Tile.Upstairs,     0 },
		{Tile.Downstairs,   3 },
		{Tile.LeftTable,    4 },
		{Tile.RightTable,   5 },
		{Tile.Fountain,     6 },
		{Tile.BossFloor,    7 },
		{Tile.BossWall,     8 }
	};

	// Store our last level
	[SerializeField, TabGroup("Level")]
	public int lastLevel = 0;

	// Store our current level
	[SerializeField, TabGroup("Level")]
	public int currentLevel = 0;

	// Lookup dictionary for our level
	[SerializeField, TabGroup("Level")]
	public Dictionary<int, Level> world = new Dictionary<int, Level>();

	// List of all the lights our spawned lights can choose from
	[SerializeField, TabGroup("Lights"),
	ListDrawerSettings(ShowIndexLabels = true)]
	public List<Color> lightColors;

	// Attempt to spawn this many lights
	[SerializeField, TabGroup("Lights")]
	public int numLights;

	// Minimum startingPos between lights
	[SerializeField, TabGroup("Lights")]
	public float lightDistance;

	// Attempt to spawn this many enemies
	[SerializeField, TabGroup("Spawn")]
	public int numEnemies;

	// Minimum startingPos between enemies
	[SerializeField, TabGroup("Spawn")]
	public int enemyDistance;

	[SerializeField, TabGroup("Player")]
	// Move the player
	public bool movePlayer;

	// Can we go downstairs?
	[HideInInspector]
	public bool downstairActive;

	[HideInInspector]
	public bool startLoading;

	// We finished loading and can remove the loading screen
	[HideInInspector]
	public bool finishedLoading;

	// Transforms required to move the player
	public Transform player;
	public Transform cam;
	public GameObject loadingScreen;

	// For reseting the minimap
	public BlitShadow blitShadow;
	public GameObject RGBCamera;

    ProgressionScript progress;

	GameObject tile4;
	GameObject tile5;
	GameObject tile6;

	private static int lastID = 1;

	private int GenerateNewID()
	{
		return lastID++;
	}

	Tile GetTile(int x, int y)
	{
		Level l = GetCurrentLevel();
		if (x < 0 || y < 0 || x >= l.width || y >= l.height)
			return Tile.Unused;

		return l.tiles[x, y];
	}

	void SetTile(int x, int y, Tile tile)
	{
		world[currentLevel].tiles[x, y] = tile;
	}

	Level GetCurrentLevel()
	{
		return world[currentLevel];
	}

	void Start()
	{
        progress = player.gameObject.GetComponent<ProgressionScript>();
		finishedLoading = true;
		lastID = 1;
		NewLevel();
	}

	public void DestroyLevel()
	{
		foreach (GameObject l in layers)
		{
			int children = l.transform.childCount;
			for(int i = children - 1; i >= 0; i--)
			{
				Destroy(l.transform.GetChild(i).gameObject);
			}
		}
	}

	public void NewLevel()
	{
		// Generate the first level
		if((currentLevel + 1) % 5 == 0)
		{
			GenerateRestLevel();
		}
		else if ((currentLevel + 2) % 5 == 0)
		{
			GenerateBossLevel();
		}
		else
		{
			GenerateDungeonLevel(50, 50, 30);
		}

		// Reset the minimap
		RGBCamera.SetActive(true);
		blitShadow.Reset();

		CreateLevel();

		// Set the player to the upstairs tile (start of level)
		if (movePlayer)
		{
			Level l = GetCurrentLevel();
			Vector3 startPos = new Vector3(l.upstairs.x - (l.width / 2),
									-l.upstairs.y + (l.height / 2)) * 0.75f;
			player.position = startPos;
			cam.position = startPos + (Vector3.back * 2);
		}
	}

	bool PlaceObject(Tile tile)
	{
		Level l = GetCurrentLevel();
		if (l.rooms.Count == 0)
			return false;

		int r = Random.Range(0, l.rooms.Count); // choose a random room
		int x = Random.Range(l.rooms[r].x + 1, l.rooms[r].x + l.rooms[r].width - 2);
		int y = Random.Range(l.rooms[r].y + 1, l.rooms[r].y + l.rooms[r].height - 2);

		if (GetTile(x, y) == Tile.Floor)
		{
			SetTile(x, y, tile);

			// place one object in one room (optional)
			l.rooms.RemoveAt(r);

			// Save the downstairs index
			if(tile == Tile.Upstairs)
			{
				l.upstairs = new Vector2Int(x, y);
			}

			return true;
		}

		return false;
	}

	bool PlaceRect(RectInt rect, Tile tile)
	{
		Level l = GetCurrentLevel();
		if (rect.x < 1 || rect.y < 1 || 
			rect.x + rect.width > l.width - 1 || 
			rect.y + rect.height > l.height - 1)
			return false;

		for (int y = rect.y; y < rect.y + rect.height; ++y)
		{
			for (int x = rect.x; x < rect.x + rect.width; ++x)
			{
				if (GetTile(x, y) != Tile.Unused)
					return false; // the area already used
			}
		}

		for (int y = rect.y - 1; y < rect.y + rect.height + 1; ++y)
		{
			for (int x = rect.x - 1; x < rect.x + rect.width + 1; ++x)
			{
				if (x == rect.x - 1 || y == rect.y - 1 || 
					x == rect.x + rect.width || y == rect.y + rect.height)
					SetTile(x, y, Tile.Wall);
				else
					SetTile(x, y, tile);
			}
		}
		return true;
	}

	bool MakeRoom(int x, int y, Direction dir, bool firstRoom = false)
	{
		const int minRoomSize = 6;
		const int maxRoomSize = 15;

		RectInt room = new RectInt();
		room.width = Random.Range(minRoomSize, maxRoomSize);
		room.height = Random.Range(minRoomSize, maxRoomSize);

		switch (dir)
		{
			case Direction.North:
				room.x = x - room.width / 2;
				room.y = y - room.height;
				break;
			case Direction.South:
				room.x = x - room.width / 2;
				room.y = y + 1;
				break;
			case Direction.West:
				room.x = x - room.width;
				room.y = y - room.height / 2;
				break;
			case Direction.East:
				room.x = x + 1;
				room.y = y - room.height / 2;
				break;
		}

		if (PlaceRect(room, Tile.Floor))
		{
			Level l = GetCurrentLevel();
			l.rooms.Add(room);
			if (dir != Direction.South || firstRoom)
				l.exits.Add(new RectInt(room.x, room.y - 1, room.width, 1));
			if (dir != Direction.North || firstRoom)
				l.exits.Add(new RectInt(room.x, room.y + room.height, room.width, 1));
			if (dir != Direction.East || firstRoom)
				l.exits.Add(new RectInt(room.x - 1, room.y, 1, room.height));
			if (dir != Direction.West || firstRoom)
				l.exits.Add(new RectInt(room.x + room.width, room.y, 1, room.width));
			return true;
		}
		return false;
	}

	bool MakeCorridor(int x, int y, Direction dir)
	{
		const int minCorridorLength = 3;
		const int maxCorridorLength = 9;

		RectInt corridor = new RectInt();
		corridor.x = x;
		corridor.y = y;

		if (Random.value > 0.5f) // horizontal corridor
		{
			corridor.width = Random.Range(minCorridorLength, maxCorridorLength);
			corridor.height = 1;

			if (dir == Direction.North)
			{
				corridor.y = y - 1;
				if (Random.value > 0.5f) // west
					corridor.x = x - corridor.width + 1;
			}
			else if (dir == Direction.South)
			{
				corridor.y = y + 1;
				if (Random.value > 0.5f) // west
					corridor.x = x - corridor.width + 1;
			}
			else if (dir == Direction.West)
				corridor.x = x - corridor.width;
			else if (dir == Direction.East)
				corridor.x = x + 1;
		}
		else // vertical corridor
		{
			corridor.width = 1;
			corridor.height = Random.Range(minCorridorLength, maxCorridorLength);

			if (dir == Direction.North)
				corridor.y = y - corridor.height;
			else if (dir == Direction.South)
				corridor.y = y + 1;
			else if (dir == Direction.West)
			{
				corridor.x = x - 1;
				if (Random.value > 0.5f) // north
					corridor.y = y - corridor.height + 1;
			}
			else if (dir == Direction.East)
			{
				corridor.x = x + 1;
				if (Random.value > 0.5f) // north
					corridor.y = y - corridor.height + 1;
			}
		}

		if (PlaceRect(corridor, Tile.Corridor))
		{
			Level l = GetCurrentLevel();
			l.corridors.Add(corridor);
			if (dir != Direction.South && corridor.width != 1) // north side
				l.exits.Add(new RectInt(corridor.x, corridor.y - 1, corridor.width, 1));
			if (dir != Direction.North && corridor.width != 1) // south side
				l.exits.Add(new RectInt(corridor.x, corridor.y + corridor.height, corridor.width, 1));
			if (dir != Direction.East && corridor.height != 1) // west side
				l.exits.Add(new RectInt(corridor.x - 1, corridor.y, 1, corridor.height));
			if (dir != Direction.West && corridor.height != 1) // east side
				l.exits.Add(new RectInt(corridor.x + corridor.width, corridor.y, 1, corridor.height));

			return true;
		}

		return false;
	}

	bool CreateFeature(int x, int y, Direction dir)
	{
		const int roomChance = 50; // corridorChance = 100 - roomChance

		int dx = 0;
		int dy = 0;

		if (dir == Direction.North)			dy = 1;
		else if (dir == Direction.South)	dy = -1;
		else if (dir == Direction.West)		dx = 1;
		else if (dir == Direction.East)		dx = -1;

		if (GetTile(x + dx, y + dy) != Tile.Floor &&
			GetTile(x + dx, y + dy) != Tile.Corridor)
			return false;

		if (Random.Range(0, 100) < roomChance && MakeRoom(x, y, dir))
		{
			SetTile(x, y, Tile.Door);
			return true;
		}
		else if (MakeCorridor(x, y, dir))
		{
			if (GetTile(x + dx, y + dy) == Tile.Floor)
				SetTile(x, y, Tile.Door);
			else // don't place a door between corridors
				SetTile(x, y, Tile.Corridor);

			return true;
		}

		return false;
	}

	bool CreateFeature()
	{
		for (int i = 0; i < 1000; ++i)
		{
			Level l = GetCurrentLevel();
			if (l.exits.Count == 0)
				break;

			// choose a random side of a random room or corridor
			int r = Random.Range(0, l.exits.Count);
			int x = Random.Range(l.exits[r].x, l.exits[r].x + l.exits[r].width - 1);
			int y = Random.Range(l.exits[r].y, l.exits[r].y + l.exits[r].height - 1);

			// north, south, west, east
			for (int j = 0; j < (int)Direction.Length; ++j)
			{
				if (CreateFeature(x, y, (Direction)j))
				{
					l.exits.RemoveAt(r);
					return true;
				}
			}
		}

		return false;
	}

	void SetStairs(int top, int bottom)
	{
		try
		{
			Level topLevel = world[top];
			Level bottomLevel = world[bottom];
			topLevel.down = bottom;
			bottomLevel.up = top;
		}
		catch (System.Exception)
		{
			Debug.Log("Couldn't connect levels " + top + " and " + bottom);
		}
	}

	void GenerateDungeonLevel(int width, int height, int maxFeatures, bool hasStairs = true)
	{
		lastLevel = currentLevel;
		currentLevel = GenerateNewID();

		if(currentLevel % 5 == 0)
		{
			return;
		}
		else if((currentLevel + 1) % 5 == 0)
		{
			return;
		}

		Level newLevel = new Level(width, height);

		world.Add(currentLevel, newLevel);
		//SetStairs(lastLevel, currentLevel);

		Level l = GetCurrentLevel();

		if (!MakeRoom(l.width / 2, l.height / 2,
			(Direction)Random.Range(0, 4), true))
		{
			Debug.Log("Unable to place the first room");
			return;
		}

		// we already placed 1 feature (the first room)
		for (int i = 1; i < maxFeatures; ++i)
		{
			if (!CreateFeature())
			{
				Debug.Log("Unable to place more features (placed " + i + ")");
				break;
			}
		}

		if (hasStairs)
		{
			if (!PlaceObject(Tile.Upstairs))
			{
				Debug.Log("Unable to place upstairs");
				return;
			}

			if (!PlaceObject(Tile.Downstairs))
			{
				Debug.Log("Unable to place downstairs");
				return;
			}
		}

		for (int i = 0; i < l.width; ++i)
		{
			for (int j = 0; j < l.height; ++j)
			{
				if (GetTile(i, j) == Tile.Corridor)
					SetTile(i, j, Tile.Floor);
			}
		}

		// Generate the various things on the map
		GenerateLights();
		GenerateEnemies();
	}
	
	// If we are not adjacent to the supplied tile return false
	bool CheckAdjacent(int x, int y, Tile tile)
	{
		if (GetTile(x, y + 1) != tile) return false;
		if (GetTile(x + 1, y) != tile) return false;
		if (GetTile(x, y - 1) != tile) return false;
		if (GetTile(x - 1, y) != tile) return false;
		return true;
	}

	// If we are adjacent to the supplied tile return false
	bool CheckAdjacentNot(int x, int y, Tile tile)
	{
		if (GetTile(x, y + 1) == tile) return false;
		if (GetTile(x + 1, y) == tile) return false;
		if (GetTile(x, y - 1) == tile) return false;
		if (GetTile(x - 1, y) == tile) return false;
		return true;
	}

	Vector3 GetTilePosition(int x, int y, Level l)
	{
		return new Vector3(x - (l.width / 2), -y + (l.height / 2)) * 0.75f;
	}

	GameObject InstantiateObject(int objectID, int layer, Vector3 pos)
	{
		GameObject newTile = Instantiate(spawnables[objectID], layers[layer].transform);
		newTile.transform.position = pos;
		return newTile;
	}

	void GenerateLights()
	{
		if(spawnables.Count == 0)
		{
			Debug.Log("No Light Prefab");
			return;
		}

		if(layers.Count < 4)
		{
			Debug.Log("No Light Layer");
			return;
		}

		List<Vector2> spawnedLights = new List<Vector2>();

		Level l = GetCurrentLevel();

		for(int i = 0; i < numLights; ++i)
		{
			int x = Random.Range(0, l.width);
			int y = Random.Range(0, l.height);

			if ((GetTile(x, y) == Tile.Floor || GetTile(x, y) == Tile.BossFloor) && CheckAdjacentNot(x, y, Tile.Unused))
			{
				bool spawn = true;
				Vector2 selfPos = new Vector2(x, y);
				for (int j = 0; j < spawnedLights.Count; ++j)
				{
					if (Vector2.Distance(selfPos, spawnedLights[j]) < lightDistance)
					{
						spawn = false;
						break;
					}
				}

				if(spawn)
				{
					Vector3 pos = GetTilePosition(x, y, l);
					if (lightColors.Count == 0) return;
					Color randomColor = lightColors[Random.Range(0, lightColors.Count)];
					randomColor.a = 0.5f;
					GameObject newLight = InstantiateObject(0, 3, pos);
					newLight.GetComponent<Light2D.LightSprite>().Color = randomColor;
					spawnedLights.Add(selfPos);
				}
			}
		}
	}

	void GenerateEnemies()
	{
		if (spawnables.Count < 1)
		{
			Debug.Log("No Enemy Prefab");
			return;
		}

		if (layers.Count < 5)
		{
			Debug.Log("No Enemy Layer");
			return;
		}

		List<Vector2> spawnedEnemies = new List<Vector2>();

		Level l = GetCurrentLevel();

		for (int i = 0; i < numEnemies; ++i)
		{
			int x = Random.Range(0, l.width);
			int y = Random.Range(0, l.height);

			// This can spawn them in the door I think
			if (GetTile(x, y) == Tile.Floor && CheckAdjacentNot(x, y, Tile.Unused))
			{
				bool spawn = true;
				Vector2 selfPos = new Vector2(x, y);
				for (int j = 0; j < spawnedEnemies.Count; ++j)
				{
					if (Vector2.Distance(selfPos, spawnedEnemies[j]) < enemyDistance)
					{
						spawn = false;
						break;
					}
				}

				if (spawn)
				{
					Vector3 pos = GetTilePosition(x, y, l);
					int enemyIndex = Random.Range(1, spawnables.Count);
					/*GameObject newEnemy = */InstantiateObject(enemyIndex, 4, pos);
					//newEnemy.GetComponent<FollowPlayer>().player = player;
					spawnedEnemies.Add(selfPos);
				}
			}
		}
	}

	GameObject InstantiateTile(int spriteID, int layer, Vector3 pos)
	{
		GameObject newTile = Instantiate(tiles[spriteID], layers[layer].transform);
		newTile.transform.position = pos;
		return newTile;
	}

	void CreateLevel()
	{
		if(layers.Count < 3)
		{
			Debug.Log("Level not set");
			return;
		}

		Level l = GetCurrentLevel();
		for (int y = 0; y < l.height; ++y)
		{
			for (int x = 0; x < l.width; ++x)
			{
				Tile t = GetTile(x, y);
				if (t == Tile.Unused) continue;

				// Get Sprite
				int sprite = tilebaseMap[t];
				if(sprite >= tiles.Count) continue;

				// Get Layer
				int layer = tilemapMap[t];

				// Create Tile
				Vector3 pos = GetTilePosition(x, y, l);

				GameObject newTile = InstantiateTile(sprite, layer, pos);
				if (t == Tile.Wall)
				{
					if (wallTiles.Count < 25) continue;
					SpriteRenderer sr = newTile.GetComponent<SpriteRenderer>();
					sr.sprite = wallTiles[GetWallPosition(x, y)];
				}
				else if (t == Tile.BossWall)
				{
					if (wallTiles.Count < 25) continue;
					SpriteRenderer sr = newTile.GetComponent<SpriteRenderer>();
					sr.sprite = bossWallTiles[GetWallPosition(x, y)];
				}
				else if (t == Tile.Door)
				{
					HingeJoint2D hj = newTile.GetComponent<HingeJoint2D>();
					JointAngleLimits2D newLimit = new JointAngleLimits2D();
					if (GetTile(x + 1, y) == Tile.Floor)
					{
						newTile.transform.eulerAngles = Vector3.forward * 90f;
						newLimit.min = -180f;
						newLimit.max = 0f;
					}
					else
					{
						newLimit.min = -90f;
						newLimit.max = 90f;
					}

					hj.limits = newLimit;
					InstantiateTile(0, 0, pos);
				}
				else if (t == Tile.Downstairs)
				{
					if((currentLevel + 1) % 5 == 0)
					{
						SpriteRenderer sr = newTile.GetComponent<SpriteRenderer>();
						sr.sprite = otherSprites[0];
					}
				}
				else if (t == Tile.Upstairs)
				{
					if ((currentLevel + 1) % 5 == 0)
					{
						SpriteRenderer sr = newTile.GetComponent<SpriteRenderer>();
						sr.sprite = otherSprites[1];
					}
				}
				else if (t == Tile.LeftTable || t == Tile.RightTable)
				{
					newTile.transform.Translate(new Vector2(0.366f, 0));
				}
			}
		}
	}

	void Update()
	{
		if(startLoading)
		{
			StartCoroutine(Loading());
		}
	}

	void LateUpdate()
	{
		if(finishedLoading)
		{
			loadingScreen.SetActive(false);
			finishedLoading = false;
		}
		if (Input.GetKeyDown(KeyCode.E) && downstairActive)
		{

            progress.CalculateLevel();
            GoNextLevel();
		}
	}

	public void GoNextLevel()
	{
        //////////
		downstairActive = false;
		startLoading = true;
		finishedLoading = false;
		loadingScreen.SetActive(true);
	}

	IEnumerator Loading()
	{
		DestroyLevel();
		NewLevel();
		finishedLoading = true;
		startLoading = false;
		yield return null;
	}

	// How dir is calculated
	//  0 | 1 | 2
	// -----------
	//  7 |   | 3
	// -----------
	//  6 | 5 | 4
	int[] Sample4Directions(int x, int y, Tile tile)
	{
		int[] dir = new int[4];
		dir[0] = (GetTile(x, y - 1) == tile) ? 1 : 0;
		dir[1] = (GetTile(x + 1, y) == tile) ? 1 : 0;
		dir[2] = (GetTile(x, y + 1) == tile) ? 1 : 0;
		dir[3] = (GetTile(x - 1, y) == tile) ? 1 : 0;
		return dir;
	}

	int[] Sample4Corners(int x, int y, Tile tile)
	{
		int[] dir = new int[4];
		dir[0] = (GetTile(x - 1, y - 1) == tile) ? 1 : 0;
		dir[1] = (GetTile(x + 1, y - 1) == tile) ? 1 : 0;
		dir[2] = (GetTile(x + 1, y + 1) == tile) ? 1 : 0;
		dir[3] = (GetTile(x - 1, y + 1) == tile) ? 1 : 0;
		return dir;
	}

	private readonly static Dictionary<int[], int> wallDictionary4 =
	 new Dictionary<int[], int>() {
		{ new int[]{ 0, 0, 0, 0 },  0 }, // None
		{ new int[]{ 1, 0, 0, 0 },  8 }, // Up Hole
		{ new int[]{ 0, 1, 0, 0 }, 14 }, // Right Hole
		{ new int[]{ 0, 0, 1, 0 }, 20 }, // Down Hole
		{ new int[]{ 0, 0, 0, 1 },  1 }, // Left Hole
		{ new int[]{ 1, 1, 0, 0 },  2 }, // Bottom Left
		{ new int[]{ 1, 0, 0, 1 }, 21 }, // Bottom Right
		{ new int[]{ 0, 1, 1, 0 },  9 }, // Top Left
		{ new int[]{ 0, 0, 1, 1 }, 15 }, // Top Right
		{ new int[]{ 1, 0, 1, 0 }, 19 }, // Parallel Vertical
		{ new int[]{ 0, 1, 0, 1 }, 13 }, // Parallel Horizontal
		{ new int[]{ 0, 1, 1, 1 }, 10 }, // Up
		{ new int[]{ 1, 1, 1, 0 },  3 }, // Left
		{ new int[]{ 1, 0, 1, 1 }, 16 }, // Right
		{ new int[]{ 1, 1, 0, 1 }, 22 }, // Bottom
		{ new int[]{ 1, 1, 1, 1 },  6 }, // All
	 };

	private readonly static Dictionary<int, int> checkCorner = 
	 new Dictionary<int, int>() {
		{  0, 0 }, // None
		{  2, 1 }, // Bottom Left
		{ 21, 2 }, // Bottom Right
		{  9, 3 }, // Top Left
		{ 15, 4 }, // Top Right
		{ 10, 5 }, // Up
		{  3, 6 }, // Left
		{ 16, 7 }, // Right
		{ 22, 8 }, // Bottom
	 };

	private readonly static Dictionary<int[], int> cornerDictionary4 =
	 new Dictionary<int[], int>() {
		{ new int[]{  0,  0,  0,  0 },  7 }, // Corners
		{ new int[]{ -1,  0, -1, -1 },  4 }, // Bottom Left + Corner
		{ new int[]{  0, -1, -1, -1 }, 23 }, // Bottom Right + Corner
		{ new int[]{ -1, -1,  0, -1 }, 11 }, // Top Left  + Corner
		{ new int[]{ -1, -1, -1,  0 }, 17 }, // Top Right + Corner
		{ new int[]{ -1, -1,  0,  0 }, 12 }, // Up + Corner
		{ new int[]{ -1,  0,  0, -1 },  5 }, // Left + Corner
		{ new int[]{  0, -1, -1,  0 }, 18 }, // Right + Corner
		{ new int[]{  0,  0, -1, -1 }, 24 }, // Bottom + Corner
	 };

	int GetDictionaryValue(Dictionary<int[], int> dict, int[] key)
	{
		foreach (KeyValuePair<int[], int> pair in dict)
		{
			if (pair.Key.SequenceEqual(key)) return pair.Value;
		}
		return 0;
	}

	bool CompareCorners(int[] a, int[] b)
	{
		for(int i = 0; i < a.Length; ++i)
		{
			if (a[i] == -1) continue;
			if (a[i] != b[i]) return false;
		}
		return true;
	}

	int GetWallPosition(int x, int y)
	{
		int[] dir4 = Sample4Directions(x, y, Tile.Wall);
		int val = GetDictionaryValue(wallDictionary4, dir4);

		if(checkCorner.ContainsKey(val))
		{
			int idx = checkCorner[val];
			int[] cor4 = Sample4Corners(x, y, Tile.Wall);
			KeyValuePair<int[], int> pair = cornerDictionary4.ElementAt(idx);
			if(CompareCorners(pair.Key, cor4))
			{
				return pair.Value;
			}
		}

		return val;
	}

	void GenerateRestLevel()
	{
		lastLevel = currentLevel;
		currentLevel = GenerateNewID();
		Level restLevel = new Level(25, 15);
		world.Add(currentLevel, restLevel);
		for (int x = 0; x < 25; x++)
		{
			for (int y = 0; y < 15; y++)
			{
				SetTile(x, y, Tile.Wall);
			}
		}

		for (int x1 = 1; x1 < 24; x1++)
		{
			for (int y2 = 1; y2 < 14; y2++)
			{
				SetTile(x1, y2, Tile.Floor);
			}
		}

		SetTile(6, 1, Tile.Wall);
		SetTile(6, 2, Tile.Wall);
		SetTile(6, 3, Tile.Wall);
		SetTile(6, 4, Tile.Wall);
		SetTile(6, 5, Tile.Wall);
		SetTile(1, 5, Tile.Wall);
		SetTile(2, 5, Tile.Wall);
		SetTile(3, 5, Tile.Wall);
		SetTile(6, 5, Tile.Wall);
		
		SetTile(18, 1, Tile.Wall);
		SetTile(18, 2, Tile.Wall);
		SetTile(18, 3, Tile.Wall);
		SetTile(18, 4, Tile.Wall);
		SetTile(18, 5, Tile.Wall);
		SetTile(23, 5, Tile.Wall);
		SetTile(22, 5, Tile.Wall);
		SetTile(21, 5, Tile.Wall);
		SetTile(18, 5, Tile.Wall);

		SetTile(12, 13, Tile.Upstairs);

		SetTile(4, 5, Tile.LeftTable);
		
		SetTile(19, 5, Tile.RightTable);

		SetTile(12, 1, Tile.Downstairs);

		SetTile(12, 8, Tile.Fountain);

		Vector3 fountain = GetTilePosition(13, 8, restLevel);

		restLevel.upstairs.x = 12;
		restLevel.upstairs.y = 13;

		Instantiate(ball, new Vector3(fountain.x, fountain.y, fountain.z), 
			Quaternion.identity, layers[2].transform);

		GenerateLights();
	}

	public void DestroyWall()
	{
		Destroy(tile4);
		Destroy(tile5);
		Destroy(tile6);
	}

	void GenerateBossLevel()
	{
		lastLevel = currentLevel;
		currentLevel = GenerateNewID();
		Level bossLevel = new Level(18, 20);
		world.Add(currentLevel, bossLevel);

		for (int x = 0; x < 18; x++)
		{
			for (int y = 0; y < 20; y++)
			{
				SetTile(x, y, Tile.BossWall);
			}
		}

		for (int x1 = 1; x1 < 17; x1++)
		{
			for (int y2 = 1; y2 < 19; y2++)
			{
				SetTile(x1, y2, Tile.BossFloor);
			}
		}

		for (int x2 = 1; x2 < 7; x2++)
		{
			for (int y = 16; y < 19; y++)
			{
				SetTile(x2, y, Tile.BossWall);
			}
		}

		for (int x2 = 10; x2 < 17; x2++)
		{
			for (int y = 16; y < 19; y++)
			{
				SetTile(x2, y, Tile.BossWall);
			}
		}

		for (int x2 = 1; x2 < 7; x2++)
		{
			for (int y = 1; y < 4; y++)
			{
				SetTile(x2, y, Tile.BossWall);
			}
		}

		for (int x2 = 10; x2 < 17; x2++)
		{
			for (int y = 1; y < 4; y++)
			{
				SetTile(x2, y, Tile.BossWall);
			}
		}

		bossLevel.upstairs.x = 8;
		bossLevel.upstairs.y = 18;

		SetTile(bossLevel.upstairs.x, bossLevel.upstairs.y, Tile.Upstairs);
		SetTile(bossLevel.upstairs.x, 1, Tile.Downstairs);

		Vector2Int[] spawnXY = new Vector2Int[]
		{
			new Vector2Int(bossLevel.upstairs.x, bossLevel.upstairs.y - 2),
			new Vector2Int(bossLevel.upstairs.x - 1, bossLevel.upstairs.y - 2),
			new Vector2Int(bossLevel.upstairs.x + 1, bossLevel.upstairs.y - 2),

			new Vector2Int(bossLevel.upstairs.x, 3),
			new Vector2Int(bossLevel.upstairs.x - 1, 3),
			new Vector2Int(bossLevel.upstairs.x + 1, 3),
		};

		Vector3[] spawnPos = new Vector3[spawnXY.Length];
		for(int i = 0; i < spawnPos.Length; ++i)
		{
			spawnPos[i] = GetTilePosition(spawnXY[i].x, spawnXY[i].y, bossLevel);
		}

		GameObject[] tiles = new GameObject[spawnXY.Length];
		for(int i = 0; i < spawnPos.Length; ++i)
		{
			tiles[i] = InstantiateTile(8, 1, spawnPos[i]);
			tiles[i].GetComponent<SpriteRenderer>().sprite = 
				bossWallTiles[GetWallPosition(spawnXY[i].x, spawnXY[i].y)];

		}

		GameObject triggerObject = Instantiate(bossDoorTrigger, spawnPos[0], Quaternion.identity);
		triggerObject.GetComponent<BossDoorTrigger>().tile1 = tiles[0];
		triggerObject.GetComponent<BossDoorTrigger>().tile2 = tiles[1];
		triggerObject.GetComponent<BossDoorTrigger>().tile3 = tiles[2];

		for(int i = 0; i < 3; ++i)
		{
			tiles[i].SetActive(false);
		}

		Vector3 bossSpawnPoint = GetTilePosition(8, 9, bossLevel);

		if((currentLevel + 1) / 5 == 1)
		{
			GameObject newBoss = Instantiate(boss1, layers[4].transform);
			newBoss.transform.position = bossSpawnPoint;
			newBoss.GetComponent<GeneralBossScript>().lg = this;
		}
		else if ((currentLevel + 1) / 5 == 2)
		{
			GameObject newBoss = Instantiate(boss2, layers[4].transform);
			newBoss.transform.position = bossSpawnPoint;
			newBoss.GetComponent<GeneralBossScript>().lg = this;
		}
		else
		{
			GameObject bossToSpawn = (bossRandom.Count > 0) ? 
			bossRandom[Random.Range(0, bossRandom.Count)] : boss1;
			GameObject newBoss = Instantiate(bossToSpawn, layers[4].transform);
			newBoss.transform.position = bossSpawnPoint;
			newBoss.GetComponent<GeneralBossScript>().lg = this;
		}

		tile4 = tiles[3];
		tile5 = tiles[4];
		tile6 = tiles[5];

		GenerateLights();
	}
}
