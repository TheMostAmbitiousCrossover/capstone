﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleManager : MonoBehaviour {
    public float seconds;
    public Color enemyColor;
	// Use this for initialization
	void Start () {
        ParticleSystem ps = GetComponent<ParticleSystem>();
        var main = ps.main;
        main.startColor = enemyColor;
        Destroy(gameObject, seconds);
    }
    // Update is called once per frame
    void Update()
    {
    }  
    
}
