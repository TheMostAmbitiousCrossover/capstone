﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour {

    public Transform player;
    public float distance;

    public float speed = 1.0f;

    private Rigidbody2D rb;
    private Transform myTransform;

    public float minDistanceToPlayer;

    private float drag = 1000.0f;

	// Use this for initialization
	void Start ()
    {
        rb = GetComponent<Rigidbody2D>();
        myTransform = transform;
        distance = 5.0f;
        rb.drag = drag;
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(player)
        {
            Vector3 direction = player.position - myTransform.position;
            if(direction.magnitude < distance && direction.magnitude > minDistanceToPlayer)
            { 
                rb.AddForce(direction.normalized * speed * drag);
            }

            
        }
	}
}
