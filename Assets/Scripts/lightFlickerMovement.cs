﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lightFlickerMovement : MonoBehaviour {

	// Light's movement gSpeed
	public float speed;
	// Light's distance away from center
	public float size;

	private Transform thisTransform;

	// Keep track of start and end positions for smooth interpolation
	private Vector2 destination;
	private Vector2 lastDestination;

	// Distance before we pick a new destination
	private float tolerance = 0.01f;

	// Keep track of the time
	private float time;

	void NewDestination()
	{
		lastDestination = thisTransform.localPosition;
		destination = Random.insideUnitCircle * size;
		time = 0;
	}

	void Start () {
		thisTransform = transform;
		NewDestination();
	}
	
	void Update () {
		Vector2 lerped = Vector2.Lerp(lastDestination, 
			destination, time);

		time += speed * Time.deltaTime;

		thisTransform.localPosition = lerped;
		if (Vector2.Distance(lerped, destination) < tolerance)
		{
			NewDestination();
		}
	}
}
