﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementScript : MonoBehaviour {

    // Will change this when we make stats that effect movement gSpeed
    public float baseMoveSpeed;
    public float moveSpeed = 1.0f;
    public bool isSlow;
    public PlayerStats playerStats;
    public Stats stats;
    // Will change this when we make stats that effect turn gSpeed
    public float turnSpeed = 1.0f;

    // Will change this when we make stats that effect dash gSpeed
    public float dashSpeed = 100.0f;

    // Will change this when we make stats that effect dash time
    public float dashTime = 0.036f;

    // We use rigidbody for movement so we don't have problems with collisions
    private Rigidbody2D playerRigidBody;
    // Store the Transform
    private Transform playerTransform;

    // Store our last movement
    private Vector3 movement;

    // Store if we are dashing
    private bool isDashing;

    // Store our dash vector
    //private Vector3 dashVector;

    // Store the deltaTime
    private float deltaTime;

    // Store the amount of time left until we stop dashing
    private float dashTimer;

    // We use this to offset the high friction we need on the 
    // rigidbody for accurate snappy movement
    private const float baseMovementSpeed = 1000;

	private float lastMag;
	public float magThreshold;

    private TrailRenderer trailRenderer;

	void Start () {
        trailRenderer = GetComponent<TrailRenderer>();
        baseMoveSpeed = moveSpeed;
        playerRigidBody = gameObject.GetComponent<Rigidbody2D>();
        playerTransform = transform;
	}

	void FixedUpdate()
	{
		deltaTime = Time.deltaTime;

		// WASD Movement
		movement = new Vector2(
			Input.GetAxis("Horizontal"),
			Input.GetAxis("Vertical")
		);
		if (!isDashing)
		{
			playerRigidBody.velocity = movement * baseMoveSpeed;
		}
		else
		{
			playerRigidBody.velocity = Vector3.Normalize(movement) * 
				dashSpeed * baseMoveSpeed;
		}
	}

	void Update () {

		deltaTime = Time.deltaTime;


        // Face the Cursor
        {
            Vector3 mousePostion = Camera.main.
                ScreenToWorldPoint(Input.mousePosition);

            Vector2 Direction = new Vector2(
                mousePostion.x - playerTransform.position.x,
                mousePostion.y - playerTransform.position.y
            );

            playerTransform.up = Direction;
        }

        // Dash
        if(Input.GetButtonDown("Jump") && !isDashing && stats.CurrentEnergy 
			>= playerStats.GetEnergyConsumption())
        {
            
            playerStats.UpdateEnergy(-playerStats.GetEnergyConsumption());
            isDashing = true;
            dashTimer = dashTime;
            //dashVector = movement;
        }
        else if (isDashing)
        {
            trailRenderer.emitting = true;
			//playerRigidBody.AddForce(Vector3.Normalize(dashVector) *
			//	dashSpeed * baseMovementSpeed);
			dashTimer -= deltaTime;
            if (dashTimer <= -0.2f) isDashing = false;
            
        }
        else if (!isDashing)
        {
            playerRigidBody.velocity = Vector2.zero;
            trailRenderer.emitting = false;
        }
	}
}
