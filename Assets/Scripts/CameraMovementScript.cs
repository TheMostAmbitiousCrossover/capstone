﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovementScript : MonoBehaviour {

	// The camera this will script will effect
	public Transform cam;

	// The target the camera will track
	public Transform target;

	// The percentage at which the camera moves
	public float xTolerance = 0.9f;
	public float yTolerance = 0.9f;

	public float cameraMovementSpeed = 1f;

	// Store the camera dimensions
	//private float height;
	//private float width;

	// Store the corner points
	//private Vector3 Middle;
	//private float halfWidth;
	//private float halfHeight;

	void Start () {
		// In the actual game this should only be done once
		// but in the editor we might need to change the aspect healthRatioUI so updates
		// may be required
		//Camera c = cam.GetComponent<Camera>();
		//height = 2f * c.orthographicSize;
		//width = height * c.aspect;

		//Middle = cam.transform.position;
		//
		//halfWidth = width / 2;
		//halfHeight = height / 2;
	}
	// Transform of the camera to shake. Grabs the gameObject's transform
	// if null.
	public Transform camTransform;

	// How long the object should shake for.
	public float shakeDuration = 0f;

	// Amplitude of the shake. A larger value shakes the camera harder.
	public float shakeAmount = 0.7f;
	public float decreaseFactor = 1.0f;


	void Awake()
	{
		if (camTransform == null)
		{
			camTransform = GetComponent(typeof(Transform)) as Transform;
		}
	}


	void Update()
	{
	 
	}
	void FixedUpdate () {

		Vector3 lerped = Vector3.Lerp(cam.position, target.position, 
			Time.deltaTime * cameraMovementSpeed);
		lerped.z = -2;
		cam.transform.position = lerped;

		//Vector3 a = target.position;
		//a.z = -2;
		//cam.transform.position = a;
		if (shakeDuration > 0)
		{
			camTransform.localPosition += Random.insideUnitSphere * shakeAmount;

			shakeDuration -= Time.deltaTime * decreaseFactor;
		}
		else
		{
			shakeDuration = 0f;
		}
	}
}
// 		Vector3 targetPos = target.position;
// 
// 		// Check x
// 		float xDistance = targetPos.x - Middle.x;
// 		float yDistance = targetPos.y - Middle.y;
// 
// 		float xPercent = xDistance / halfWidth;
// 		float yPercent = yDistance / halfHeight;


//         if (xPercent >= xTolerance || xPercent <= -xTolerance ||
//             yPercent >= yTolerance || yPercent <= -yTolerance)
//         {
//             Vector3 lerped = Vector3.Lerp(cam.transform.position, target.position, cameraMovementSpeed * Time.deltaTime);
//             lerped.z = -2;
//             cam.transform.position = lerped;
//             Middle = cam.transform.position;
//         }

// Horizontal
//         if (xPercent >= xTolerance || xPercent <= -xTolerance)
// 		{
//             // 			int dir = 1;
//             // 			if (xPercent < 0) dir *= -1;
//             //             Vector3 lerped = Vector3.Lerp(Middle, target.position, cameraMovementSpeed * Time.deltaTime);
//             //             lerped.z = -2;
//             //             cam.transform.position = lerped;
//             //cam.transform.position += Vector3.right * cameraMovementSpeed * Time.deltaTime * dir;
//             //			Middle = cam.transform.position;
// 		}

// Vertical
// 		if (yPercent >= yTolerance || yPercent <= -yTolerance)
// 		{
//             // 			int dir = 1;
//             // 			if (yPercent < 0) dir *= -1;
//             //             Vector3 lerped = Vector3.Lerp(Middle, target.position, cameraMovementSpeed * Time.deltaTime);
//             //             lerped.z = -2;
//             //             cam.transform.position = lerped;
//             //cam.transform.position += Vector3.up * cameraMovementSpeed * Time.deltaTime * dir;
//             //Middle = cam.transform.position;
//         }
