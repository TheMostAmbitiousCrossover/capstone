﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Sirenix.OdinInspector;

public class PlayerCombat : SerializedMonoBehaviour
{
    
    public enum WeaponType
    {
        Unarmed = 0,  // 1H Nothing
        Sword = 1,  // 1H Left click Swing        | Right click Swing
        Javelin = 2,  // 1H Left click Throw        | Right click Throw
        Whip = 3,  // 2H Left click Sling        | Right click Attach and Release
        Axe = 4,  // 2H Left click Swing        | Right click Throw
        Shield = 5,  // 1H Left click Bash         | Right click bash

        Pistol = 6,  // 1H Left click Shoot        | Right click Shoot
        Shotgun = 7,  // 1H Left click Shoot        | Right click Shoot
        Rocket = 8,  // 2H Left click Shoot        | Right click Homing line
        Machine = 9,  // 2H Left click Shoot        | Right click Place and Pickup
        Raygun = 10, // 2H Left click thin beam    | Right click thick beam
        RecallGun = 11,
        ShiedGun = 12,
        Length = 13
    }

    [System.Serializable]
    public class WeaponContainer
    {
        [TabGroup("References")]
        public GameObject weapon;
        [TabGroup("References")]
        public GameObject spawn;
        [TabGroup("Type")]
        public WeaponType weaponType;

        [TabGroup("References")]
        public Rigidbody2D rb;
        [TabGroup("References")]
        public Transform tf;

        [TabGroup("Time")]
        public float timer;
        [TabGroup("Time")]
        public float time;

        [TabGroup("Time")]
        public float holdTimer;
        [TabGroup("Time")]
        public float maxHold;

        [TabGroup("Checks")]
        public bool pressed;
    }

    delegate bool Attack(WeaponScript weapon, WeaponContainer hand,
        ref Transform tf, ref float timer, ref float time,
        int direction, GameObject spawn, Vector2 mousePos);

    private Attack[] attackLeft = null;
    private Attack[] attackRight = null;

    // We use rigidbody for movement so we don't have problems with collisions
    private Rigidbody2D playerRigidBody;
    // Store the Transform
    //private Transform playerTransform;

    public Vector3 to = Vector3.forward;
    public Vector3 from = Vector3.zero;

    //public float swingDistance;

    [TabGroup("Left")]
    public WeaponContainer left;
    [TabGroup("Right")]
    public WeaponContainer right;

    public GameObject weaponGameObjectRight;
    public GameObject weaponGameObjectLeft;

    //To access the script on each weapon. To access the actual weapon add '.weapon'
    //can't make it public WeaponScript.Weapon because that's stops me from initialising its value
    public WeaponScript weaponRightScript;
    public WeaponScript weaponLeftScript;

    public PlayerStats stats;

    public GameObject bulletPrefab;
    public float leftFireRateTimer;
    public float rightFireRateTimer;

    public Camera mousePosCamera;

    // Use this for initialization
    void Awake()
    {
        stats = GetComponent<PlayerStats>();

        weaponRightScript = weaponGameObjectRight.GetComponentInChildren<WeaponScript>();
        weaponLeftScript = weaponGameObjectLeft.GetComponentInChildren<WeaponScript>();

        left.weaponType = weaponLeftScript.weapon.weaponType;
        right.weaponType = weaponRightScript.weapon.weaponType;

        weaponGameObjectRight.GetComponentInChildren<Collider2D>().enabled = false;
        weaponGameObjectRight.GetComponentInChildren<SpriteRenderer>().enabled = false;
        weaponGameObjectLeft.GetComponentInChildren<Collider2D>().enabled = false;
        weaponGameObjectLeft.GetComponentInChildren<SpriteRenderer>().enabled = false;

        //currWeapon = Weapon.unarmed;
        //playerTransform = transform;
        playerRigidBody = gameObject.GetComponent<Rigidbody2D>();

        attackLeft = new Attack[]
        {
            Empty,
            Swing,
            Throw,
            Sling,
            AxeSwing,
            Bash,
            Shoot,
            Shoot,
            Shoot,
            Shoot,
            Beam,
            RecallShot,
            Shoot
        };

        attackRight = new Attack[]
        {
            Empty,
            Swing,
            Throw,
            Attach,
            AxeSwing,
            Bash,
            Shoot,
            Shoot,
            Shoot,
            Shoot,
            Beam,
            RecallShot,
            Shoot
        };
        //leftFireRateTimer = weaponLeftScript.weapon.attackspeed;
        //leftFireRateTimer = 0;
    }

    void UpdateLeftWeapon()
    {
        //bool leftClickDown = Input.GetButtonDown("Fire1");
        bool leftClick = Input.GetButton("Fire1");
        if (leftClick)
        {
            left.pressed = attackLeft[(int)left.weaponType](weaponLeftScript,
                left, ref left.tf, ref left.timer, ref left.time, 1, left.spawn,
                mousePosCamera.ScreenToWorldPoint(Input.mousePosition));
            leftFireRateTimer = weaponLeftScript.weapon.fireRate;
        }
    }

    void UpdateRightWeapon()
    {
        //bool rightClickDown = Input.GetButtonDown("Fire2");
        bool rightClick = Input.GetButton("Fire2");
        if (rightClick)
        {
            right.pressed = attackRight[(int)right.weaponType](weaponRightScript,
                right, ref right.tf, ref right.timer, ref right.time, 1, right.spawn,
                mousePosCamera.ScreenToWorldPoint(Input.mousePosition));
            rightFireRateTimer = weaponRightScript.weapon.fireRate;
        }
    }

    void LateUpdateRewrite()
    {
        // This doesnt need to be here but for simplicity sakes we'll keep it
        // here for now
        left.weaponType = weaponLeftScript.weapon.weaponType;
        right.weaponType = weaponRightScript.weapon.weaponType;

        // Shooting Cooldown
        leftFireRateTimer -= Time.deltaTime;
        rightFireRateTimer -= Time.deltaTime;

        // If we cant shoot return
        if (leftFireRateTimer > 0 && rightFireRateTimer > 0) return;

        // Update the Left/Right side Respectively
        if (leftFireRateTimer < 0) UpdateLeftWeapon();
        if (rightFireRateTimer < 0) UpdateRightWeapon();

    }

    void LateUpdate()
    {
        LateUpdateRewrite();
        return;

        // 		left.weaponType = weaponLeftScript.weapon.weaponType;
        // 		right.weaponType = weaponRightScript.weapon.weaponType;
        // 
        // 		if (Input.GetButton("Fire1") && !left.pressed)
        // 		{
        // 			if (left.weaponType == WeaponType.Pistol)
        // 			{
        // 				if (instantiateTimer <= 0)
        // 				{
        // 					left.pressed = attackLeft[(int)WeaponType.Pistol](weaponLeftScript, left, ref left.tf, ref left.timer, ref left.time, 1,
        // 						left.spawn, (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition));
        // 					instantiateTimer = weaponLeftScript.weapon.fireRate;
        // 				}
        // 			}
        // 
        // 			if (left.weaponType == WeaponType.Shotgun)
        // 			{
        // 				if (instantiateTimer <= 0)
        // 				{
        // 					attackLeft[(int)WeaponType.Shotgun](weaponLeftScript, left, ref left.tf, ref left.timer, ref left.time, 1,
        // 						left.spawn, (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition));
        // 					instantiateTimer = weaponLeftScript.weapon.fireRate;
        // 				}
        // 			}
        // 
        // 			if (left.weaponType == WeaponType.Raygun)
        // 			{
        // 				if (instantiateTimer <= 0)
        // 				{
        // 				   
        // 					left.pressed = attackLeft[(int)WeaponType.Raygun](weaponLeftScript, left, ref left.tf, ref left.timer, ref left.time, 1,
        // 						left.spawn, (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition));
        // 					instantiateTimer = weaponLeftScript.weapon.fireRate;
        // 				}
        // 			}
        // 		}
        // 		if (Input.GetButtonDown("Fire1") && !left.pressed)
        // 		{
        // 			if (left.weaponType == WeaponType.Pistol)
        // 			{
        // 				if (instantiateTimer <= 0)
        // 				{
        // 					left.pressed = attackLeft[(int)WeaponType.Pistol](weaponLeftScript, left, ref left.tf, ref left.timer, ref left.time, 1,
        // 						left.spawn, Vector2.zero);
        // 					instantiateTimer = weaponLeftScript.weapon.fireRate;
        // 				}
        // 			}
        // 
        // 			if (left.weaponType == WeaponType.Shotgun)
        // 			{
        // 				if (instantiateTimer <= 0)
        // 				{
        // 					Shoot(weaponLeftScript, left, ref left.tf, ref left.timer, ref left.time, 1,
        // 						left.spawn, (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition));
        // 					instantiateTimer = weaponLeftScript.weapon.fireRate;
        // 				}
        // 			}
        // 
        // 			if (left.weaponType == WeaponType.Raygun)
        // 			{
        // 				if (instantiateTimer <= 0)
        // 				{        
        // 					left.pressed = attackLeft[(int)WeaponType.Raygun](weaponLeftScript, left, ref left.tf, ref left.timer, ref left.time, 1,
        // 						left.spawn, Vector2.zero);
        // 					instantiateTimer = weaponLeftScript.weapon.fireRate;
        // 				}
        // 			}
        // 		}
        // 		instantiateTimer -= Time.deltaTime;
        // 		if (Input.GetButtonUp("Fire1") && !left.pressed)
        // 		{
        // 			if (left.weaponType == WeaponType.Pistol)
        // 			{
        // 				if (instantiateTimer <= 0)
        // 				{
        // 					left.pressed = attackLeft[(int)WeaponType.Pistol](weaponLeftScript, left, ref left.tf, ref left.timer, ref left.time, 1,
        // 						left.spawn, Vector2.zero);
        // 					instantiateTimer = weaponLeftScript.weapon.fireRate;
        // 				}
        // 			  
        // 			}
        // 		}
        // 
        // 		//Same thing for weapon 2
        // 		if (Input.GetButton("Fire2") && !right.pressed)
        // 		{
        // 			if (right.weaponType == WeaponType.Pistol)
        // 			{
        // 				if (instantiateTimer2 <= 0)
        // 				{
        // 					right.pressed = attackRight[(int)WeaponType.Pistol](weaponRightScript, right, ref right.tf, ref right.timer, ref right.time, -1,
        // 						right.spawn, Vector2.zero);
        // 					instantiateTimer2 = weaponRightScript.weapon.fireRate;
        // 				}
        // 			}
        // 
        // 			if (right.weaponType == WeaponType.Shotgun)
        // 			{
        // 				if (instantiateTimer2 <= 0)
        // 				{
        // 					right.pressed = attackRight[(int)WeaponType.Shotgun](weaponRightScript, right, ref right.tf, ref right.timer, ref right.time, -1,
        // 						right.spawn, (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition));
        // 					instantiateTimer2 = weaponRightScript.weapon.fireRate;
        // 				}
        // 			}
        // 
        // 			if (right.weaponType == WeaponType.Raygun)
        // 			{
        // 				if (instantiateTimer2 <= 0)
        // 				{
        // 					right.pressed = attackRight[(int)WeaponType.Raygun](weaponRightScript, right, ref right.tf, ref right.timer, ref right.time, 1,
        // 						right.spawn, Vector2.zero);
        // 					instantiateTimer2 = weaponRightScript.weapon.fireRate;
        // 				}
        // 			}
        // 		}
        // 		if (Input.GetButtonDown("Fire2") && !right.pressed)
        // 		{
        // 			if (right.weaponType == WeaponType.Pistol)
        // 			{
        // 				if (instantiateTimer2 <= 0)
        // 				{
        // 					right.pressed = attackRight[(int)WeaponType.Pistol](weaponRightScript, right, ref right.tf, ref right.timer, ref right.time, -1,
        // 						right.spawn, Vector2.zero);
        // 					instantiateTimer2 = weaponRightScript.weapon.fireRate;
        // 				}
        // 			}
        // 
        // 			if (right.weaponType == WeaponType.Shotgun)
        // 			{
        // 				if (instantiateTimer2 <= 0)
        // 				{
        // 					right.pressed = attackRight[(int)WeaponType.Shotgun](weaponRightScript, right, ref right.tf, ref right.timer, ref right.time, -1,
        // 						right.spawn, (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition));
        // 					instantiateTimer2 = weaponRightScript.weapon.fireRate;
        // 				}
        // 			}
        // 
        // 			if (right.weaponType == WeaponType.Raygun)
        // 			{
        // 				if (instantiateTimer2 <= 0)
        // 				{
        // 					right.pressed = attackRight[(int)WeaponType.Raygun](weaponRightScript, right, ref right.tf, ref right.timer, ref right.time, 1,
        // 						right.spawn, Vector2.zero);
        // 					instantiateTimer2 = weaponRightScript.weapon.fireRate;
        // 				}
        // 			}
        // 		}
        // 		instantiateTimer2 -= Time.deltaTime;
        // 		if (Input.GetButtonUp("Fire2") && !right.pressed)
        // 		{
        // 
        // 		}
        // 
        // 		//Perform attack based in type and if pressed is true
        // 		for (int i = 0; i < (int)WeaponType.Length; ++i)
        // 		{
        // 			if (left.weaponType == (WeaponType)i)
        // 			{
        // 				if (left.pressed)
        // 					left.pressed = attackLeft[i](weaponLeftScript, left, ref left.tf, ref left.timer, ref left.time, 1,
        // 						left.spawn, Vector2.zero);
        // 			}
        // 
        // 			if (right.weaponType == (WeaponType)i)
        // 			{
        // 				if (right.pressed)
        // 					right.pressed = attackRight[i](weaponRightScript, right, ref right.tf, ref right.timer, ref right.time, -1,
        // 						right.spawn, Vector2.zero);
        // 			}
        // 		}
    }

    public float CalculateDamage(WeaponScript.Weapon weapon)
    {
        return Mathf.Round(stats.DoLeftDamage(weapon) * weapon.weaponChargeMod * 100f) / 100f;
    }

    bool Empty(WeaponScript weapon, WeaponContainer hand, ref Transform transform, ref float timer, ref float time, int direction, GameObject spawn, Vector2 mousePos)
    {
        return false;
    }

    bool Swing(WeaponScript weaponScript, WeaponContainer hand, ref Transform transform, ref float timer, ref float time, int direction, GameObject spawn, Vector2 mousePos)
    {
        Vector3 a = from;
        Vector3 b = to * weaponScript.weapon.swingDistance;
        if (direction < 0)
        {
            a = b;
            b = from;
        }

        Vector3 lerped = Vector3.Lerp(a, b, time / timer);
        transform.localEulerAngles = lerped;

        time += Time.deltaTime * Mathf.Abs(direction);
        //reset values once the attack is perfomed
        if (time >= timer)
        {
            weaponScript.weapon.swingDistance = 90.0f;
            return false;
        }
        return true;
    }

    bool Throw(WeaponScript weapon, WeaponContainer hand, ref Transform transform, ref float timer, ref float time, int direction, GameObject spawn, Vector2 mousePos)
    {
        return false;
    }

    bool Sling(WeaponScript weapon, WeaponContainer hand, ref Transform transform, ref float timer, ref float time, int direction, GameObject spawn, Vector2 mousePos)
    {
        return false;
    }
    bool AxeSwing(WeaponScript weaponScript, WeaponContainer hand, ref Transform transform, ref float timer, ref float time, int direction, GameObject spawn, Vector2 mousePos)
    {

        if (weaponScript.gameObject.GetComponent<PolygonCollider2D>().enabled == false)
            weaponScript.gameObject.GetComponent<PolygonCollider2D>().enabled = true;
        else
            weaponScript.gameObject.GetComponent<PolygonCollider2D>().enabled = false;

        if (weaponScript.gameObject.GetComponent<SpriteRenderer>().enabled == false)
            weaponScript.gameObject.GetComponent<SpriteRenderer>().enabled = true;
        else
            weaponScript.gameObject.GetComponent<SpriteRenderer>().enabled = false;

        return false;
    }
    bool Attach(WeaponScript weapon, WeaponContainer hand, ref Transform transform, ref float timer, ref float time, int direction, GameObject spawn, Vector2 mousePos)
    {
        return false;
    }

    bool Bash(WeaponScript weapon, WeaponContainer hand, ref Transform transform, ref float timer, ref float time, int direction, GameObject spawn, Vector2 mousePos)
    {
        return false;
    }

    bool Shoot(WeaponScript weapon, WeaponContainer hand, ref Transform transform, ref float timer, ref float time, int direction, GameObject spawn, Vector2 mousePos)
    {
        //laserscript.DrawRaycast(weaponRightScript.transform.position, ((Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition) - playerRigidBody.position).normalized);

        if (hand.weaponType == WeaponType.Shotgun)
        {
            float spread = weapon.weapon.spread;

            //mousePos = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 dir = (mousePos - playerRigidBody.position).normalized;
            Debug.DrawLine(playerRigidBody.position, mousePos, Color.green, 2.5f, false);

            for (int i = 0; i < weapon.weapon.bulletPershot; i++)
            {
                GameObject bullet = bulletPrefab;
                bullet.GetComponent<BulletScript>().direction = new Vector2(dir.x + Random.Range(-spread, spread), dir.y + Random.Range(-spread, spread)).normalized;

                bullet = Instantiate(weapon.bulletPrefab, weapon.gameObject.transform.position, Quaternion.Euler(0, 0, playerRigidBody.rotation + Vector2.SignedAngle(dir.normalized, bullet.GetComponent<BulletScript>().direction.normalized))) as GameObject;
                bullet.GetComponent<BulletScript>().SetParentWeapon(weapon.weapon);
                bullet.GetComponent<BulletScript>().damage = CalculateDamage(weapon.weapon) / weapon.weapon.bulletPershot;
                //bullet.GetComponent<bulletScript>().parentWeapon.Copy(weapon.weapon);

            }
        }
        else
        {
            mousePos = (Camera.main.ScreenToWorldPoint(Input.mousePosition));
            Vector2 dir = ((Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition) - playerRigidBody.position).normalized;
            //Check collision
            var bullet = Instantiate(weapon.bulletPrefab, weapon.gameObject.transform.position, transform.rotation) as GameObject;
            //bullet.GetComponent<bulletScript>().SetParentWeapon(weapon.weapon);
            bullet.GetComponent<BulletScript>().SetParentWeapon(weapon.weapon);
            bullet.GetComponent<BulletScript>().damage = CalculateDamage(weapon.weapon);
            bullet.GetComponent<BulletScript>().direction = dir;
        }
        //Debug.Log("SETTING PARENT " + bullet.GetComponent<bulletScript>().parentWeapon.weaponDamage);
        return false;
    }

    bool Beam(WeaponScript weapon, WeaponContainer hand, ref Transform transform, ref float timer, ref float time, int direction, GameObject spawn, Vector2 mousePos)
    {
        var laser = Instantiate(weaponRightScript.lasePrefab, weaponRightScript.gameObject.transform.position, weaponRightScript.gameObject.transform.rotation) as GameObject;

        LaserScript laserscript = laser.GetComponent<LaserScript>();
        laser.GetComponent<LaserScript>().damage = CalculateDamage(weapon.weapon);
        laserscript.DrawRaycast(weapon.transform.position, (mousePos - playerRigidBody.position).normalized);


        //RaycastHit2D hit = Physics2D.Raycast(weapon.transform.position, Vector2.up, 5);
        Debug.DrawRay(weapon.transform.position, Vector2.up, Color.blue);
        return false;

    }
    bool RecallShot(WeaponScript weapon, WeaponContainer hand, ref Transform transform, ref float timer, ref float time, int direction, GameObject spawn, Vector2 mousePos)
    {

        Vector2 dir = (mousePos - playerRigidBody.position).normalized;
        GameObject orb = bulletPrefab;

        orb = Instantiate(weapon.bulletPrefab, weapon.gameObject.transform.position, transform.rotation) as GameObject;
        orb.GetComponent<BulletScript>().SetParentWeapon(weapon.weapon);
        orb.GetComponent<BulletScript>().damage = CalculateDamage(weapon.weapon);
        orb.GetComponent<BulletScript>().direction = dir;
        StartCoroutine(Wait(orb));

        return false;
    }
    public IEnumerator Wait(GameObject orb)
    {
        yield return new WaitForSeconds(0.3f);
        if (orb != null)
        {
            float initialSpeed = orb.GetComponent<BulletScript>().bulletSpeed;
            orb.GetComponent<BulletScript>().bulletSpeed = 0;
            yield return new WaitForSeconds(0.5f);
            Vector2 dirToPlayer = (orb.transform.position - transform.position).normalized;
            orb.GetComponent<BulletScript>().bulletSpeed = initialSpeed;
            orb.GetComponent<BulletScript>().direction = -dirToPlayer;
        }
    }

    public float Remap(float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }
}