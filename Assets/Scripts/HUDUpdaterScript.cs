﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDUpdaterScript : SerializedMonoBehaviour
{
	[TabGroup("References")]
    public Stats stats;

	// UI
	[SerializeField, TabGroup("References")]
	private Text healthText;
	[SerializeField, TabGroup("References")]
	private Text healthBlinkText;
	[SerializeField, TabGroup("References")]
	private Transform heartBeatTransform;
	[SerializeField, TabGroup("References")]
	private Image energyFill;
	[SerializeField, TabGroup("References")]
	private GameObject flatLine;
	[SerializeField, TabGroup("References")]
	private GameObject healthWavesMask;

	[TabGroup("Values")]
    public float maxHealth;
	[TabGroup("Values")]
    public float currentHealth;
	[TabGroup("Values")]
    public float healthRegenPerSecond;

	[TabGroup("Values")]
    public float maxEnergy;
	[TabGroup("Values")]
    public float currentEnergy;
	[TabGroup("Values")]
    public float energyRegenPerSecond;

    //  HeartRate
	[TabGroup("Values")]
    public float waveScale;
	[TabGroup("Values")]
    public float heartRate;
	[TabGroup("Values")]
    float mapHealthToBpm;

	[TabGroup("Values")]
    private float energyRatio;

    // colors
	[TabGroup("Values")]
    public Color energyLowColor;
	[TabGroup("Values")]
    public Color energyNormalColor;

    public Image LeftReload;
    public Image RightReload;
    PlayerCombat player;
    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerCombat>();
        maxHealth = stats.MaxHealth;
        currentHealth = stats.CurrentHealth;
        maxEnergy = stats.MaxEnergy;
        currentEnergy = stats.CurrentEnergy;
        energyRatio = (currentEnergy / maxEnergy);
    }

    public void UpdateHealthHud()
    {
        LeftReload.fillAmount = 1 - player.leftFireRateTimer / player.weaponLeftScript.weapon.fireRate;
        RightReload.fillAmount = 1 - player.rightFireRateTimer / player.weaponRightScript.weapon.fireRate;
        // update text for the health 
        healthText.text = (Mathf.FloorToInt(currentHealth).ToString());
        healthBlinkText.text = healthText.text;

        // heart rate
        mapHealthToBpm = Remap(currentHealth, 1, maxHealth, 200, 60);
        heartRate = mapHealthToBpm;

        heartBeatTransform.localScale = new Vector3(Remap(heartRate, 200, 60, 4, waveScale), heartBeatTransform.localScale.y, heartBeatTransform.localScale.z);

        //you dead
        if (currentHealth <= 0)
        {
            OnDeath();
            //flatLine
        }
        else if (currentHealth >= maxHealth)
        {
            heartRate = 60;
            heartBeatTransform.localScale = Vector3.one * waveScale;
        }
    }

    public void OnDeath()
    {
        currentHealth = 0;
        heartRate = 0;
        flatLine.SetActive(true);
        healthWavesMask.SetActive(false);
        healthRegenPerSecond = 0;
        energyRegenPerSecond = 0;
    }

    public void UpdateEnergyHud()
    {
        //  update for energy UI
        energyRatio = (currentEnergy / maxEnergy);
        energyFill.fillAmount = energyRatio;

    }

    //  increase the maximum health
    public void IncreaseMaxHealth(float additionalHealth)
    {
        maxHealth += additionalHealth;
        currentHealth += additionalHealth;
    }

    //  decrease the current health         --  taking damage
    public void DecreaseCurrentHealth(float damageTaken)
    {
        StartCoroutine(DamageBlink());
        Debug.Log("You took " + damageTaken + " damage!");
        currentHealth -= damageTaken;

        if (currentHealth < 0)
        {
            currentHealth = 0;
            Debug.Log("yeet");
        }
            
    }

    //  increase the current health         -- drinking poition?
    public void IncreaseCurrentHealth(float healingDone)
    {
        currentHealth += healingDone;

        if (currentHealth > maxHealth)
            currentHealth = maxHealth;
    }

    //  regen hp over delta time called in update 
    public void HealthRegen(float healthRegen)
    {
        float healthgain = healthRegen * Time.fixedDeltaTime;
        currentHealth += healthgain;

        if (currentHealth > maxHealth)
            currentHealth = maxHealth;
    }

    // increease the regen persecond 
    public void IncreaseHealthRegenPerSecond(float regen)
    {
        healthRegenPerSecond += regen;
    }

    //  decrease the regen per second
    public void DecreaseHealthRegenPerSecond(float regen)
    {
        healthRegenPerSecond -= regen;
    }

    public float Remap(float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }

    //  to increase the maximum number of energy
    public void IncreaseMaxEnergy(float additionalEnergy)
    {
        maxEnergy += additionalEnergy;
    }

    //  increase a flat value of enerygy            ------ drinking potion?
    public void IncreaseCurrentEnergy(float energyRegen)
    {
        currentEnergy += energyRegen;

        if (currentEnergy > maxEnergy)
            currentEnergy = maxEnergy;
    }


    //  set how much energy is used and check if you have enough energy
    public void DecreaseCurrentEnergy(float energyUsed)
    {
        if (energyUsed > currentEnergy)
        {
            Debug.Log("Not enough Energy");
        }
        else
        {
            currentEnergy -= energyUsed;
        }
    }

    //  set the regen amount per delta time
    //  called in update for not with regenpersecond passing through 
    public void EnergyRegen(float energyRegen)
    {
        float energygain = energyRegen * Time.fixedDeltaTime;
        currentEnergy += energygain;

        if (currentEnergy > maxEnergy)
            currentEnergy = maxEnergy;
    }

    //  increase the regenpersecond value
    public void IncreaseEnergyRegenPerSecond(float regenValue)
    {
        energyRegenPerSecond += regenValue;
    }

    //  decrease regenpersecond value
    public void DecreaseEnergyRegenPerSecond(float regenValue)
    {
        energyRegenPerSecond -= regenValue;
    }

    private void FixedUpdate()
    {
        maxHealth = stats.MaxHealth;
        currentHealth = stats.CurrentHealth;
        maxEnergy = stats.MaxEnergy;
        currentEnergy = stats.CurrentEnergy;
        energyRatio = (currentEnergy / maxEnergy);
        UpdateHealthHud();

        HealthRegen(healthRegenPerSecond);

        UpdateEnergyHud();

        EnergyRegen(energyRegenPerSecond);
    }
    public IEnumerator DamageBlink()
    {
        healthBlinkText.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        healthBlinkText.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.05f);
        healthBlinkText.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        healthBlinkText.gameObject.SetActive(false);
    }
    // Update is called once per frame
    void Update ()
    {
		
    }
}
