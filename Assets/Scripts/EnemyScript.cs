﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyScript : MonoBehaviour
{
    public enum EnemyType
    {
        Chargie = 0,
        Cuip = 1,
        BossA = 2,
        BossB = 3,
        Length = 4
    }

    [System.Serializable]
    public class Enemy
    {
        public float currentHealth;
        public float maxHealth;
        public GameObject particle;
        public EnemyType enemyType;
        public float stunTime;
        //stun, knockbacks, etc... will have more impact the bigger this is
        public float impairmentWeakness;
        public Color color;
        public SpriteRenderer blinkSprite;
        public GameObject damageTextPrefab;
        public bool isBoss;
    }

	[SerializeField]
   
	public GameObject player;
	public PlayerCombat playercombat;
	[SerializeField]
	public GameObject tempHudPlayerObject;
	public Enemy enemy = new Enemy();
	public Stats enemyStats;
	public ProgressionScript progress;
    public int currentLevel;

	private void Awake()
	{
		enemyStats = GetComponent<Stats>();
	}
	// Use this for initialization
	void Start()
	{
        currentLevel = GameObject.Find("Level").GetComponent<LevelGeneration>().currentLevel;
		player = GameObject.FindGameObjectWithTag("Player");
		playercombat = player.GetComponent<PlayerCombat>();
        progress = playercombat.GetComponent<ProgressionScript>();
		tempHudPlayerObject = GameObject.FindGameObjectWithTag("HUD");
		enemy.color = GetComponent<SpriteRenderer>().color;
		enemy.currentHealth = enemyStats.CurrentHealth;
		enemy.currentHealth = enemyStats.MaxHealth;
	}

	public void AddMaxHealth(float max)
	{
		enemy.maxHealth += max;
       
    }

	public void DecreaseMaxHealth(float max)
	{
		enemy.maxHealth -= max;
       
    }

	public void AddCurrentHealth(float health)
	{
        enemyStats.CurrentHealth += health;
        //enemy.currentHealth += health;
	}

	public void DecreaseCurrentHealth(float health)
	{
        enemyStats.CurrentHealth -= health;
        //enemy.currentHealth -= health;
	}

	public void DamageTaken(WeaponScript.Weapon weapon)
	{
		float roundedDamage = playercombat.CalculateDamage(weapon);
		DecreaseCurrentHealth(roundedDamage);
		Debug.Log("Enemy took " + roundedDamage + " damage!");
		//Instantiate(enemy.damageText, transform.position, Quaternion.identity);

		//Spawn particle effect on when damage is taken
		GameObject particle = Instantiate(enemy.particle, transform.position, Quaternion.identity);
		particle.GetComponent<ParticleManager>().enemyColor = enemy.color;

		GameObject damageText = Instantiate(enemy.damageTextPrefab, transform.localPosition, Quaternion.identity);

		foreach (Text t  in damageText.GetComponentsInChildren<Text>())
		{
			t.text = roundedDamage.ToString("F0");
		}
		//particle.transform.parent = gameObject.transform;
		//CheckHealth();
		StartCoroutine(IFrame(0.2f));
		//Stop enemy's movement for a split second before returning to initial speed
		
	   // StartCoroutine(DamageBlink());
	}

    public List<GameObject> damageTextList = new List<GameObject>();
	public void BulletDamageTaken(float damage)
	{
       
		float roundedDamage = (damage * 100f) / 100f;
		DecreaseCurrentHealth(roundedDamage);
		//Spawn particle effect on when damage is taken
		GameObject particle = Instantiate(enemy.particle, transform.position, Quaternion.identity);
		particle.GetComponent<ParticleManager>().enemyColor = enemy.color;


        //If this enemy already has a damage text, edit it and make it last longer
        if (this.damageTextList.Count >= 1)
        {
            damageTextList[0].GetComponentInChildren<TextAnimation>().UpdateText(roundedDamage);
            //Gives a reference to this enemy for the damage text/canvas
            damageTextList[0].GetComponentInChildren<TextAnimation>().parentObj = this.gameObject;
            damageTextList[0].GetComponent<DestroyObject>().parentObj = this.gameObject;

            damageTextList[0].GetComponentInChildren<DestroyObject>().time = 0.6f;
        }
        //Create a new damage text prefab if this enemy hasn't been hit recently
        else
        {
            GameObject damageText = Instantiate(enemy.damageTextPrefab, transform.localPosition, Quaternion.identity);
            damageText.GetComponent<DestroyObject>().parentObj = this.gameObject;
            damageTextList.Add(damageText);

            //Gives a reference to this enemy for the damage text/canvas
            damageTextList[0].GetComponentInChildren<TextAnimation>().parentObj = this.gameObject;
            damageTextList[0].GetComponentInChildren<TextAnimation>().UpdateText(roundedDamage);
           
        }
	// have a counter, if the enemy gets hit again before timer runs out, add damage to the damageTextInstead of creating another
	}
    
	public void CheckHealth()
	{
        enemy.currentHealth = enemyStats.CurrentHealth;
		if (enemy.currentHealth <= 0)
		{
            enemyStats.CurrentHealth = 0;
            // Janky method to remove the wall from the boss room
            try
			{

				if (gameObject.GetComponent<BossAScript>() != null)
				{
					gameObject.GetComponent<BossAScript>().ResetBulletPrefab();
				}

				GetComponent<GeneralBossScript>().lg.DestroyWall();
			}
			catch(System.Exception)
			{
			}
            try
            {
                GetComponent<BossAScript>().UpdateUIHealthBar();
            }
            catch (System.Exception)
            {
            }

            progress.CalculateEnemyKill(currentLevel, enemy);
            Destroy(gameObject);
		}
	  
	}

	// Update is called once per frame
	void Update()
	{
		CheckHealth();
	}

	//make enemy "un-hittable" for short time after getting hit to avoid collision repetition from one attack 
	//If we still want them to attack then leave 'isTrigger' colliders active (but I think all collider off is good)
	public IEnumerator IFrame(float iFrameTime)
	{
		GetComponent<Collider2D>().enabled = false;
		yield return new WaitForSeconds(iFrameTime);
		GetComponent<Collider2D>().enabled = true;
	}
}
