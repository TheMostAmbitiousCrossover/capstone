﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilityPointsSystem : MonoBehaviour
{

    private int abilityPoint;
    [SerializeField] private Text abilityPointText;

    public void AddAbilityPoints(int ap)
    {
        this.abilityPoint += ap;
    }

    private void Update()
    {
        // place into text how many ability points you have remaining and display it as a text 
        abilityPointText.text = abilityPoint.ToString("00"); 
    }

    public int GetAbilityPoint()
    {
        return abilityPoint;
    }
}