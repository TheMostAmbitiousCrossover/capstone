﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemContainerScript : MonoBehaviour
{
	public Item item;

	public void SetItem(Item localItem)
	{
		this.item = localItem;
	}

	public Item GetItem()
	{
		return this.item;
	}

}
