﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

public class ScoreBoard : SerializedMonoBehaviour
{

    [SerializeField]
    GameObject scoreText;

    [SerializeField]
    Text totalScoreText;

    public GameObject text;

    Transform scoreTextTransform;
    Transform keyTextTransform;
    Transform valueTextTransform;

    Text keyText;
    Text valueText;

    ProgressionScript progress;

	// Use this for initialization
	void Start ()
    {
        progress = GameObject.FindGameObjectWithTag("Player").GetComponent<ProgressionScript>();

        scoreTextTransform = scoreText.transform;
        keyTextTransform = scoreTextTransform.GetChild(0);
        valueTextTransform = scoreTextTransform.GetChild(1);

        keyText= keyTextTransform.GetComponent<Text>();
        valueText= valueTextTransform.GetComponent<Text>();

        MakeScore();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void MakeScore()
    {
        foreach (KeyValuePair<string, float> pair in progress.progress)
        {
            if (pair.Value > 0 || pair.Key.Equals(progress.levelsTravelledScoreString))
            {
                if ((!pair.Key.Equals(progress.bossKillCountString)) && (!pair.Key.Equals(progress.enemyKillCountString)) && (!pair.Key.Equals(progress.levelsTravelledString)))
                {
                    if (pair.Key.Equals(progress.enemyKillScoreString))
                    {
                        keyText.text = pair.Key;
                        valueText.text = pair.Value.ToString("F0") + " (" + progress.enemyKillCount + ")";
                    }
                    else if (pair.Key.Equals(progress.bossKillsString))
                    {
                        keyText.text = pair.Key;
                        valueText.text = pair.Value.ToString("F0") + " (" + progress.bossKillCount + ")";
                    }
                    else if (pair.Key.Equals(progress.levelsTravelledScoreString))
                    {
                        keyText.text = pair.Key;
                        valueText.text = pair.Value.ToString("F0") + " (" + progress.levelsTravelled + ")";
                    }
                    else
                    {
                        keyText.text = pair.Key;
                        valueText.text = pair.Value.ToString("F0");
                    }

                    Instantiate(scoreText, transform);
                }
            }
        }

        totalScoreText.text = progress.totalScore.ToString("F0");
    }
}

