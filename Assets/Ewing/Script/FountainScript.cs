﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FountainScript : MonoBehaviour {

    bool uiOpened = false;
    bool nearFountain = false;
    public GameObject restManager;
    public GameObject panel;
    public GameObject player;
    public Stats stats;

    // Use this for initialization
    void Start()
    {
        restManager = GameObject.Find("RestManager");
        panel = restManager.GetComponent<RestManager>().healPanel;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && nearFountain && !uiOpened)
        {
            OpenUIFountain();
        }

        else if ((Input.GetKeyDown(KeyCode.E) || !nearFountain) && uiOpened)
        {
            CloseUIFountain();
        }

        else if (!nearFountain && uiOpened)
        {
            CloseUIFountain();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        nearFountain = true;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        nearFountain = false;
    }

    void OpenUIFountain()
    {
        uiOpened = true;
        panel.SetActive(true);
    }

    void CloseUIFountain()
    {
        uiOpened = false;
        panel.SetActive(false);
    }

    public void Cancel()
    {
        panel.SetActive(false);
    }

    public void Heal()
    {
        stats.CurrentHealth = stats.MaxHealth;
        uiOpened = false;
    }
}
