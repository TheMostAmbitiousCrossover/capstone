﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class PlayerStats : SerializedMonoBehaviour
{
	Stats playerStats;
	public GameObject gameOverPanel;
	public LevelGeneration lg;

    ProgressionScript progress;
	//private readonly float unarmedScale			= 0.2f;
	//private readonly float swordScale			= 0.4f;
	//private readonly float leftJavalinScale		= 0.3f;
	//private readonly float rightJavalinScale	= 0.5f;
	//private readonly float whipScale			= 0.4f;
	//private readonly float leftAxeScale			= 0.6f;
	//private readonly float rightAxeScale		= 0.6f;
	//private readonly float shieldScale			= 0.4f;
	private readonly float pistolScale			= 0.5f;
	private readonly float shotgunScale			= 0.15f;
	private readonly float rocketScale			= 1.0f;
	private readonly float machineScale			= 0.3f;
	private readonly float raygunScale			= 0.7f;
    private readonly float recallGunScale       = 0.5f;

    [TabGroup("Left")]
	public WeaponScript.Weapon left;
	[TabGroup("Right")]
	public WeaponScript.Weapon right;

	[TabGroup("Left")]
	public float leftMinAtt;
	[TabGroup("Left")]
	public float leftMaxAtt;
	[TabGroup("Right")]
	public float rightMinAtt;
	[TabGroup("Right")]
	public float rightMaxAtt;

	public float energyRegenSpeed = 15f;
	public float healthScaling = 10f;

	public float CalculateMaxHealth()
	{
		return playerStats.Endurance * healthScaling;
	}

	void PlayerStartingStats()
	{   

		playerStats.Strength = 5;
		playerStats.Dexerity = 10;
		playerStats.Endurance = 500;
		playerStats.Agility = 5;
		playerStats.MaxEnergy = 100;
		playerStats.CurrentEnergy = playerStats.MaxEnergy;
		playerStats.MaxHealth = CalculateMaxHealth();
		playerStats.CurrentHealth = playerStats.MaxHealth;
		//playerStats.EnergyConsumption
		playerStats.EnergyRegen = energyRegenSpeed * playerStats.MaxEnergy / 100f;
	}

	void Start()
	{
		left = GetComponent<PlayerCombat>().weaponLeftScript.weapon;
		right = GetComponent<PlayerCombat>().weaponRightScript.weapon;
        progress = GetComponent<ProgressionScript>();
		//Debug.Log("PlayerStat");
		playerStats = GetComponent<Stats>();
		PlayerStartingStats();

		//use this function when swap works for now if no swapping it only needs to call once 
		UpdateLeftAttackStats(left);
		UpdateRightAttackStats(right);
	}

	float ExpectedDamage(float attackMin, float attackMax)
	{
		return Random.Range(attackMin, attackMax);
	}

	public float DoLeftDamage(WeaponScript.Weapon weapon)
	{
		return UpdateLeftAttackStats(weapon);
	}

	public float DoRightDamage(WeaponScript.Weapon weapon)
	{
		return UpdateRightAttackStats(weapon);
	}

	private void Update()
	{
		if(playerStats.CurrentHealth <= 0)
		{
			foreach (MonoBehaviour script in 
				gameObject.GetComponents<MonoBehaviour>())
			{
				script.enabled = false;
			}
			gameObject.GetComponent<Rigidbody2D>().constraints = 
				RigidbodyConstraints2D.FreezePositionX | 
				RigidbodyConstraints2D.FreezePositionY | 
				RigidbodyConstraints2D.FreezeRotation;
			Time.timeScale = 0.1f;
			playerStats.CurrentHealth = 0;
            progress.CalculateTotalScore();
			gameOverPanel.SetActive(true);
			lg.currentLevel = 0;
			lg.lastLevel = 0;
		}
		RegenEnergy();
	}

	// This is not backwards compatible with melee weapons
	public float GetScaling(PlayerCombat.WeaponType type)
	{
		switch(type)
		{
			case PlayerCombat.WeaponType.Pistol:
				return pistolScale;
			case PlayerCombat.WeaponType.Shotgun:
				return shotgunScale;
			case PlayerCombat.WeaponType.Rocket:
				return rocketScale;
			case PlayerCombat.WeaponType.Machine:
				return machineScale;
			case PlayerCombat.WeaponType.Raygun:
				return raygunScale;
            case PlayerCombat.WeaponType.RecallGun:
                return recallGunScale;
        }
		Debug.Log("You called GetScaling in PlayerStats and the " +
			"type was not a ranged weapon so you do no damage");
		return 0;
	}

	float GetStatScaling(WeaponScript.Weapon weapon)
	{
		return (playerStats.Strength * weapon.strengthScaling) +
			   (playerStats.Dexerity * weapon.dexerityScaling) +
			   (playerStats.Endurance * weapon.enduranceScaling) +
			   (playerStats.Agility * weapon.agilityScaling);
	}

	public float NewUpdateAttackStats(WeaponScript.Weapon weapon, 
		ref float min, ref float max)
	{
		float scaling = GetScaling(weapon.weaponType);
		float statScaling = GetStatScaling(weapon);
		min = scaling * (weapon.minDmg + statScaling);
		max = scaling * (weapon.maxDmg + statScaling);
		float averageDmg = ExpectedDamage(min, max);
        return averageDmg; 
	}

	//not done
	public float UpdateLeftAttackStats(WeaponScript.Weapon weapon)
	{
		return NewUpdateAttackStats(weapon, ref leftMinAtt, ref leftMaxAtt);

// 		float damage = 0;
// 		float averageDmg = 0;
// 
// 		switch (weapon.weaponType)
// 		{
// 			case PlayerCombat.WeaponType.Unarmed:
// 				leftMinAtt = unarmedScale * (weapon.minDmg + playerStats.stats["strength"]);
// 				leftMaxAtt = unarmedScale * (weapon.maxDmg + playerStats.stats["strength"]);
// 				averageDmg = ExpectedDamage(weapon.minDmg, weapon.maxDmg);
// 				damage += unarmedScale * (averageDmg + playerStats.stats["strength"]);
// 				break;
// 
// 			case PlayerCombat.WeaponType.Sword:
// 				leftMinAtt = swordScale * (weapon.minDmg + playerStats.stats["strength"]);
// 				leftMaxAtt = swordScale * (weapon.maxDmg + playerStats.stats["strength"]);
// 				averageDmg = ExpectedDamage(weapon.minDmg, weapon.maxDmg);
// 				damage += swordScale * (averageDmg + playerStats.stats["strength"]);
// 				break;
// 
// 			case PlayerCombat.WeaponType.Javelin:
// 				leftMinAtt = leftJavalinScale * (weapon.minDmg + playerStats.stats["strength"]);
// 				leftMaxAtt = leftJavalinScale * (weapon.maxDmg + playerStats.stats["strength"]);
// 				averageDmg = ExpectedDamage(weapon.minDmg, weapon.maxDmg);
// 				damage += leftJavalinScale * (averageDmg + playerStats.stats["strength"]);
// 				break;
// 
// 			case PlayerCombat.WeaponType.Whip:
// 				leftMinAtt = whipScale * (weapon.minDmg + playerStats.stats["strength"]);
// 				leftMaxAtt = whipScale * (weapon.maxDmg + playerStats.stats["strength"]);
// 				averageDmg = ExpectedDamage(weapon.minDmg, weapon.maxDmg);
// 				damage += whipScale * (averageDmg + playerStats.stats["dexterity"]);
// 				break;
// 
// 			case PlayerCombat.WeaponType.Axe:
// 				leftMinAtt = leftAxeScale * (weapon.minDmg + playerStats.stats["strength"]);
// 				leftMaxAtt = leftAxeScale * (weapon.maxDmg + playerStats.stats["strength"]);
// 				averageDmg = ExpectedDamage(weapon.minDmg, weapon.maxDmg);
// 				damage += leftAxeScale * (averageDmg + playerStats.stats["strength"]);
// 				break;
// 
// 			case PlayerCombat.WeaponType.Shield:
// 				leftMinAtt = shieldScale * (weapon.minDmg + playerStats.stats["strength"]);
// 				leftMaxAtt = shieldScale * (weapon.maxDmg + playerStats.stats["strength"]);
// 				averageDmg = ExpectedDamage(weapon.minDmg, weapon.maxDmg);
// 				damage += shieldScale * (averageDmg + playerStats.stats["strength"]);
// 				break;
// 
// 			case PlayerCombat.WeaponType.Pistol:
// 				leftMinAtt = pistolScale * (weapon.minDmg + playerStats.stats["dexterity"]);
// 				leftMaxAtt = pistolScale * (weapon.maxDmg + playerStats.stats["dexterity"]);
// 				averageDmg = ExpectedDamage(weapon.minDmg, weapon.maxDmg);
// 				damage += pistolScale * (averageDmg + playerStats.stats["dexterity"]);
// 				break;
// 
// 			case PlayerCombat.WeaponType.Shotgun:
// 				leftMinAtt = shotgunScale * (weapon.minDmg + playerStats.stats["dexterity"]);
// 				leftMaxAtt = shotgunScale * (weapon.maxDmg + playerStats.stats["dexterity"]);
// 				averageDmg = ExpectedDamage(weapon.minDmg, weapon.maxDmg);
// 				damage += shotgunScale * (averageDmg + playerStats.stats["dexterity"]);
// 				break;
// 
// 			case PlayerCombat.WeaponType.Rocket:
// 				leftMinAtt = rocketScale * (weapon.minDmg + playerStats.stats["dexterity"]);
// 				leftMaxAtt = rocketScale * (weapon.maxDmg + playerStats.stats["dexterity"]);
// 				averageDmg = ExpectedDamage(weapon.minDmg, weapon.maxDmg);
// 				damage += rocketScale * (averageDmg + playerStats.stats["dexterity"]);
// 				break;
// 
// 			case PlayerCombat.WeaponType.Machine:
// 				leftMinAtt = machineScale * (weapon.minDmg + playerStats.stats["dexterity"]);
// 				leftMaxAtt = machineScale * (weapon.maxDmg + playerStats.stats["dexterity"]);
// 				averageDmg = ExpectedDamage(weapon.minDmg, weapon.maxDmg);
// 				damage += machineScale * (averageDmg + playerStats.stats["dexterity"]);
// 				break;
// 
// 			case PlayerCombat.WeaponType.Raygun:
// 				leftMinAtt = raygunScale * (weapon.minDmg + playerStats.stats["dexterity"]);
// 				leftMaxAtt = raygunScale * (weapon.maxDmg + playerStats.stats["dexterity"]);
// 				averageDmg = ExpectedDamage(weapon.minDmg, weapon.maxDmg);
// 				damage += raygunScale * (averageDmg + playerStats.stats["dexterity"]);
// 				break;
// 
// 			default:
// 				Debug.Log("you f*cked up");
// 				break;
// 		}
// 
// 		return damage;
	}
	public float UpdateRightAttackStats(WeaponScript.Weapon weapon)
	{
		return NewUpdateAttackStats(weapon, ref rightMinAtt, ref rightMaxAtt);

// 		float damage = 0;
// 		float averageDmg = 0;
// 		switch (weapon.weaponType)
// 		{
// 			case PlayerCombat.WeaponType.Unarmed:
// 				rightMinAtt = unarmedScale * (weapon.minDmg + playerStats.stats["strength"]);
// 				rightMaxAtt = unarmedScale * (weapon.maxDmg + playerStats.stats["strength"]);
// 				averageDmg = ExpectedDamage(weapon.minDmg, weapon.maxDmg);
// 				damage += unarmedScale * (averageDmg + playerStats.stats["strength"]);
// 				break;
// 
// 			case PlayerCombat.WeaponType.Sword:
// 				rightMinAtt = swordScale * (weapon.minDmg + playerStats.stats["strength"]);
// 				rightMaxAtt = swordScale * (weapon.maxDmg + playerStats.stats["strength"]);
// 				averageDmg = ExpectedDamage(weapon.minDmg, weapon.maxDmg);
// 				damage += swordScale * (averageDmg + playerStats.stats["strength"]);
// 				break;
// 
// 			case PlayerCombat.WeaponType.Javelin:
// 				rightMinAtt = rightJavalinScale * (weapon.minDmg + playerStats.stats["strength"]);
// 				rightMaxAtt = rightJavalinScale * (weapon.maxDmg + playerStats.stats["strength"]);
// 				averageDmg = ExpectedDamage(weapon.minDmg, weapon.maxDmg);                
// 				damage += rightJavalinScale * (averageDmg + playerStats.stats["strength"]);
// 				break;
// 
// 			case PlayerCombat.WeaponType.Whip:
// 				rightMinAtt = whipScale * (weapon.minDmg + playerStats.stats["strength"]);
// 				rightMaxAtt = whipScale * (weapon.maxDmg + playerStats.stats["strength"]);
// 				averageDmg = ExpectedDamage(weapon.minDmg, weapon.maxDmg);
// 				damage += whipScale * (averageDmg + playerStats.stats["dexterity"]);
// 				break;
// 
// 			case PlayerCombat.WeaponType.Axe:
// 				rightMinAtt = rightAxeScale * (weapon.minDmg + playerStats.stats["strength"]);
// 				rightMaxAtt = rightAxeScale * (weapon.maxDmg + playerStats.stats["strength"]);
// 				averageDmg = ExpectedDamage(weapon.minDmg, weapon.maxDmg);
// 				damage += rightAxeScale * (averageDmg + playerStats.stats["strength"]);
// 				break;
// 
// 			case PlayerCombat.WeaponType.Shield:
// 				rightMinAtt = shieldScale * (weapon.minDmg + playerStats.stats["strength"]);
// 				rightMaxAtt = shieldScale * (weapon.maxDmg + playerStats.stats["strength"]);
// 				averageDmg = ExpectedDamage(weapon.minDmg, weapon.maxDmg);                
// 				damage += shieldScale * (averageDmg + playerStats.stats["strength"]);
// 				break;
// 
// 			case PlayerCombat.WeaponType.Pistol:
// 				rightMinAtt = pistolScale * (weapon.minDmg + playerStats.stats["dexterity"]);
// 				rightMaxAtt = pistolScale * (weapon.maxDmg + playerStats.stats["dexterity"]);
// 				averageDmg = ExpectedDamage(weapon.minDmg, weapon.maxDmg);                
// 				damage += pistolScale * (averageDmg + playerStats.stats["dexterity"]);
// 				break;
// 
// 			case PlayerCombat.WeaponType.Shotgun:
// 				rightMinAtt = shotgunScale * (weapon.minDmg + playerStats.stats["dexterity"]);
// 				rightMaxAtt = shotgunScale * (weapon.maxDmg + playerStats.stats["dexterity"]);
// 				averageDmg = ExpectedDamage(weapon.minDmg, weapon.maxDmg);              
// 				damage += shotgunScale * (averageDmg + playerStats.stats["dexterity"]);
// 				break;
// 
// 			case PlayerCombat.WeaponType.Rocket:
// 				rightMinAtt = rocketScale * (weapon.minDmg + playerStats.stats["dexterity"]);
// 				rightMaxAtt = rocketScale * (weapon.maxDmg + playerStats.stats["dexterity"]);
// 				averageDmg = ExpectedDamage(weapon.minDmg, weapon.maxDmg);             
// 				damage += rocketScale * (averageDmg + playerStats.stats["dexterity"]);
// 				break;
// 
// 			case PlayerCombat.WeaponType.Machine:
// 				rightMinAtt = machineScale * (weapon.minDmg + playerStats.stats["dexterity"]);
// 				rightMaxAtt = machineScale * (weapon.maxDmg + playerStats.stats["dexterity"]);
// 				averageDmg = ExpectedDamage(weapon.minDmg, weapon.maxDmg);               
// 				damage += machineScale * (averageDmg + playerStats.stats["dexterity"]);
// 				break;
// 
// 			case PlayerCombat.WeaponType.Raygun:
// 				rightMinAtt = raygunScale * (weapon.minDmg + playerStats.stats["dexterity"]);
// 				rightMaxAtt = raygunScale * (weapon.maxDmg + playerStats.stats["dexterity"]);
// 				averageDmg = ExpectedDamage(weapon.minDmg, weapon.maxDmg);                
// 				damage += raygunScale * (averageDmg + playerStats.stats["dexterity"]);
// 				break;
// 
// 			default:
// 				Debug.Log("you f*cked up");
// 				break;
// 		}
// 		return damage;
	}

	public void RegenEnergy()
	{
		if (playerStats.CurrentEnergy <= playerStats.MaxEnergy)
		{
			playerStats.CurrentEnergy += energyRegenSpeed * Time.deltaTime;
		}
	}

	public float GetEnergyConsumption()
	{
		return playerStats.EnergyConsumption;
	}

	public void UpdateHealth(float value)
	{
		if (value < 0)
		{
            //Shake camera if damage is taken. Possibilty to 
            // scale the duration and the scale of the shake based on damage.
            playerStats.GetComponentInParent<CameraMovementScript>().shakeAmount = 0.1f;
            playerStats.GetComponentInParent<CameraMovementScript>().shakeDuration = 0.3f;
		}
		playerStats.CurrentHealth += value;
		//playerStats.stats["currentHealth"] = playerStats.CurrentHealth;
	}
	public void UpdateEnergy(float value)
	{
		playerStats.CurrentEnergy += value;
		//playerStats.stats["currentEnergy"] = playerStats.CurrentEnergy;
	}

	public void AddOneStr()
	{
		ModifyStrength(1);
	}
	public void AddOneDex()
	{
		ModifyDexterity(1);
	}
	public void AddOneEnd()
	{
		ModifyEndurance(1);
	}
	public void AddOneAgi()
	{
		ModifyAgility(1);
	}

	public void ModifyStrength(float number)
	{
		playerStats.Strength += number;
	}
	public void ModifyEndurance(float number)
	{
		playerStats.Endurance += number;
		float oldMaxHealth = playerStats.MaxHealth;
		playerStats.MaxHealth = CalculateMaxHealth();
		playerStats.CurrentHealth += playerStats.MaxHealth - oldMaxHealth;
	}
	public void ModifyDexterity(float number)
	{
		playerStats.Dexerity += number;
	}
	public void ModifyAgility(float number)
	{
		playerStats.Agility += number;
	}
	// This will fail if we modify the endurance
	public void ModifyMaxHealth(float number)
	{
		playerStats.MaxHealth += number;
		playerStats.CurrentHealth += number;
	}
	public void ModifyMaxEnergy(float number)
	{
		playerStats.MaxEnergy += number;
	}

}
