﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class Stats : SerializedMonoBehaviour
{
	// These are names for common things in the dictionary
	// These should not normally be used, just there so names are synced
	// Use the Getters and setters if u want to change something in the 
	// dictionary
	public static readonly string strengthString			= "statStrength";
	public static readonly string dexerityString			= "statDexterity";
	public static readonly string enduranceString			= "statEndurance";
	public static readonly string agilityString				= "statAgility";
	public static readonly string currentHPString			= "healthCurrent";
	public static readonly string maxHPString				= "healthMax";
	public static readonly string currentEnergyString		= "energyCurrent";
	public static readonly string maxEnergyString			= "energyMax";
	public static readonly string energyRegenString			= "energyRegen";
	public static readonly string energyConsumptionString	= "energyConsumption";
	public static readonly string enemyDamageString			= "enemyDamage";
	public static readonly string fluxCurrency				= "fluxCurrency";
	public static readonly string fluxLetter				= "ƒ";

	public float _energyConsumption;
	public float _maxHealth;
	public float _enemyDamage;

    //[HideInInspector]

	// We initialize a dictionary the the default values, this should avoid the
	// the access error exceptions
	[ShowInInspector]
    private Dictionary<string, float> stats = 
		new Dictionary<string, float>()
	{
		{ strengthString,			0 },
		{ dexerityString,			0 },
		{ enduranceString,			0 },
		{ agilityString,			0 },
		{ currentHPString,			0 },
		{ maxHPString,				0 },
		{ currentEnergyString,		0 },
		{ maxEnergyString,			0 },
		{ energyRegenString,		0 },
		{ energyConsumptionString,  0 },
		{ enemyDamageString,		0 }
	};
    public float hp;
	//This is the regen based on max health (% of max health) It is not just +this float per second
	void Start()
	{

        //keep every thing lower case and no space(use under score for spaces)
        //don't add junk into dictionary
        //		stats.Add("strength", 0);
        // 		stats.Add("dexterity", 0);
        // 		stats.Add("endurance", 0);
        // 		stats.Add("agility", 0);
        // 		stats.Add("maxHealth", _maxHealth);
        // 		stats.Add("currentHealth",stats["maxHealth"]);
        // 		stats.Add("maxEnergy",0);
        // 		stats.Add("currentEnergy",stats["maxEnergy"]);
        // 		stats.Add("energyRegen", 0);
        //		stats.Add("energyConsumption", 0);
        // 		stats.Add("enemyDamage", _enemyDamage);

		InitializeStats();
	}

	public void InitializeStats()
	{
		MaxHealth = _maxHealth;
		CurrentHealth = MaxHealth;
		CurrentEnergy = MaxEnergy;
		EnergyConsumption = _energyConsumption;
		EnemyDamage = _enemyDamage;
        
	}
    private void Update()
    {
        hp = CurrentHealth;
    }
    private void CheckingIfPeopleIncorreclyUsedThis(string val)
	{
		if (val == strengthString ||
			val == dexerityString ||
			val == enduranceString ||
			val == agilityString ||
			val == currentHPString ||
			val == maxHPString ||
			val == currentEnergyString ||
			val == maxEnergyString ||
			val == energyRegenString ||
			val == energyConsumptionString ||
			val == enemyDamageString)
		{
			Debug.Log("Hey, u just tried to use this[val] on a predefined " +
				"value instead of using the one I provided! Don't");
		}
	}

    // Get Default Getters / Setters for common stuff
    // I'm writting super shorthand so we dont't have tons of empty brace lines
    public float this[string val]
    {
        get
        {
            CheckingIfPeopleIncorreclyUsedThis(val);
            return stats[val];
        }
        set
        {
            CheckingIfPeopleIncorreclyUsedThis(val);
            stats[val] = value;
        }
    }

    public float Strength
    {
        get { return stats[strengthString]; }
        set { stats[strengthString] = value; }
    }
    public float Dexerity
    {
        get { return stats[dexerityString]; }
        set { stats[dexerityString] = value; }
    }
    public float Endurance
    {
        get { return stats[enduranceString]; }
        set { stats[enduranceString] = value; }
    }
    public float Agility
    {
        get { return stats[agilityString]; }
        set { stats[agilityString] = value; }
    }
    public float CurrentHealth
    {
        get { return stats[currentHPString]; }
        set { stats[currentHPString] = value; }
    }
    public float MaxHealth
    {
        get { return stats[maxHPString]; }
        set { stats[maxHPString] = value; }
    }
    public float CurrentEnergy
    {
        get { return stats[currentEnergyString]; }
        set { stats[currentEnergyString] = value; }
    }
    public float MaxEnergy
    {
        get { return stats[maxEnergyString]; }
        set { stats[maxEnergyString] = value; }
    }
    public float EnergyRegen
    {
        get { return stats[energyRegenString]; }
        set { stats[energyRegenString] = value; }
    }
    public float EnergyConsumption
    {
        get { return stats[energyConsumptionString]; }
        set { stats[energyConsumptionString] = value; }
    }
    public float EnemyDamage
    {
        get { return stats[enemyDamageString]; }
        set { stats[enemyDamageString] = value; }
    }

    //public float CurrentLevel
    //{
    //    get { return stats[currentLevelString]; }
    //    set { stats[currentLevelString] = value; }
    //}
}