﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class ProgressionScript : SerializedMonoBehaviour {

    [ShowInInspector]
    public Dictionary<string, float> progress = new Dictionary<string, float>();
    public readonly float chargieBaseScore = 10;
    public readonly float cuipBaseScore = 15;
    public readonly float bossABaseScore = 50;
    public readonly float bossBBaseScore = 50;

    public readonly float levelScoreScaling = 3;
    public readonly float bossScoreScaling = 0.5f;
    public readonly float enemyScoreScaling = 0.2f;

    public float totalScore = 0;

    public readonly string enemyKillScoreString          = "enemyKillScore";
    public readonly string bossKillsString               = "bossKills";
    public readonly string levelsTravelledScoreString    = "levelsTravelledScore";
    public readonly string levelsTravelledString         = "levelsTravelled";
    public readonly string enemyKillCountString          = "enemyKillCount";
    public readonly string bossKillCountString           = "bossKillCount";

    private void Start()
    {
        progress.Add(enemyKillScoreString           , 0);
        progress.Add(bossKillsString                , 0);
        progress.Add(levelsTravelledString          , 0);
        progress.Add(levelsTravelledScoreString     , 0);
        progress.Add(enemyKillCountString           , 0);
        progress.Add(bossKillCountString            , 0);
    }

    public float enemyKillScore
    {
        get { return progress[enemyKillScoreString]; }
        set { progress[enemyKillScoreString] = value; }
    }

    public float bossKills
    {
        get { return progress[bossKillsString]; }
        set { progress[bossKillsString] = value; }
    }

    public float levelsTravelled
    {
        get { return progress[levelsTravelledString]; }
        set { progress[levelsTravelledString] = value; }
    }

    public float levelsTravelledScore
    {
        get { return progress[levelsTravelledScoreString]; }
        set { progress[levelsTravelledScoreString] = value; }
    }

    public float enemyKillCount
    {
        get { return progress[enemyKillCountString]; }
        set { progress[enemyKillCountString] = value; }
    }

    public float bossKillCount
    {
        get { return progress[bossKillCountString]; }
        set { progress[bossKillCountString] = value; }
    }

    public float GetBaseScore(EnemyScript.Enemy enemy)
    {
        switch (enemy.enemyType)
        {
            case EnemyScript.EnemyType.Chargie:
                return chargieBaseScore;
                
            case EnemyScript.EnemyType.Cuip:
                return cuipBaseScore;
                
            case EnemyScript.EnemyType.BossA:
                return bossABaseScore;
                
            case EnemyScript.EnemyType.BossB:
                return bossBBaseScore;
        }
        return 0;
    }

    public void CalculateEnemyKill(int level, EnemyScript.Enemy enemy)
    {
        if (!enemy.isBoss)
        {
            enemyKillScore += ((level * enemyScoreScaling) * GetBaseScore(enemy));
            enemyKillCount++;
        }
        else
        {
            bossKills += ((level * bossScoreScaling) * GetBaseScore(enemy));
            bossKillCount++;
        }
    }

    public void CalculateLevel()
    {
        levelsTravelled++;
        levelsTravelledScore = levelsTravelled * levelScoreScaling;
    }

    public void CalculateTotalScore()
    {
        float value = 0;

        foreach (KeyValuePair<string, float> key in progress)
        {
            if ((!key.Key.Equals(bossKillCountString)) && (!key.Key.Equals(enemyKillCountString)) &&(!key.Key.Equals(levelsTravelledString)))
            {
                value = key.Value;
                totalScore += value;
            }
        }
    }


    // Update is called once per frame
    void Update () {
		
	}
}
