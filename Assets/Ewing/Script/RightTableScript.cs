﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RightTableScript : MonoBehaviour
{ 
    public bool shopOpened = false;
    public bool nearShop = false;
    public GameObject restManager;
    public GameObject panel;

    private void Start()
    {
        restManager = GameObject.Find("RestManager");
        panel = restManager.GetComponent<RestManager>().shopRight;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && nearShop && !shopOpened)
        {
            openStore();
        }

        else if ((Input.GetKeyDown(KeyCode.E) || !nearShop) && shopOpened)
        {
            closeStore();
        }

        else if (!nearShop && shopOpened)
        {
            closeStore();
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        nearShop = true;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        nearShop = false;
    }

    void openStore()
    {
        shopOpened = true; 
        panel.SetActive(true);
    }

    void closeStore()
    {
        shopOpened = false;
        panel.SetActive(false);
    }
}
