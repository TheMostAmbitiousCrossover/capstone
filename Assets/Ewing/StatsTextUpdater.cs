﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

public class StatsTextUpdater : SerializedMonoBehaviour {

	[TabGroup("References")]
    public PlayerStats playerStats;

	[TabGroup("References")]
    public Text strText;
	[TabGroup("References")]
    public Text dexText;
	[TabGroup("References")]
    public Text endText;
	[TabGroup("References")]
    public Text agiText;
	[TabGroup("References")]
    public Text skillPointText;
	[TabGroup("References")]
    public Stats stats;

	[TabGroup("References")]
    public Text healthText;
	[TabGroup("References")]
    public Text energyText;

	[TabGroup("References")]
    public Text leftAttackText;
	[TabGroup("References")]
    public Text rightAttackText;

	[TabGroup("References")]
    public Text defenseText;

	[TabGroup("Values")]
    public float maxhealth;
	[TabGroup("Values")]
    public float currenthealth;

	[TabGroup("Values")]
    public float maxEnergy;
	[TabGroup("Values")]
    public float currentEnergy;

	[TabGroup("Values")]
    public float leftAttackMin;
	[TabGroup("Values")]
    public float leftAttackMax;

	[TabGroup("Values")]
    public float rightAttackMin;
	[TabGroup("Values")]
    public float rightAttackMax;
	[TabGroup("Values")]
    public float defense;

	[TabGroup("Values")]
    public float str;
	[TabGroup("Values")]
    public float dex;
	[TabGroup("Values")]
    public float end;
	[TabGroup("Values")]
    public float agi;

	[TabGroup("Values")]
    public float skillPoint;
    // Use this for initialization
    void Start ()
    {
        str = stats.Strength;
        dex = stats.Dexerity;
        end = stats.Endurance;
        agi = stats.Agility;

        maxhealth = stats._maxHealth;
        currenthealth = stats.CurrentHealth;

        maxEnergy = stats.MaxEnergy;
        currentEnergy = stats.CurrentEnergy;

        playerStats.NewUpdateAttackStats(playerStats.left, ref leftAttackMin, ref leftAttackMax);
        playerStats.NewUpdateAttackStats(playerStats.right, ref rightAttackMin, ref rightAttackMax);
    }

    public void UpdateStats()
    {
		str = stats.Strength;
		dex = stats.Dexerity;
		end = stats.Endurance;
		agi = stats.Agility;
        maxhealth = stats.MaxHealth;
        currenthealth = stats.CurrentHealth;
        maxEnergy = stats.MaxEnergy;
        currentEnergy = stats.CurrentEnergy;

        leftAttackMin = playerStats.leftMinAtt;
        leftAttackMax = playerStats.leftMaxAtt;

        playerStats.NewUpdateAttackStats(playerStats.left, ref leftAttackMin, ref leftAttackMax);
        playerStats.NewUpdateAttackStats(playerStats.right, ref rightAttackMin, ref rightAttackMax);
    }

    public void UpdateStr()
    {
        str = stats.Strength;
    }
    public void UpdateDex()
    {
        dex = stats.Dexerity;
    }
    public void UpdateEnd()
    {
        end = stats.Endurance;
    }
    public void UpdateAgi()
    {
        agi = stats.Agility;
    }

    private void FixedUpdate()
    {
        UpdateStats();
        strText.text = str.ToString();
        dexText.text = dex.ToString();
        endText.text = end.ToString();
        agiText.text = agi.ToString();
        healthText.text = currenthealth.ToString("F0") + " / " + maxhealth.ToString();
        energyText.text = currentEnergy.ToString("F0") + " / " + maxEnergy.ToString();
        leftAttackText.text = leftAttackMin.ToString("F0") + " ~ " + leftAttackMax.ToString("F0");
        rightAttackText.text = rightAttackMin.ToString("F0") + " ~ " + rightAttackMax.ToString("F0");
    }
}
