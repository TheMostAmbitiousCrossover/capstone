﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossDoorTrigger : MonoBehaviour {

    public GameObject tile1;
    public GameObject tile2;
    public GameObject tile3;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            tile1.SetActive(true);
            tile2.SetActive(true);
            tile3.SetActive(true);
			Destroy(gameObject);
        }
    }
}
