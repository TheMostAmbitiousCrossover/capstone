﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ArmorType
{
	Helmet = 0,
	Body = 1,
	Glove = 2,
	Shoe = 3
}

[System.Serializable]
public class ItemArmor : ItemEquipable {

	public ArmorType armorType;
	public float defenceValue;

	public ItemArmor()
	{

	}

	public ItemArmor(ItemArmor item)
	{
		this.id = GenerateID();
		this.name = item.name;
		this.image = item.image;
		this.itemType = item.itemType;
		this.width = item.width;
		this.height = item.height;
		this.x = item.x;
		this.y = item.y;
		this.description = item.description;
		this.equipped = item.equipped;
		this.strength = item.strength;
		this.dexerity = item.dexerity;
		this.endurance = item.endurance;
		this.agility = item.agility;
		this.armorType = item.armorType;
		this.defenceValue = item.defenceValue;
	}

	public ItemArmor(ItemArmor item, ItemType type)
	{
		this.id = GenerateID();
		this.name = item.name;
		this.image = item.image;
		this.itemType = type;
		this.width = item.width;
		this.height = item.height;
		this.x = item.x;
		this.y = item.y;
		this.description = item.description;
		this.equipped = item.equipped;
		this.strength = item.strength;
		this.dexerity = item.dexerity;
		this.endurance = item.endurance;
		this.agility = item.agility;
		this.armorType = item.armorType;
		this.defenceValue = item.defenceValue;
	}
}
