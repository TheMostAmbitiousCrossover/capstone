﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ListInventoryViewer : MonoBehaviour {

	public ListInventory inventory;
	public RectTransform listSlider;
	public GameObject listItemPrefab;

	public Image itemImage;
	public Text itemNameText;
	public Text itemDescriptionText;

	public GameObject equipTextGO;
	public Text equipText;

	[HideInInspector] public int selectedItemIdx;

	private readonly Vector3 baseTranslate = new Vector3(0, -70, 0) / 1080f;

	// Initial Position of the listSlider
	// We use this + listoffset to find the end Position
	private Vector3 listInitPos;

	// The Start and end times for lerping
	private Vector3 listStartPos;
	private Vector3 listEndPos;

	// Keep track of the lerp time
	private float lerpTime;

	// How long the lerp should be
	public float totalLerpTime;

	public bool shouldUpdate = true;

	public void Start()
	{
		listInitPos = listSlider.position;

		listStartPos = listInitPos;
		listEndPos = listInitPos;
		lerpTime = totalLerpTime;
	}

	public void DestroyAllSliderChildren()
	{
		foreach(Transform item in listSlider)
		{
			Destroy(item.gameObject);
		}
	}

	public void UpdateItems()
	{
		Vector3 itemTranslateScaled = baseTranslate * Screen.height;

		int i;
		for (i = 0; i < inventory.Count; ++i)
		{
			GameObject prefab = Instantiate(listItemPrefab, 
				listSlider.position + (itemTranslateScaled * i), 
				Quaternion.identity, listSlider);

			// This will break if we change the prefab
			Text text = prefab.transform.GetChild(0).GetComponent<Text>();
			text.text = inventory.GetItemListDisplayName(i);
		}

		// If we have no items
		if(i == 0)
		{
			return;
		}

		// We reset the item
		if(selectedItemIdx > i)
		{
			selectedItemIdx = i - 1;
		}

		UpdateSelectedItem();
	}

	void UpdateLerper()
	{
		listStartPos = listSlider.position;
		listEndPos = listInitPos + (-baseTranslate * selectedItemIdx * Screen.height);
		lerpTime = 0;
	}

	void UpdateSelectedItem()
	{
		UpdateLerper();
		Item selectedItem = inventory[selectedItemIdx];
		itemNameText.text = selectedItem.name;
		itemDescriptionText.text = selectedItem.InventoryDescription();
		itemImage.sprite = selectedItem.image;

		if(ItemDatabase.IsEquppable(selectedItem))
		{
			equipTextGO.SetActive(true);
			if(inventory.IsEquipped(selectedItem)) equipText.text = "Unequip";
			else equipText.text = "Equip";
		}
		else
		{
			equipTextGO.SetActive(false);
		}
	}

	public void OnEnable()
	{
		UpdateView();
	}

	public void UpdateView()
	{
		DestroyAllSliderChildren();
		UpdateItems();
	}

	void Update () {
		
		if(shouldUpdate)
		{
			UpdateView();
			shouldUpdate = false;
		}

		float pct = lerpTime / totalLerpTime;
		if(pct >= 1)
		{
			if(Input.GetKey(KeyCode.W))
			{
				selectedItemIdx = Modulo(selectedItemIdx - 1, inventory.Count);
				UpdateSelectedItem();
			}

			if(Input.GetKey(KeyCode.S))
			{
				selectedItemIdx = Modulo(selectedItemIdx + 1, inventory.Count);
				UpdateSelectedItem();
			}
		}
		pct = lerpTime / totalLerpTime;
		if (pct <= 1)
		{
			listSlider.position = Vector3.Lerp(listStartPos, listEndPos, pct);
			lerpTime += Time.deltaTime;
			if (lerpTime > totalLerpTime) lerpTime = totalLerpTime;
		}
	}

	int Modulo(int x, int m)
	{
		return (x % m + m) % m;
	}
}
