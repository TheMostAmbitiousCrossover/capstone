﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemEquipable : Item {

	[SerializeField, HideInInspector] public int equipped = -1;
	[SerializeField] public float strength;
	[SerializeField] public float dexerity;
	[SerializeField] public float endurance;
	[SerializeField] public float agility;

	public ItemEquipable()
	{

	}

	public ItemEquipable(ItemEquipable item)
	{
		this.id = GenerateID();
		this.name = item.name;
		this.image = item.image;
		this.itemType = item.itemType;
		this.width = item.width;
		this.height = item.height;
		this.x = item.x;
		this.y = item.y;
		this.equipped = item.equipped;
		this.strength = item.strength;
		this.dexerity = item.dexerity;
		this.endurance = item.endurance;
		this.agility = item.agility;
		this.description = item.description;
	}

	public override string InventoryDescription()
	{
		return description + "\n" +
			"Strength: " + strength + "\n" +
			"Dexerity: " + dexerity + "\n" +
			"Endurance: " + endurance + "\n" +
			"Agility: " + agility + "\n";
	}
}
