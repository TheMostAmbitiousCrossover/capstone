﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public abstract class ItemBase {

	public enum ItemType
	{
		IWeapon = 0,
		IArmor = 1,
		IOther = 2
	}

	protected static int currentID = 1;

	[SerializeField, HideInInspector] public int id;
	[SerializeField] public string name;
    [SerializeField] public Sprite image;
	[SerializeField, HideInInspector] public ItemType itemType;
    [SerializeField, HideInInspector] public int width;
	[SerializeField, HideInInspector] public int height;
	[SerializeField, HideInInspector] public int x;
	[SerializeField, HideInInspector] public int y;
	[SerializeField] public string description;

    public abstract void PerformAction();

	public abstract string InventoryDescription();

	public int GenerateID()
	{
		return currentID++;
	}
}
