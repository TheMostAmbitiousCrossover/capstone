﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryPanelControl : MonoBehaviour {

	public GameObject wholeWindow;
	public GameObject characterWindow;
	public GameObject inventoryWindow;

	public bool characterVisible;
	public bool inventoryVisible;

	public void Start()
	{
		characterVisible = false;
		inventoryVisible = false;
		UpdateInventoryWindows();
	}

	public void Update()
	{
		if(Input.GetKeyDown(KeyCode.I))
		{
			OpenInventory();
		}
		else if(Input.GetKeyDown(KeyCode.P))
		{
			OpenCharacter();
		}
	}

	public void OpenCharacter()
	{
		characterVisible = !characterVisible;
		inventoryVisible = false;
		UpdateInventoryWindows();
	}

	public void OpenInventory()
	{
		inventoryVisible = !inventoryVisible;
		characterVisible = false;
		UpdateInventoryWindows();
	}

	void UpdateInventoryWindows()
	{
		ShowWindow((characterVisible || inventoryVisible) ? true : false);
		ShowCharacter(characterVisible);
		ShowInventory(inventoryVisible);
	}

	void ShowWindow(bool val)
	{
		wholeWindow.SetActive(val);
	}

	void ShowCharacter(bool val)
	{
		characterWindow.SetActive(val);
	}

	void ShowNothing()
	{

	}

	void ShowInventory(bool val)
	{
		inventoryWindow.SetActive(val);
	}
}
