﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class ListEquipment : SerializedMonoBehaviour
{
	public ListInventory inventory;
	public HotbarItemViewer hotbar;

	// NOTE: These are references to the items in the inventory
	// Changing these will change the item in the inventory
	[SerializeField, TabGroup("Equipment"),
	ListDrawerSettings(ShowIndexLabels = true,
	ListElementLabelName = "name", Expanded = true)]
	public ItemEquipable[] equipment = new ItemEquipable[8];

	public const int LHAND	= 0;
	public const int RHAND	= 1;
	public const int HELM	= 2;
	public const int BODY	= 3;
	public const int GLOVE	= 4;
	public const int SHOE	= 5;
	public const int OLHAND	= 6;
	public const int ORHAND	= 7;

	public Item this[int val]
	{
		get
		{
			return equipment[val];
		}
		set
		{
			// Remove the equipped int from the item we are unequipping
			if(equipment[val] != null) equipment[val].equipped = -1;
			// Equip the new item (and simultaneously unequip the old item)
			equipment[val] = (ItemEquipable)value;

			// Change the equipped int on the new item if it exists
			if(equipment[val] != null) equipment[val].equipped = val;

			// Update the player stats

			hotbar.UpdateWeaponImages();
		}
	}

	public delegate bool EquipCheck(int slot, Item item, int enumValue);

	[HideInInspector] public EquipCheck[] checkDelegate;
	private readonly int[] equipmentEnumIdx = new int[]
	{
		(int) WeaponHandleType.Length,
		(int) WeaponHandleType.OneHand,
		(int) ArmorType.Helmet,
		(int) ArmorType.Body,
		(int) ArmorType.Glove,
		(int) ArmorType.Shoe,
		(int) WeaponHandleType.Length,
		(int) WeaponHandleType.OneHand
	};

	void Start () {
		SetDelegates();
	}
	
	public void SetDelegates()
	{
		checkDelegate = new EquipCheck[]
		{
			IsWeapon,
			IsWeapon,
			IsArmor,
			IsArmor,
			IsArmor,
			IsArmor,
			IsArmor,
			IsWeapon,
			IsWeapon
		};
	}

	void SwapEquipment(int idx1, int idx2)
	{
		ItemEquipable temp = equipment[idx1];

		equipment[idx1] = equipment[idx2];
		if(equipment[idx1] != null) equipment[idx1].equipped = idx1;

		equipment[idx2] = temp;
		if(equipment[idx2] != null) temp.equipped = idx2;
	}

	public void SwapMainHand()
	{
		SwapEquipment(LHAND, OLHAND);
		SwapEquipment(RHAND, ORHAND);
		hotbar.UpdateWeaponImages();
	}

	void Update () {
	}

	bool IsWeapon(int slot, Item item, int enumValue)
	{
		var weapon = item as ItemWeapon;
		return (weapon != null);
	}

	bool IsArmor(int slot, Item item, int enumValue)
	{
		var armor = item as ItemArmor;
		if (armor == null) return false;
		return ((int)armor.armorType == enumValue) ? true : false;
	}
	
	public bool CanPlaceIntoSlot(int slot, Item item)
	{
		return checkDelegate[slot](slot, item, equipmentEnumIdx[slot]);
	}
}
