﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorSwing : MonoBehaviour {

	public GameObject otherTrigger;
	public HingeJoint2D hj;
	public BoxCollider2D bc;
	public float direction;

	public void OnTriggerEnter2D(Collider2D collision)
	{
		if(collision.CompareTag("Player"))
		{
			JointMotor2D jm = hj.motor;
			jm.motorSpeed *= direction;
			hj.motor = jm;

			hj.useMotor = true;
			Destroy(bc);
			Destroy(otherTrigger);
			Destroy(gameObject);
		}
	}
}