﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class BlockInventory : MonoBehaviour {

	public Sprite fallbackSprite;
	public GameObject spawnSprite;
	public BlockEquipment equipment;
	public RectTransform inventoryTransform;
	public ItemDatabase itemDB;

	public RectTransform anchorFinder;

	public Vector2Int inventorySize = new Vector2Int(10, 4);

	[SerializeField] public List<Item> inventory = new List<Item>();
	public List<GameObject> spawnedSprites;
	public ArrayLayout data = new ArrayLayout();

	// set x & y to this to skip some functions
	private const int SKIP = -5;

	// When something has failed
	private const int FAIL = -1;

	void Start()
	{
		AddItem(ItemBase.ItemType.IWeapon, 0);
		AddItem(ItemBase.ItemType.IArmor, 0);
		AddItem(ItemBase.ItemType.IWeapon, 1);
		AddItem(ItemBase.ItemType.IWeapon, 1);
		AddItem(ItemBase.ItemType.IWeapon, 0);
	}

	void Update()
	{
		if(Input.GetKeyDown(KeyCode.R))
		{
			AddItem(ItemBase.ItemType.IOther, 0);
		}
		if(Input.GetKeyDown(KeyCode.T))
		{
			RemoveItem(0);
		}

		if(Input.GetButton("Fire1"))
		{
			Vector3 mousePos = Input.mousePosition;
			anchorFinder.position = mousePos;
			AnchorToCorners(anchorFinder);
		}
	}

	public void AddItem(ItemBase.ItemType type, int key)
	{
		var item = itemDB.GetItem(type, key);
		CheckItem(type, item);
		CheckInventory(item);
	}

	public void AddItem(ItemBase.ItemType type, string name)
	{
		var item = itemDB.GetItem(type, name);
		CheckItem(type, item);
		CheckInventory(item);
	}

	public void RemoveItem(int key)
	{
		if(key >= inventory.Count)
		{
			throw new System.ArgumentOutOfRangeException();
		}
		var item = inventory[key];
		FreeSpace(item.x, item.y, item.width, item.height);
		Destroy(spawnedSprites[key].gameObject);
		spawnedSprites.RemoveAt(key);
		inventory.RemoveAt(key);

		try
		{
			equipment.ChangeEquipment(item, FAIL);
		}
		catch (System.Exception)
		{
		}

		item = null;
	}

	// Check the item is set correctly
	void CheckItem(ItemBase.ItemType type, Item item)
	{
		if(System.String.IsNullOrEmpty(item.name))
		{
			Debug.LogWarning("This item has no name, please add one");
		}
		if(!item.image)
		{
			Debug.LogWarning("This item has no image, please add one");
		}
		if(item.itemType != type)
		{
			Debug.LogWarning("This item's type is not equal to the one supplied");
		}
		if(item.width == 0)
		{
			Debug.LogWarning("This item's width is 0");
		}
		if(item.height == 0)
		{
			Debug.LogWarning("This item's height is 0");
		}
	}

	void CheckInventory(Item item)
	{
		Vector2Int freeSpot = new Vector2Int();

		if (CheckAddable(item, ref freeSpot))
		{
			OccupySpace(freeSpot.x, freeSpot.y, item.width, item.height);
			item.x = freeSpot.x;
			item.y = freeSpot.y;

			inventory.Add(item);
			InstantiateItem(inventory.Count - 1);
		}
	}

	bool GetData(int row, int col)
	{
		return data[col][row];
	}

	void SetData(int row, int col, bool val)
	{
		data[col][row] = val;
	}

	bool IsOverlapping(int i, int j, int width, int height)
	{
		for (int x = 0; x < width; ++x)
		{
			for (int y = 0; y < height; ++y)
			{
				if (GetData(i + x, j + y))
				{
					return true;
				}
			}
		}
		return false;
	}

	bool IsOverlappingIgnoreSelf(int i, int j, 
		int width, int height, int startX, int startY)
	{
		FreeSpace(startX, startY, width, height);

		for (int x = 0; x < width; ++x)
		{
			for (int y = 0; y < height; ++y)
			{
				if (GetData(i + x, j + y))
				{
					OccupySpace(startX, startY, width, height);
					return true;
				}
			}
		}
		OccupySpace(startX, startY, width, height);

		return false;
	}

	bool IsOverlappingIgnoreRect(int i, int j, int width, int height, 
		int k, int l, int width2, int height2)
	{
		FreeSpace(k, l, width2, height2);

		for (int x = 0; x < width; ++x)
		{
			for (int y = 0; y < height; ++y)
			{
				if (GetData(i + x, j + y))
				{
					OccupySpace(k, l, width2, height2);
					return true;
				}
			}
		}
		OccupySpace(k, l, width2, height2);

		return false;
	}

	Vector2Int GetFreeSpot(int width, int height)
	{
		for(int i = 0; i < inventorySize.x - (width - 1); ++i)
		{
			for(int j = 0; j < inventorySize.y - (height - 1); ++j)
			{
				if (!IsOverlapping(i, j, width, height))
				{
					return new Vector2Int(i, j);
				}
			}
		}
		return new Vector2Int(FAIL, FAIL);
	}

	void OccupySpace(int i, int j, int width, int height)
	{
		// We do this so we can skip the space freeing
		if (i == SKIP && j == SKIP) return;

		for (int x = 0; x < width; ++x)
		{
			for (int y = 0; y < height; ++y)
			{
				SetData(i + x, j + y, true);
			}
		}
	}

	void FreeSpace(int i, int j, int width, int height)
	{
		// We do this so we can skip the space freeing
		if (i == SKIP && j == SKIP) return;

		for (int x = 0; x < width; ++x)
		{
			for (int y = 0; y < height; ++y)
			{
				if(!GetData(i + x, j + y))
				{
					Debug.LogWarning("Attempting to clear an empty slot at\nx:" 
						+ (i + x) + " y:" + (j + y) );
				}
				SetData(i + x, j + y, false);
			}
		}
	}

	bool CheckAddable(Item item, ref Vector2Int freeSpot)
	{
		freeSpot = GetFreeSpot(item.width, item.height);
		if (freeSpot.x == FAIL || freeSpot.y == FAIL)
		{
			Debug.LogError("There is no free room to add item " + item.name);
			return false;
		}
		return true;
	}

	void InstantiateItem(int idx)
	{
		var item = inventory[idx];
		
		// Create the object
		var newGO = Instantiate(spawnSprite, inventoryTransform);
		if(!item.image) {
			Debug.LogWarning("Item " + item.name + " has no image, using fallback");
			newGO.GetComponent<Image>().sprite = fallbackSprite;
		} else {
			newGO.GetComponent<Image>().sprite = item.image;
		}
		newGO.GetComponent<BlockItemMovement>().item = item;
		newGO.GetComponent<BlockItemMovement>().inv = this;

		// Set the anchors
		RectTransform rt = newGO.GetComponent<RectTransform>();
		SetObjectAnchors(rt, item, inventorySize);
		rt.pivot = new Vector2(1f / (2f * (float)item.width), 
			1 - (1f / (2f * (float)item.height)));
		spawnedSprites.Add(newGO);
	}

	Vector2 GetAnchorFinderPosition()
	{
		return anchorFinder.anchorMin;
	}

	Vector2Int FindMousePosition()
	{
		float width = 1f / inventorySize.x;
		float height = 1f / inventorySize.y;

		// Min & Max are the same
		Vector2 anchor = GetAnchorFinderPosition();

		for (int i = 0; i < inventorySize.x; ++i)
		{
			for (int j = 0; j < inventorySize.y; ++j)
			{
				Vector2 min = new Vector2(i * width, 
					1 - (j * height) - (height));
				Vector2 max = min + new Vector2(width, height);
				if (anchor.x >= min.x && anchor.x <= max.x &&
					anchor.y >= min.y && anchor.y <= max.y)
				{
					return new Vector2Int(i, j);
				}
			}
		}
		return new Vector2Int(FAIL, FAIL);
	}

	public void AnchorItemToEquipPanel(Item item, 
		RectTransform itemRect, RectTransform rt)
	{
		item.x = SKIP;
		item.y = SKIP;
		itemRect.anchorMin = rt.anchorMin;
		itemRect.anchorMax = rt.anchorMax;
		itemRect.offsetMin = itemRect.offsetMax = Vector2.zero;
	}

	public int FindItemIdxInInventory(Item item)
	{
		return inventory.FindIndex(x => (x.id == item.id));
	}

	bool UnequipEquipment(int equipmentIndex)
	{
		// Get Equipment
		Item item = equipment.itemsEquipped[equipmentIndex];
		// Find Freespot
		Vector2Int spot = GetFreeSpot(item.width, item.height);
		// If None, return
		if (spot.x == FAIL || spot.y == FAIL) return false;
		// Occupy the space
		OccupySpace(spot.x, spot.y, item.width, item.height);
		// Set x/y
		item.x = spot.x;
		item.y = spot.y;
		// Remove it from being equipped
		equipment.ChangeEquipment(item, FAIL);
		// Set the anchors
		int invIdx = FindItemIdxInInventory(item);
		RectTransform rect =
			spawnedSprites[invIdx].GetComponent<RectTransform>();
		SetObjectAnchors(rect, item, inventorySize);
		AnchorToCorners(rect);
		return true;
	}

	void SwapEquipment(Item equip, RectTransform equipRect, int equipIdx)
	{
		try
		{
			// We try to remove other equipment 
			// eg. trying to equipping 2H with 2 1H equipped already
			if(equipIdx == BlockEquipment.LHAND || equipIdx == BlockEquipment.RHAND)
			{
				ItemWeapon itemWeapon = (ItemWeapon)equip;
				int otherEquipIdx = (equipIdx == BlockEquipment.RHAND) ?
					BlockEquipment.LHAND : BlockEquipment.RHAND;
				if(itemWeapon.weaponHandleType == WeaponHandleType.TwoHand &&
					/*equipment.IsItemEquippedAtPanel(equipIdx) &&*/
						equipment.IsItemEquippedAtPanel(otherEquipIdx))
				{
					if (!UnequipEquipment(otherEquipIdx)) return;
				}
				else if (itemWeapon.weaponHandleType == 
					WeaponHandleType.OneHand &&
					equipIdx == BlockEquipment.RHAND)
				{
					ItemWeapon otherWeapon = 
						(ItemWeapon)equipment.itemsEquipped[otherEquipIdx];
					if(otherWeapon.weaponHandleType == WeaponHandleType.TwoHand)
					{
						if (!UnequipEquipment(otherEquipIdx)) return;
					}
				}
			}

			// Item we are trying to remove
			Item removeItem = equipment.itemsEquipped[equipIdx];

			// item we are trying to add's x/y
			int equipX = equip.x;
			int equipY = equip.y;

			// Equip the item
			RectTransform equipPanel = equipment.equipPanels[equipIdx];
			AnchorItemToEquipPanel(equip, equipRect, equipPanel);
			equipment.ChangeEquipment(equip, equipIdx);
			equipment.itemsEquipped[equipIdx] = equip;
			FreeSpace(equipX, equipY, equip.width, equip.height);

			// Remove the old item
			removeItem.x = equipX;
			removeItem.y = equipY;

			int removeIdx = FindItemIdxInInventory(removeItem);
			RectTransform removeRect =
				spawnedSprites[removeIdx].GetComponent<RectTransform>();
			SetObjectAnchors(removeRect, removeItem, inventorySize);
			equipment.ChangeEquipment(removeItem, FAIL);
			OccupySpace(equipX, equipY, removeItem.width, removeItem.height);

			equipment.itemsEquipped[equipIdx] = equip;
		}
		catch (System.Exception)
		{
		}
	}

	public bool EquipItem(Item item, RectTransform itemRect)
	{
		int idx = equipment.CheckEquipmentPanels(item);
		if (idx != FAIL)
		{
			SwapEquipment(item, itemRect, idx);
			return false;
		}

		Debug.Log("Out of bounds");
		return false;
	}

	void SwapItems(Item item, RectTransform itemRect, Vector2Int pos)
	{
		Vector2 anchor = GetAnchorFinderPosition();

		// Find the item we are trying to swap with
		int swappingIdx;
		RectTransform swappingRect = null;

		for (swappingIdx = 0; swappingIdx < spawnedSprites.Count; ++swappingIdx)
		{
			swappingRect = spawnedSprites[swappingIdx].
				GetComponent<RectTransform>();
			if(BlockEquipment.IsMouseInPanel(anchor, swappingRect))
			{
				break;
			}
		}
		
		// Incase we dont find it
		if(swappingIdx == spawnedSprites.Count)
		{
			throw new System.NullReferenceException();
		}

		Item swappingItem = inventory[swappingIdx];

		int swapX = swappingItem.x, swapY = swappingItem.y;

		if(item.x == SKIP || item.x == SKIP)
		{
			ItemEquipable equipable = (ItemEquipable)item;
			int equipmentIdx = equipable.equipped;
			SwapEquipment(swappingItem, swappingRect, equipmentIdx);
		}
		else
		{
			if (!IsOverlappingIgnoreRect(pos.x, pos.y, item.width, item.height,
					swapX, swapY, swappingItem.width, swappingItem.height) &&

				!IsOverlappingIgnoreRect(item.x, item.y,
				swappingItem.width, swappingItem.height,
				item.x, item.y, item.width, item.height))
			{
				FreeSpace(swapX, swapY,
					swappingItem.width, swappingItem.height);
				FreeSpace(item.x, item.y, item.width, item.height);

				swappingItem.x = item.x;
				swappingItem.y = item.y;
				item.x = swapX;
				item.y = swapY;

				OccupySpace(swappingItem.x, swappingItem.y,
					swappingItem.width, swappingItem.height);
				OccupySpace(item.x, item.y, item.width, item.height);

				SetObjectAnchors(swappingRect, swappingItem, inventorySize);
				SetObjectAnchors(itemRect, item, inventorySize);
			}
		}
	}

	public bool MoveItem(Item item, RectTransform itemRect, Vector2Int pos)
	{
		try
		{
			if (IsOverlappingIgnoreSelf(pos.x, pos.y,
				item.width, item.height, item.x, item.y))
			{
				//Debug.Log("Overlapping another item");
				SwapItems(item, itemRect, pos);
				return false;
			}
			else
			{
				FreeSpace(item.x, item.y, item.width, item.height);
				OccupySpace(pos.x, pos.y, item.width, item.height);
				item.x = pos.x;
				item.y = pos.y;

				try
				{
					equipment.ChangeEquipment(item, FAIL);
				}
				catch (System.Exception)
				{
				}
			}
		}
		catch (System.Exception)
		{
			OccupySpace(item.x, item.y, item.width, item.height);
			Debug.Log("Out of bounds");
			return false;
		}

		return true;
	}

	public bool ItemEndDrag(Item item, RectTransform itemRect)
	{
		Vector2Int pos = FindMousePosition();
		if(pos.x == FAIL || pos.y == FAIL)
		{
			return EquipItem(item, itemRect);
		}
		else
		{
			return MoveItem(item, itemRect, pos);
		}
	}

	public static void SetObjectAnchors(RectTransform rt, 
		Item item, Vector2Int size)
	{
		float width = 1f / size.x;
		float height = 1f / size.y;

		rt.anchorMin = new Vector2(item.x * width,
			1 - (item.y * height) - (height * item.height));
		rt.anchorMax = rt.anchorMin +
			new Vector2(width * item.width, (height * item.height));

		rt.offsetMin = rt.offsetMax = Vector2.zero;
	}

	public static void AnchorToCorners(RectTransform rect)
	{
		RectTransform t = rect;
		RectTransform pt = t.parent as RectTransform;

		if (t == null || pt == null) return;

		Vector2 newAnchorsMin = new Vector2(t.anchorMin.x + t.offsetMin.x / pt.rect.width,
											t.anchorMin.y + t.offsetMin.y / pt.rect.height);
		Vector2 newAnchorsMax = new Vector2(t.anchorMax.x + t.offsetMax.x / pt.rect.width,
											t.anchorMax.y + t.offsetMax.y / pt.rect.height);

		t.anchorMin = newAnchorsMin;
		t.anchorMax = newAnchorsMax;
		t.offsetMin = t.offsetMax = Vector2.zero;
	}
}