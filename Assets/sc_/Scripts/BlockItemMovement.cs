﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlockItemMovement : MonoBehaviour {

	public RectTransform rect;
	public BlockInventory inv;
	//public Image image;
	public Item item;

	public void Start()
	{
		rect = GetComponent<RectTransform>();
	}

	public void StartDragItem()
	{
		rect.SetAsLastSibling();
	}

	public void DragItem()
	{
		Vector2 mousePos = Input.mousePosition;
		rect.position = mousePos;
	}

	public void EndDragItem()
	{
		if(inv.ItemEndDrag(item, rect))
		{
			BlockInventory.SetObjectAnchors(rect, item, inv.inventorySize);
			BlockInventory.AnchorToCorners(rect);
		}
		else
		{
			//rect.localPosition = Vector3.zero;
			rect.anchoredPosition = Vector2.zero;
			rect.anchoredPosition3D = Vector3.zero;
		}
	}
}
