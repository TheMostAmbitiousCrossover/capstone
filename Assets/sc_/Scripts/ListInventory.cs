﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class ListInventory : SerializedMonoBehaviour {

	public ListEquipment equipment;
	public ListInventoryViewer inventoryViewer;
	private ItemDatabase itemDatabase;

	[SerializeField, TabGroup("Inventory"),
	ListDrawerSettings(ShowIndexLabels = true,
	ListElementLabelName = "name", Expanded = true)]
	public List<Item> inventory = new List<Item>();

	// Short hands for itemtypes
	const ItemBase.ItemType weapon = ItemBase.ItemType.IWeapon;
	const ItemBase.ItemType armor = ItemBase.ItemType.IArmor;
	const ItemBase.ItemType other = ItemBase.ItemType.IOther;

	public Item this[int val]
	{
		get
		{
			return inventory[val];
		}
	}

	public int Count
	{
		get
		{
			return inventory.Count;
		}
	}

	public bool IsEquipped(Item item)
	{
		ItemEquipable equipable = item as ItemEquipable;
		if(equipable != null) return equipable.equipped != -1;
		return false;
	}

	void Start () {
		itemDatabase = FindObjectOfType<ItemDatabase>();

		if(equipment.checkDelegate == null)
		{
			equipment.SetDelegates();
		}

		AddItem(itemDatabase.GetItem(weapon, 2));
		AddItem(itemDatabase.GetItem(weapon, 2));
		AddItem(itemDatabase.GetItem(weapon, 3));
		AddItem(itemDatabase.GetItem(armor, 0));
		AddItem(itemDatabase.GetItem(armor, 1));
		AddItem(itemDatabase.GetItem(armor, 2));
		AddItem(itemDatabase.GetItem(armor, 3));
		AddItem(itemDatabase.GetItem(other, 0));
		//EquipItem(ListEquipment.LHAND, 0);
	}
	
	public void AddItem(Item item)
	{
		inventory.Add(item);
	}

	public void RemoveItem(Item item)
	{
		inventory.Remove(item);
	}

	// Don't use this one, it doesnt do any checks
	// For testing purposes only
	public void EquipItem(int slot, int inventoryIdx)
	{
		Item equippingItem = inventory[inventoryIdx];
		if(ItemDatabase.IsEquppable(equippingItem) &&
			equipment.CanPlaceIntoSlot(slot, equippingItem))
		{
			equipment[slot] = equippingItem;
		}
		else
		{
			Debug.LogError("You can't equip " + equippingItem.name);
		}
	}

	public void UnequipItem(int slot)
	{
		equipment[slot] = null;
	}

	void EquipOnLeft()
	{
		int idx = inventoryViewer.selectedItemIdx;
		// Can we equip it?
		if (!ItemDatabase.IsEquppable(inventory[idx])) return;

		ItemEquipable itemEquipable = inventory[idx] as ItemEquipable;
		// Unequip Equipped Items
		if (itemEquipable.equipped != -1)
		{
// 			if((equipment[ListEquipment.LHAND] as ItemWeapon).weaponHandleType == WeaponHandleType.TwoHand)
// 			{
// 
// 			}
// 
// 			if (itemEquipable.equipped == ListEquipment.RHAND)
// 			{
// 				var temp = equipment[ListEquipment.LHAND];
// 				equipment[ListEquipment.LHAND] = itemEquipable;
// 				equipment[ListEquipment.RHAND] = temp;
// 				return;
// 			}

			equipment[itemEquipable.equipped] = null;
			return;
		}

		// Cast to weapon
		if (itemEquipable.itemType == ItemBase.ItemType.IWeapon)
		{
			var itemWeapon = itemEquipable as ItemWeapon;
			var equippingHandle = itemWeapon.weaponHandleType;
			var equippedLHandle = WeaponHandleType.Length;

			// Is the item equipped 1H or 2H?
			var lHandEquip = equipment[ListEquipment.LHAND] as ItemWeapon;
			if (lHandEquip != null)
				equippedLHandle = lHandEquip.weaponHandleType;

			// If we have equipped a 2H weapon, we can bypass everything
			// If we are equipping a 2H weapon, we can also bypass everything
			// if LHand is Null, we can also bypass everything
			if (equippedLHandle == WeaponHandleType.TwoHand ||
				equippingHandle == WeaponHandleType.TwoHand ||
				equippedLHandle == WeaponHandleType.Length)
			{
				equipment[ListEquipment.LHAND] = itemEquipable;
				equipment[ListEquipment.RHAND] = null;
			}
			// At this point we know equipping OneHanded Weapon
			else
			{
				equipment[ListEquipment.LHAND] = itemEquipable;
			}

		}
		// Or cast to armor
		else if (itemEquipable.itemType == ItemBase.ItemType.IArmor)
		{
			var itemArmor = itemEquipable as ItemArmor;
			if (itemArmor.armorType == ArmorType.Helmet)
			{
				equipment[ListEquipment.HELM] = itemEquipable;
			}
			else if (itemArmor.armorType == ArmorType.Body)
			{
				equipment[ListEquipment.BODY] = itemEquipable;
			}
			else if (itemArmor.armorType == ArmorType.Glove)
			{
				equipment[ListEquipment.GLOVE] = itemEquipable;
			}
			else if (itemArmor.armorType == ArmorType.Shoe)
			{
				equipment[ListEquipment.SHOE] = itemEquipable;
			}
		}
	}

	void EquipOnRight()
	{
		int idx = inventoryViewer.selectedItemIdx;
		// Can we equip it?
		if (!ItemDatabase.IsEquppable(inventory[idx])) return;

		ItemEquipable itemEquipable = inventory[idx] as ItemEquipable;
		// Unequip Equipped Items
		if (itemEquipable.equipped != -1)
		{
// 			if(itemEquipable.equipped == ListEquipment.LHAND)
// 			{
// 				var temp = equipment[ListEquipment.RHAND];
// 				equipment[ListEquipment.RHAND] = itemEquipable;
// 				equipment[ListEquipment.LHAND] = temp;
// 				return;
// 			}

			equipment[itemEquipable.equipped] = null;
			return;
		}

		// Cast to weapon
		if (itemEquipable.itemType == ItemBase.ItemType.IWeapon)
		{
			var itemWeapon = itemEquipable as ItemWeapon;
			var equippingHandle = itemWeapon.weaponHandleType;
			var equippedLHandle = WeaponHandleType.Length;
			var equippedRHandle = WeaponHandleType.Length;

			// Is the item equipped 1H or 2H?
			var lHandEquip = equipment[ListEquipment.LHAND] as ItemWeapon;
			if (lHandEquip != null)
				equippedLHandle = lHandEquip.weaponHandleType;

			var rHandEquip = equipment[ListEquipment.RHAND] as ItemWeapon;
			if (rHandEquip != null)
				equippedRHandle = rHandEquip.weaponHandleType;

			// If we have equipped a 2H weapon, we can bypass everything
			// If we are equipping a 2H weapon, we can also bypass everything
			// if LHand is Null, we can also bypass everything
			if (equippingHandle == WeaponHandleType.TwoHand)
			{
				equipment[ListEquipment.LHAND] = itemEquipable;
				equipment[ListEquipment.RHAND] = null;
			}
			else if (equippedLHandle == WeaponHandleType.TwoHand)
			{
				equipment[ListEquipment.LHAND] = null;
				equipment[ListEquipment.RHAND] = itemEquipable;
			}
			// At this point we know equipping OneHanded Weapon
			else
			{
				equipment[ListEquipment.RHAND] = itemEquipable;
			}
		}
		else if (itemEquipable.itemType == ItemBase.ItemType.IArmor)
		{
			var itemArmor = itemEquipable as ItemArmor;
			if (itemArmor.armorType == ArmorType.Helmet)
			{
				equipment[ListEquipment.HELM] = itemEquipable;
			}
			else if (itemArmor.armorType == ArmorType.Body)
			{
				equipment[ListEquipment.BODY] = itemEquipable;
			}
			else if (itemArmor.armorType == ArmorType.Glove)
			{
				equipment[ListEquipment.GLOVE] = itemEquipable;
			}
			else if (itemArmor.armorType == ArmorType.Shoe)
			{
				equipment[ListEquipment.SHOE] = itemEquipable;
			}
		}
	}

	// Try to equip the selected item in the item viewer
	void EquipSelectedItem()
	{
		int idx = inventoryViewer.selectedItemIdx;
		// Can we equip it?
		if (!ItemDatabase.IsEquppable(inventory[idx])) return;

		ItemEquipable itemEquipable = inventory[idx] as ItemEquipable;
		// Unequip Equipped Items
		if (itemEquipable.equipped != -1)
		{
			equipment[itemEquipable.equipped] = null;
			return;
		}

		// Cast to weapon
		if(itemEquipable.itemType == ItemBase.ItemType.IWeapon)
		{
			var itemWeapon = itemEquipable as ItemWeapon;
			var equippingHandle = itemWeapon.weaponHandleType;
			var equippedLHandle = WeaponHandleType.Length;
			var equippedRHandle = WeaponHandleType.Length;

			// Is the item equipped 1H or 2H?
			var lHandEquip = equipment[ListEquipment.LHAND] as ItemWeapon;
			if (lHandEquip != null)
				equippedLHandle = lHandEquip.weaponHandleType;

			var rHandEquip = equipment[ListEquipment.RHAND] as ItemWeapon;
			if (rHandEquip != null)
				equippedRHandle = rHandEquip.weaponHandleType;

			// If we have equipped a 2H weapon, we can bypass everything
			// If we are equipping a 2H weapon, we can also bypass everything
			// if LHand is Null, we can also bypass everything
			if(equippedLHandle == WeaponHandleType.TwoHand ||
				equippingHandle == WeaponHandleType.TwoHand ||
				equippedLHandle == WeaponHandleType.Length)
			{
				equipment[ListEquipment.LHAND] = itemEquipable;
				equipment[ListEquipment.RHAND] = null;
			}
			// If RHand is Null
			else if (equippedRHandle == WeaponHandleType.Length)
			{
				// Dont change LHand cos there might be something
				equipment[ListEquipment.RHAND] = itemEquipable;
			}
			// At this point we know equipping OneHanded Weapon
			else
			{
				equipment[ListEquipment.LHAND] = itemEquipable;
			}
		}
		// Or cast to armor
		else if (itemEquipable.itemType == ItemBase.ItemType.IArmor)
		{
			var itemArmor = itemEquipable as ItemArmor;
			if(itemArmor.armorType == ArmorType.Helmet)
			{
				equipment[ListEquipment.HELM] = itemEquipable;
			}
			else if(itemArmor.armorType == ArmorType.Body)
			{
				equipment[ListEquipment.BODY] = itemEquipable;
			}
			else if(itemArmor.armorType == ArmorType.Glove)
			{
				equipment[ListEquipment.GLOVE] = itemEquipable;
			}
			else if(itemArmor.armorType == ArmorType.Shoe)
			{
				equipment[ListEquipment.SHOE] = itemEquipable;
			}
		}
	}

	void Update () {
		// Swap Equipment
		if(Input.GetKeyDown(KeyCode.D))
		{
			equipment.SwapMainHand();
			// Should also change the quick swap stuff
			inventoryViewer.shouldUpdate = true;
		}

		// Equip Left
		if(Input.GetKeyDown(KeyCode.Q))
		{
			EquipOnLeft();
			inventoryViewer.shouldUpdate = true;
		}

		//Equip Right
		if(Input.GetKeyDown(KeyCode.E))
		{
			EquipOnRight();
			inventoryViewer.shouldUpdate = true;
		}

		// Drop
		if(Input.GetKeyDown(KeyCode.A))
		{
			// Unequip and drop
			inventoryViewer.shouldUpdate = true;
		}
	}

	public string GetItemListDisplayName(int i)
	{
		Item item = inventory[i];
		if(IsEquipped(item))
		{
			return item.name + " (Equipped)";
		}
		return item.name;
	}
}
