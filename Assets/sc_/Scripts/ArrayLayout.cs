﻿using System.Collections;
using UnityEngine;

[System.Serializable]
public class ArrayLayout {

	[System.Serializable]
	public class ColumnData
	{
		public bool[] column;

		public bool this[int key]
		{
			set
			{
				column[key] = value;
			}
			get
			{
				return column[key];
			}
		}
	}
	public ColumnData[] row;
	public ColumnData this[int key]
	{
		set
		{
			row[key] = value;
		}
		get
		{
			return row[key];
		}
	}
}
