﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WeaponHandleType
{
	OneHand = 0,
	TwoHand = 1,
	Length = 2
}

[System.Serializable]
public class ItemWeapon : ItemEquipable {

	public PlayerCombat.WeaponType weaponType;
	public WeaponHandleType weaponHandleType;
	public float damage;

	public ItemWeapon()
	{

	}

	public ItemWeapon(ItemWeapon item)
	{
		this.id = GenerateID();
		this.name = item.name;
		this.image = item.image;
		this.itemType = item.itemType;
		this.width = item.width;
		this.height = item.height;
		this.x = item.x;
		this.y = item.y;
		this.description = item.description;
		this.equipped = item.equipped;
		this.strength = item.strength;
		this.dexerity = item.dexerity;
		this.endurance = item.endurance;
		this.agility = item.agility;
		this.weaponType = item.weaponType;
		this.weaponHandleType = item.weaponHandleType;
		this.damage = item.damage;
	}

	public ItemWeapon(ItemWeapon item, ItemType type)
	{
		this.id = GenerateID();
		this.name = item.name;
		this.image = item.image;
		this.itemType = type;
		this.width = item.width;
		this.height = item.height;
		this.x = item.x;
		this.y = item.y;
		this.description = item.description;
		this.equipped = item.equipped;
		this.strength = item.strength;
		this.dexerity = item.dexerity;
		this.endurance = item.endurance;
		this.agility = item.agility;
		this.weaponType = item.weaponType;
		this.weaponHandleType = item.weaponHandleType;
		this.damage = item.damage;
	}
}
