﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Item : ItemBase {

	public Item()
	{

	}

	public Item(Item item)
	{
		this.id = GenerateID();
		this.name = item.name;
		this.image = item.image;
		this.itemType = item.itemType;
		this.width = item.width;
		this.height = item.height;
		this.x = item.x;
		this.y = item.y;
		this.description = item.description;
	}

	public Item(Item item, ItemType type)
	{
		this.id = GenerateID();
		this.name = item.name;
		this.image = item.image;
		this.itemType = type;
		this.width = item.width;
		this.height = item.height;
		this.x = item.x;
		this.y = item.y;
		this.description = item.description;
	}

	public override void PerformAction()
	{
		throw new System.NotImplementedException();
	}

	public override string InventoryDescription()
	{
		return description;
	}
}
