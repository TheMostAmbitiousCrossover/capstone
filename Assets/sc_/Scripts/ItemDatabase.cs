﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Sirenix.OdinInspector;

// This class holds all the items we can equip in the game
public class ItemDatabase : SerializedMonoBehaviour {

	[SerializeField, TabGroup("Weapons"), 
		ListDrawerSettings(/*NumberOfItemsPerPage = 1, */ShowIndexLabels = true, 
		ListElementLabelName = "name", Expanded = true)]
	public List<ItemWeapon> weaponList = new List<ItemWeapon>();

	[SerializeField, TabGroup("Armour"), 
		ListDrawerSettings(/*NumberOfItemsPerPage = 1, */ShowIndexLabels = true, 
		ListElementLabelName = "name", Expanded = true)]
	public List<ItemArmor> armorList = new List<ItemArmor>();

	[SerializeField, TabGroup("Other"), 
		ListDrawerSettings(/*NumberOfItemsPerPage = 1, */ShowIndexLabels = true, 
		ListElementLabelName = "name", Expanded = true)]
	public List<Item> otherList = new List<Item>();

	public float WeaponListLength
	{
		get
		{
			return weaponList.Count;
		}
	}

	public float ArmorListLength
	{
		get
		{
			return armorList.Count;
		}
	}

	public float OtherListLength
	{
		get
		{
			return otherList.Count;
		}
	}

	void Start()
	{

	}

	public static bool IsEquppable(Item i)
	{
		ItemEquipable e = i as ItemEquipable;
		return (e != null) ? true : false;
	}

	public static ItemWeapon CastWeapon(Item i)
	{
		if(i.itemType != ItemBase.ItemType.IWeapon)
		{
			Debug.LogError("Not a weapon");
			throw new System.InvalidCastException();
		}
		return (ItemWeapon)i;
	}

	public static ItemArmor CastArmor(Item i)
	{
		if (i.itemType != ItemBase.ItemType.IArmor)
		{
			Debug.LogError("Not armor");
			throw new System.InvalidCastException();
		}
		return (ItemArmor)i;
	}

	// This makes a copy of the item in the database
	// We don't want to modify the vars in playmode
	public Item GetItem(ItemBase.ItemType type, int key)
	{
		string errorMsg = "";
		try
		{
			switch(type)
			{
				case ItemBase.ItemType.IWeapon:
					return new ItemWeapon(weaponList[key], type);

				case ItemBase.ItemType.IArmor:
					return new ItemArmor(armorList[key], type);

				case ItemBase.ItemType.IOther:
					return new Item(otherList[key], type);
			}
		}
		catch (System.Exception ex)
		{
			errorMsg = ex.StackTrace;
		}
		throw new System.Exception("Item of type " + type +
			" and index of " + key + " does not exist\n" + errorMsg);
	}

	// This makes a copy of the item in the database
	// We don't want to modify the vars in playmode
	public Item GetItem(ItemBase.ItemType type, string name)
	{
		string errorMsg = "";
		try
		{
			switch (type)
			{
				case ItemBase.ItemType.IWeapon:
					return new ItemWeapon(weaponList.First(x => x.name == name), type);

				case ItemBase.ItemType.IArmor:
					return new ItemArmor(armorList.First(x => x.name == name), type);

				case ItemBase.ItemType.IOther:
					return new Item(otherList.First(x => x.name == name), type);
			}
		}
		catch (System.Exception ex)
		{
			errorMsg = ex.StackTrace;
		}
		throw new System.Exception("Item of type " + type +
				" named " + name + " does not exist\n" + errorMsg);
	}
}
