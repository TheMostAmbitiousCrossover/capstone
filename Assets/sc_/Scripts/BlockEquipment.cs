﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockEquipment : MonoBehaviour {

	public List<RectTransform> equipPanels;
	public Item[] itemsEquipped;
	public BlockInventory inv;

	public const int LHAND	= 0;
	public const int RHAND	= 1;
	public const int TOP	= 2;
	public const int BODY	= 3;
	public const int GLOVE	= 4;
	public const int SHOE	= 5;

	private readonly Dictionary<int, 
		System.Func<BlockEquipment, Item, int, int, bool>> CheckCondition = 
	new Dictionary<int, System.Func<BlockEquipment, Item, int, int, bool>>
	{
		{ LHAND,	IsWeapon },
		{ RHAND,	IsWeapon },
		{ TOP,		IsArmor },
		{ BODY,		IsArmor },
		{ GLOVE,	IsArmor },
		{ SHOE,		IsArmor },
	};

	private readonly Dictionary<int, int> EquipmentEnumInt =
	new Dictionary<int, int>
	{
		{ LHAND, (int)WeaponHandleType.Length	}, // 1H or 2H
		{ RHAND, (int)WeaponHandleType.OneHand	}, // 1H
		{ TOP,   (int)ArmorType.Helmet	}, // Helm
		{ BODY,  (int)ArmorType.Body	}, // Body
		{ GLOVE, (int)ArmorType.Glove	}, // Glove
		{ SHOE,  (int)ArmorType.Shoe	}, // Shoe
	};

	// When something has failed
	private const int FAIL = -1;

	void Start()
	{
		itemsEquipped = new Item[equipPanels.Count];
	}

	public bool IsItemEquippedAtPanel(int idx)
	{
		if (itemsEquipped[idx] == null) return false;
		return itemsEquipped[idx].id != 0;
	}

	public int CheckEquipmentPanels(Item item)
	{
		Vector2 anchor = inv.anchorFinder.anchorMin;
		int i = 0;
		foreach(var panel in equipPanels)
		{
			if (IsMouseInPanel(anchor, panel))
			{
				if(CheckCondition[i](this, item, i, EquipmentEnumInt[i]))
				{
					return i;
				}
			}
			++i;
		}
		return FAIL;
	}

// 	public static bool BasicCheck(Item item)
// 	{
// 		var checkCast = (Equipable)item;
// 
// 		return true;
// 	}

	public static bool IsWeapon(BlockEquipment e, Item item, int idx, int enumInt)
	{
		try
		{
			//BasicCheck(item);
			var wep = ItemDatabase.CastWeapon(item);
			if (enumInt != (int)WeaponHandleType.Length)
			{
				// 2H
				if ((int)wep.weaponHandleType != enumInt)
				{
					return false;
				}
			}

// 			if(wep.weaponHandleType == WeaponHandleType.OneHand)
// 			{
// 				int other = (idx == RHAND) ? LHAND : RHAND;
// 				if (((ItemWeapon)e.itemsEquipped[other]).weaponHandleType == 
// 					WeaponHandleType.TwoHand)
// 				{
// 					return false;
// 				}
// 			}
// 			else if(wep.weaponHandleType == WeaponHandleType.TwoHand)
// 			{
// 				if (e.itemsEquipped[RHAND].name != null)
// 				{
// 					return false;
// 				}
// 			}

			return true;
		}
		catch (System.Exception)
		{
			return false;
		}
	}

	public static bool IsArmor(BlockEquipment e, Item item, int idx, int enumInt)
	{
		try
		{
			//BasicCheck(item);
			var arm = ItemDatabase.CastArmor(item);
			if((int)arm.armorType != enumInt)
			{
				return false;
			}
			return true;
		}
		catch (System.Exception)
		{
			return false;
		}
	}

	public void ChangeEquipment(Item item, int idx)
	{
		var equippable = ((ItemEquipable)item);
		if (equippable.equipped != FAIL)
		{
			itemsEquipped[equippable.equipped] = null;
		}
		equippable.equipped = idx;
	}

	public static bool IsMouseInPanel(Vector2 point, RectTransform rect)
	{
		if (point.x >= rect.anchorMin.x && point.x <= rect.anchorMax.x &&
			point.y >= rect.anchorMin.y && point.y <= rect.anchorMax.y)
		{
			return true;
		}
		return false;
	}
}
