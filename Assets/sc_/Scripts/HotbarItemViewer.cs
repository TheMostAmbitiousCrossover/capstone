﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HotbarItemViewer : MonoBehaviour {

	public ListEquipment equipment;
	public Image lHand;
	public Image rHand;
	public Image olHand;
	public Image orHand;

	void UpdateWeaponImage(Image panel, int idx)
	{
		if(equipment[idx] != null) panel.sprite = equipment[idx].image;
		else panel.sprite = null;
	}

	public void UpdateWeaponImages()
	{
		UpdateWeaponImage(lHand, ListEquipment.LHAND);
		UpdateWeaponImage(rHand, ListEquipment.RHAND);
		UpdateWeaponImage(olHand, ListEquipment.OLHAND);
		UpdateWeaponImage(orHand, ListEquipment.ORHAND);
	}
}
