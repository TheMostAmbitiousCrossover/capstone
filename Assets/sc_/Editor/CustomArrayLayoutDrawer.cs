﻿// using System.Collections;
// using System.Collections.Generic;
// using UnityEngine;
// using UnityEditor;
// 
// [CustomPropertyDrawer(typeof(ArrayLayout))]
// public class CustomArrayLayoutDrawer : PropertyDrawer {
// 
// 	private const float itemWidth = 15f;
// 	private const float itemHeight = 15f;
// 	private const int columns = 10;
// 	private const int rows = 4;
// 
// 	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
// 	{
// 		EditorGUI.PrefixLabel(position, label);
// 		Rect newposition = position;
// 		newposition.y += itemHeight;
// 		SerializedProperty row = property.FindPropertyRelative("row");
// 		if (row.arraySize != rows)
// 			row.arraySize = rows;
// 		for(int i = 0; i < row.arraySize; ++i)
// 		{
// 			SerializedProperty col = row.GetArrayElementAtIndex(i).FindPropertyRelative("column");
// 			newposition.height = itemHeight;
// 			if (col.arraySize != columns)
// 				col.arraySize = columns;
// 			newposition.width = itemWidth;
// 			for (int j = 0; j < columns; j++)
// 			{
// 				EditorGUI.PropertyField(newposition, col.GetArrayElementAtIndex(j), GUIContent.none);
// 				newposition.x += newposition.width;
// 			}
// 			newposition.x = position.x;
// 			newposition.y += itemHeight;
// 		}
// 	}
// 
// 	public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
// 	{
// 		return itemHeight * (rows + 1);
// 	}
// }
