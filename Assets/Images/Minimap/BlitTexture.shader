﻿Shader "Custom/BlitTexture"
{
	Properties
	{
		_MainTex("TextureRGB", 2D) = "white" {}
		_Position("PlayerPos", Vector) = (0, 0, 0, 0)
	}
	SubShader
	{
		// No culling or depth
		//Cull Off ZWrite Off ZTest Always
		//Blend SrcAlpha OneMinusSrcAlpha
		Blend One One
		BlendOp Add

		Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }

		Pass
	{
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		
		#include "UnityCG.cginc"

		struct appdata
	{
		float4 vertex : POSITION;
		float2 uv : TEXCOORD0;
	};

	struct v2f
	{
		float2 uv : TEXCOORD0;
		float4 vertex : SV_POSITION;
	};

	v2f vert(appdata v)
	{
		v2f o;
		o.vertex = UnityObjectToClipPos(v.vertex);
		o.uv = v.uv;
		return o;
	}

	sampler2D _MainTex;
	vector _Position;

	fixed4 frag(v2f i) : SV_Target
	{
		float2 j = i.uv;
		
		const float imageOffset = 0.5f;

		const float imageWidth = 10.0f;
		const float imageHeight = 17.0f;

		// Shrink the image
		j.r *= imageWidth;
		j.g *= imageHeight;

		// Center the image
		j.r -= (imageWidth / 2) - imageOffset;
		j.g -= (imageHeight / 2) - imageOffset;

		j.r -= _Position.x;
		j.g -= _Position.y;

		// Discard any UV's out of 0 - 1
		if (j.r > 1 || j.g > 1 ||
			j.r < 0 || j.g < 0) 
				discard;

		// Get the Color
		fixed4 col = tex2D(_MainTex, j).rrrr;
		return col;
		}
			ENDCG
		}
	}
}
