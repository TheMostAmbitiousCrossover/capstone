﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldGlowAnim : MonoBehaviour {
    public float shineLocation;
    [Range(0.1f,1f)]
    public float speed;
    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
        shineLocation += Time.deltaTime * speed;
        if(shineLocation >= 1)
        {
            shineLocation = 0;
        }
        GetComponent<Renderer>().material.SetFloat("_ShineLocation", shineLocation);
    }
}
