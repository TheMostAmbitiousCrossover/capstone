﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour {


    [SerializeField]
    public WeaponScript.Weapon parentWeapon = new WeaponScript.Weapon();
    public Rigidbody2D rb;
    public float damage;
    public bool bounce;
    [Range(0.1f,10.0f)]
    public float bulletSpeed;
    public Vector2 direction;
    float timeAlive;
    public bool goThrough;
    public float tickPerSec = 10f;
    private float _tickRate;
    // Use this for initialization
    void Start ()
    {
        tickPerSec = 1 / tickPerSec;
        _tickRate = tickPerSec;
        rb = GetComponent<Rigidbody2D>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        rb.velocity = (direction * bulletSpeed);
        Vector2 moveDirection = rb.velocity.normalized;
        if (moveDirection != Vector2.zero)
        {
            float angle = Mathf.Atan2(moveDirection.y, moveDirection.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle+90));
        }
        if (bulletSpeed * timeAlive > parentWeapon.range)
        {
            Destroy(this.gameObject);
        }
       
        timeAlive += Time.deltaTime;
       // this.transform.localPosition = this.transform.localPosition + new Vector3(0,0.12f,0);
    }

    public void SetParentWeapon(WeaponScript.Weapon weapon)
    {
        parentWeapon = weapon;
        parentWeapon.weaponDamage = weapon.weaponDamage;
        parentWeapon.weaponChargeMod = weapon.weaponChargeMod;
        parentWeapon.knockBack = weapon.knockBack;
        parentWeapon.weaponType = weapon.weaponType;
        parentWeapon.range = weapon.range;
        parentWeapon.attackspeed = weapon.attackspeed;
       
    }
    
    
    private void OnTriggerStay2D(Collider2D collider)
    {

       
        if (collider.CompareTag("Enemy"))
        {
  
            if (_tickRate <= 0)
            {

                collider.gameObject.GetComponent<EnemyScript>().BulletDamageTaken(damage);
                _tickRate = tickPerSec;
            }
            _tickRate -= Time.deltaTime;
            /**knockback on bullets, optional**/
            //collision.attachedRigidbody.AddForce(difference * parentWeapon.knockBack * collision.GetComponent<EnemyScript>().enemy.impairmentWeakness, ForceMode2D.Impulse);


        }
    }
    private void OnTriggerEnter2D(Collider2D collider)
    {

        if (collider.CompareTag("Enemy"))
        {
           
                collider.gameObject.GetComponent<EnemyScript>().BulletDamageTaken(damage);
                //Debug.Log("Enemy took " + damage + " damage");
                Vector3 difference = (collider.transform.position - this.transform.position).normalized;
            /**knockback on bullets, optional**/
            //collision.attachedRigidbody.AddForce(difference * parentWeapon.knockBack * collision.GetComponent<EnemyScript>().enemy.impairmentWeakness, ForceMode2D.Impulse);

            if (!goThrough)
            {
                Destroy(this.gameObject);
            }
        }
        if(collider.CompareTag("Door"))
        {
            Vector3 difference = (collider.transform.position - this.transform.position).normalized;
            collider.attachedRigidbody.AddForce(difference * 7, ForceMode2D.Impulse);
            Destroy(this.gameObject);
        }

        if (collider.CompareTag("Bullet"))
        {
            Destroy(collider.gameObject);
            if (!goThrough)
            {
                Destroy(this.gameObject);
            }
        }

        if (collider.CompareTag("Prop"))
        {
            Vector3 difference = (collider.transform.position - this.transform.position).normalized;
            collider.attachedRigidbody.AddForce(difference * parentWeapon.knockBack/100, ForceMode2D.Impulse);
            Destroy(this.gameObject);
        }


        if (collider.CompareTag("Wall"))
        {
            Destroy(this.gameObject);
            if (!bounce)
            {
                Destroy(this.gameObject);
            }
            else
            {
                //dir *= -1;
            }

        }
       

    }
}
