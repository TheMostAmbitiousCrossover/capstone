﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyObject : MonoBehaviour {

    public float time;
    public GameObject parentObj;
	// Use this for initialization
	void Start () {
       
	}
	
	// Update is called once per frame
	void Update () {
        time -= Time.deltaTime;
        if (time <= 0)
        {
            if (parentObj != null)
            {
                parentObj.GetComponent<EnemyScript>().damageTextList.RemoveAt(parentObj.GetComponent<EnemyScript>().damageTextList.Count - 1);
                Destroy(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }
}
