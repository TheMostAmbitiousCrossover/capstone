﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

public class TextAnimation : MonoBehaviour {

    public GameObject parentObj;
    public AnimationCurve highCurve;
    public AnimationCurve lowCurve;
    float time;
    bool leftDir;
    float rand;

    public Vector2 scale;

    RectTransform rt;

    float maxTime;
    private void Awake()
    {
        rt = GetComponent<RectTransform>();
    }
    // Use this for initialization
    void Start () {

        maxTime = transform.GetComponentInParent<DestroyObject>().time;
        rt = GetComponent<RectTransform>();

        if (Random.Range(0, 2) == 0)
        {
            leftDir = false;
        }
        else
            leftDir = true;
        time = 0;
        rand = Random.Range(0f, 1f);
        //rand = Random.Range(lowCurve.Evaluate(time), highCurve.Evaluate(time));
    }
    bool spawnAnimation = true;
    // Update is called once per frame
    void Update () {
        
        time += Time.deltaTime / maxTime;
        //this.GetComponent<RectTransform>().localPosition = new Vector3(rand*10, time*10, -0.5f);
        if (spawnAnimation)
        {
            float high = highCurve.Evaluate(time);
            float low = lowCurve.Evaluate(time);
            float avg = Mathf.Lerp(low, high, rand);

            Vector3 endPos = new Vector3(rand * scale.x, avg * scale.y, -0.5f);
            if (leftDir) endPos.x *= -1;
            rt.localPosition = Vector3.Lerp(rt.localPosition, endPos, time);
        
            if(rt.localPosition == endPos)
            {
                time = 0;
                spawnAnimation = false;
            }
        }
        //if (leftDir)
        //this.GetComponent<RectTransform>().localPosition =  Vector3.Lerp(this.GetComponent<RectTransform>().localPosition, new Vector3(rand + time * 7, time * 10, -0.5f), time);
        //else
        //this.GetComponent<RectTransform>().localPosition = Vector3.Lerp(this.GetComponent<RectTransform>().localPosition, new Vector3(rand+-time * 7, time * 10, -0.5f), time);
    }

    //this var keeps track of the actual damage value carried
    public float damageValue;

    public void UpdateText(float value)
    {
        transform.parent.position = parentObj.transform.position;
        if(!spawnAnimation)
        rand = Random.Range(0f, 1f);
        spawnAnimation = true;
        
        rt.localPosition = Vector3.zero;
        float sum = damageValue + value;
        damageValue = sum;
        gameObject.GetComponent<Text>().text = sum.ToString("F0");
        transform.GetChild(0).GetComponent<Text>().text = sum.ToString("F0");
        //StartCoroutine(UpdateVisual(this.transform.localScale.x, 0.1f));
    }
    //WIP
    public IEnumerator UpdateVisual(float initialScale, float overTime)
    {
        float time = 0;
        time += Time.deltaTime;
        while (time < overTime)
        {
            Mathf.Lerp(initialScale, initialScale * 1.5f, Time.deltaTime);
            yield return null;
        }
        
        Mathf.Lerp(initialScale * 1.5f, initialScale, Time.deltaTime);
    }

}
