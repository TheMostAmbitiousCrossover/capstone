using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserScript : MonoBehaviour {


    [SerializeField]
    public WeaponScript.Weapon parentWeapon = new WeaponScript.Weapon();
    public Rigidbody2D rb;
    public float damage;
    public bool bounce;
    public float dir;
    public LineRenderer lineRenderer; 
    [Range(0.1f,10.0f)]
    public float laserRange;
    public LayerMask laserMask;

	// Use this for initialization
	void Start ()
    {
        lineRenderer = this.GetComponent<LineRenderer>();
        dir = 0.3f;
        rb = GetComponent<Rigidbody2D>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        
       // this.transform.localPosition = this.transform.localPosition + new Vector3(0,0.12f,0);
	}

    public void SetParentWeapon(WeaponScript.Weapon weapon)
    {
        parentWeapon = weapon;
        parentWeapon.weaponDamage = weapon.weaponDamage;
        parentWeapon.weaponChargeMod = weapon.weaponChargeMod;
        parentWeapon.knockBack = weapon.knockBack;
        parentWeapon.weaponType = weapon.weaponType;
        parentWeapon.range = weapon.range;
        parentWeapon.attackspeed = weapon.attackspeed;
        Debug.Log("SETTING PARENT " + weapon.weaponDamage);
    }

    public RaycastHit2D DrawRaycast(Vector2 from, Vector2 to)
    {
        RaycastHit2D hit = Physics2D.Raycast(from, to * laserRange, 100, laserMask);
        DrawLine(from, to, hit);

        if (hit.collider.CompareTag("Enemy"))
        {
            hit.collider.gameObject.GetComponent<EnemyScript>().BulletDamageTaken(damage);
        }
        return hit;
    }
   
    public void DrawLine(Vector2 from, Vector2 to, RaycastHit2D ray)
    {
        Debug.DrawRay(from, to * laserRange, Color.red, 2.5f, false);
        lineRenderer.SetPosition(0, new Vector3(from.x, from.y,-1));
        //lineRenderer.SetPosition(1, new Vector3(to.x * laserRange, to.y* laserRange, -1));
        lineRenderer.SetPosition(1, new Vector3(ray.point.x, ray.point.y, -1));
    }
        
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Enemy")
        {
            collider.gameObject.GetComponent<EnemyScript>().BulletDamageTaken(damage);
            Debug.Log("Enemy took " + damage + " damage");
            //Vector3 difference = (collider.transform.position - this.transform.position).normalized;
            /**knockback on bullets, optional**/
            //collision.attachedRigidbody.AddForce(difference * parentWeapon.knockBack * collision.GetComponent<EnemyScript>().enemy.impairmentWeakness, ForceMode2D.Impulse);
            Destroy(this.gameObject);
        }
        /**if(collider.CompareTag("Door"))
        {
            Vector3 difference = (collider.transform.position - this.transform.position).normalized;
            collider.attachedRigidbody.AddForce(difference * 7, ForceMode2D.Impulse);
            Destroy(this.gameObject);
        }
    ***/
        if (collider.CompareTag("Wall"))
        {
            if (!bounce)
            {
                Debug.Log("WALLLLLLL");
                Destroy(this.gameObject);
            }
            else
            {
                //dir *= -1;
            }

        }
       

    }
}
