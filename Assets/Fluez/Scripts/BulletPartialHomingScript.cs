﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BulletPartialHomingScript : MonoBehaviour
{

    [SerializeField] private String playerTagName;
    [SerializeField] private String bulletTagName;
    [SerializeField] private String downStairsTIleName;

    [System.Serializable]
    public class BulletControlVariable
    {
        public float timeBeforeSeekTarget;
        public float hoverTime;
        public float initialSpeed;
        public float gSpeed;
        public float slerpSpeed;
        public Vector2 rotation;
        public bool startRotation = false;
        public float damage;
        public bool homing;
		public bool bounceOn = false;
    }

    public BulletControlVariable bulletControlVariable = new BulletControlVariable();

    private float localSpeed;
    public GameObject player;
    private Rigidbody2D rb;

    public GameObject parent;

    public void setSlerpSpeed(float slerpSpeed)
    {
        bulletControlVariable.slerpSpeed = slerpSpeed;
    }

    public void AddCircleCollider2D()
    {
        gameObject.AddComponent<CircleCollider2D>();
        gameObject.GetComponent<CircleCollider2D>().isTrigger = true;

    }

    // Use this for initialization
    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();

        localSpeed = bulletControlVariable.gSpeed;
        player = GameObject.FindGameObjectWithTag(playerTagName);
        StartCoroutine(MainEnumerator());
    }

    public IEnumerator MainEnumerator()
    {
        localSpeed = bulletControlVariable.initialSpeed;
        //Debug.Log("initial start");
        yield return new WaitForSeconds(bulletControlVariable.timeBeforeSeekTarget);

        if (bulletControlVariable.homing)
        {
            localSpeed = 0;

            // rotate to face player ??????????????????????
            bulletControlVariable.rotation = new Vector2
            (
                player.transform.position.x - transform.position.x,
                player.transform.position.y - transform.position.y
            );

            bulletControlVariable.startRotation = true;

            //Debug.Log("float time");
            yield return new WaitForSeconds(bulletControlVariable.hoverTime);
        }

        localSpeed = bulletControlVariable.gSpeed;

        //Debug.Log("end couroutine");
    }

    public void SetTimeBeforeSeek(float value)
    {
        bulletControlVariable.timeBeforeSeekTarget = value;
    }

    void Update()
    {
        if (bulletControlVariable.startRotation)
        {
            transform.up = Vector3.Slerp(transform.up, bulletControlVariable.rotation, Time.deltaTime * bulletControlVariable.slerpSpeed);
        }

        rb.velocity = (transform.up * localSpeed);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject == parent)
            return;
        
        if (other.gameObject.CompareTag(bulletTagName))
            return;

        if (other.gameObject.CompareTag(downStairsTIleName))
            return;
        
        if (other.gameObject.CompareTag(playerTagName))
        {
            // hit player
            player.GetComponent<PlayerStats>().UpdateHealth(-bulletControlVariable.damage);
			///////////////////////////////////////////////////////////////////////////////////////////////
	        Destroy(gameObject);

			return;
		}

		if (bulletControlVariable.bounceOn)
		{
			localSpeed *= -1;
		}
		else
		{
			////////////////////////////////////////////////////////////////////////////////////////////////
			Destroy(gameObject);
		}

    }

	
}
