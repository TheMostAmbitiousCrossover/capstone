﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrencyDropScript : MonoBehaviour
{
	[System.Serializable]
    public class CurrencyDetails
    {
	    public int currencyAmount;
	    public string currencyName;
	    public GameObject currencyGameObject;
    }

    //public List<CurrencyDetails> currencyList = new List<CurrencyDetails>();
	public CurrencyDetails currencyDetails = new CurrencyDetails();

	public bool startExecution = false;

	private void SpawningCurrency()
	{
		// spawn the coins

	}

	private void FixedUpdate()
	{
		if (startExecution)
		{
			SpawningCurrency();
		}
	}
}
