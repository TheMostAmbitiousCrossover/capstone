﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class HolderManagerScript : MonoBehaviour
{
	[TabGroup("Beam Details")]
	public BossBScript.BeamDetails beamDetails;

	[TabGroup("Spiral Details")]
	public BossBScript.BulletHellSpiralDetails bulletHellSpiralDetails;

	[TabGroup("References")]
	public BossBScript parent;
	public  float myRotationZ;
	public float reverseSpeed = 1;

	private void Start()
	{
		parent = GetComponentInParent<BossBScript>();
		beamDetails = parent.beamDetails;
		bulletHellSpiralDetails = parent.bulletHellSpiralDetails;
	}

	public void BeamExecution(bool startBeamPhase, bool crossOn)
	{
		beamDetails.startBeamPhase = startBeamPhase;
		beamDetails.crossOn = crossOn;

		myRotationZ = beamDetails.crossOn ? 45 : 0;

		transform.eulerAngles = new Vector3(0, 0, myRotationZ);

		Vector3 current = Vector3.zero;
		beamDetails.beamPrefab.GetComponent<BeamScript>().damageOn = false;

		for (int i = 0; i < 4; i++)
		{
			GameObject beamGameObject = Instantiate(beamDetails.beamPrefab, transform.position, Quaternion.Euler(current));

			beamGameObject.transform.parent = gameObject.transform;
			current.z += 90;
		}

		current = Vector3.zero;
		beamDetails.beamPrefab.GetComponent<BeamScript>().damageOn = true;

		for (int i = 0; i < 4; i++)
		{
			GameObject beamGameObject = Instantiate(beamDetails.beamPrefab, transform.position, Quaternion.Euler(current));

			beamGameObject.transform.parent = gameObject.transform;
			//beamGameObject.GetComponent<LineRenderer>().enabled = false;
			current.z += 90;

			BeamScript beamScript = beamGameObject.GetComponent<BeamScript>();

			beamScript.bossBStats = parent.bossBStats;
			beamScript.parent = parent;
		}


	}

	private void BeamPhase()
	{
		//transform.rotation = Quaternion.Slerp(transform.rotation,Quaternion.Euler(0, 0, targetZ), Time.deltaTime * rotationSpeed);
		transform.Rotate(Vector3.forward * Time.deltaTime * beamDetails.rotationSpeed * reverseSpeed, Space.World);

		if (transform.eulerAngles.z >= beamDetails.maxAngle)
		{
			if (beamDetails.reverseOn)
			{
				reverseSpeed = -1;
			}
			else
			{
				parent.flags.stopBeam = true;

				transform.eulerAngles = new Vector3(0, 0, myRotationZ);

				foreach (Transform child in transform)
				{
					GameObject.Destroy(child.gameObject);
				}
			}
		}


		if (transform.eulerAngles.z < myRotationZ)
		{
			parent.flags.stopBeam = true;

			transform.eulerAngles = new Vector3(0, 0, myRotationZ);
			reverseSpeed = 1;

			foreach (Transform child in transform)
			{
				GameObject.Destroy(child.gameObject);
			}
		}


	}

	public void BulletPhase()
	{
		if (parent.bulletHellSpiralDetails.reverseOn)
			reverseSpeed = -1;
		else
		{
			reverseSpeed = 1;
		}

		transform.Rotate(Vector3.forward * Time.deltaTime * bulletHellSpiralDetails.rotationSpeed * reverseSpeed, Space.World);
	}

	public IEnumerator RotatingBulletHellEnumerator()
	{
		float angleForEachShots = bulletHellSpiralDetails.degreeToCover / (float) bulletHellSpiralDetails.numberofBulletPerBarrage;


		for (int i = 0; i < bulletHellSpiralDetails.numberOfBarrages; i++)
		{
			Vector3 current = transform.eulerAngles;

			for (int x = 0; x < bulletHellSpiralDetails.numberofBulletPerBarrage; x++)
			{
				GameObject bullet = Instantiate(bulletHellSpiralDetails.bulletPrefab, transform.position, Quaternion.Euler(current));
				current.z += angleForEachShots;

				bullet.transform.parent = parent.transform;

				BulletPartialHomingScript bulletScript = bullet.GetComponent<BulletPartialHomingScript>();

				bulletScript.parent = parent.gameObject;
				bulletScript.bulletControlVariable.gSpeed = bulletHellSpiralDetails.bulletSpeed;
				bulletScript.bulletControlVariable.homing = false;
				bulletScript.AddCircleCollider2D();
				bulletScript.bulletControlVariable.damage = parent.bossBStats.EnemyDamage;
			}
			yield return new WaitForSeconds(bulletHellSpiralDetails.delayBetweenBarrages);

		}

		parent.flags.counterForSpiral++;

		if (parent.flags.counterForSpiral >= parent.bulletHellSpiralDetails.amountOfTimesForSpiral)
		{
			if (Random.Range(1, 3) == 1)
			{
				parent.flags.breakPointBridge = false;
				parent.flags.counterForBeam = 0;
				parent.bulletHellSpiralDetails.reverseOn = false;
				reverseSpeed = 1;
			}
		}

		if (Random.Range(1, 3) == 1)
			parent.bulletHellSpiralDetails.reverseOn = !parent.bulletHellSpiralDetails.reverseOn;

		parent.flags.inSpiralShot = false;
		parent.flags.bulletHellSpiralOn = false;
		

		
	}

	public IEnumerator Test()
	{
		float angleForEachShots = bulletHellSpiralDetails.degreeToCover / (float) bulletHellSpiralDetails.numberofBulletPerBarrage;

		for (int i = 0; i < bulletHellSpiralDetails.numberOfBarrages; i++)
		{
			Vector3 current = transform.eulerAngles;

			for (int x = 0; x < bulletHellSpiralDetails.numberofBulletPerBarrage; x++)
			{
				Transform bulletTransform = parent.bulletHolder.transform.GetChild(0);
				//bulletTransform.eulerAngles = current;

				GameObject bullet = bulletTransform.gameObject;

				//GameObject bullet = Instantiate(bulletHellSpiralDetails.bulletPrefab, transform.position, Quaternion.Euler(current));
				current.z += angleForEachShots;

				bullet.SetActive(true);
				
				//bullet.transform.parent = parent.transform;

				SpawnableBulletScript bulletScript = bullet.GetComponent<SpawnableBulletScript>();

				bulletScript.parent = this.gameObject;
				bulletScript.bulletControlVariable.gSpeed = bulletHellSpiralDetails.bulletSpeed;
				bulletScript.bulletControlVariable.homing = false;
				bulletScript.AddCircleCollider2D();
			}
			yield return new WaitForSeconds(bulletHellSpiralDetails.delayBetweenBarrages);

		}

		parent.flags.inSpiralShot = false;
		parent.flags.bulletHellSpiralOn = false;
	}

	private void FixedUpdate()
	{
		if (beamDetails.startBeamPhase)
		{
			BeamPhase();
		}


	}

}
