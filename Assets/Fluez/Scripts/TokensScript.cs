﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TokensScript : MonoBehaviour
{
	[System.Serializable]
	public class TokenShotDetails
	{
		public float shotsCooldown;
		public float maxAngleDifferential;
		public float minAngleDifferential;
		public float delayBetweenSeries;
		public int numberOfSeries;
		public int numberOfShots;
		public float bulletSpeed;
		public float frontRadius = 360f;
	}
	public TokenShotDetails tokenShotDetails = new TokenShotDetails();

	public GameObject bulletGameObject;

	[System.Serializable]
	public class Flags
	{
		public bool inShots;
		public bool shotOn;
	}
	public Flags flags = new Flags();

	private void FixedUpdate()
	{
		if (flags.shotOn && !flags.inShots)
		{
			flags.inShots = true;
			StartCoroutine(StartTokenShotEnumerator());

		}
	}
	public IEnumerator StartTokenShotEnumerator()
	{
		int total = tokenShotDetails.numberOfShots;


		float angle = tokenShotDetails.frontRadius / (float) total;

		Vector3 current = transform.eulerAngles;
		current.z -= tokenShotDetails.frontRadius / 2;
		
		for (int i = 0; i < tokenShotDetails.numberOfSeries; i++)
		{
			current.z += angle / (float) Random.Range(tokenShotDetails.maxAngleDifferential, tokenShotDetails.maxAngleDifferential);

			for (int x = 0; x < tokenShotDetails.numberOfShots; x++)
			{
				GameObject bullet = Instantiate(bulletGameObject, transform.position, Quaternion.Euler(current));
				current.z += angle;
				//bullet.transform.eulerAngles = new Vector3(0, 0, bullet.transform.eulerAngles.z);
				bullet.transform.parent = gameObject.transform;
				// ^instantiate and move to make it a child of the parent

				BulletPartialHomingScript bulletScript = bullet.GetComponent<BulletPartialHomingScript>();

				bulletScript.parent = this.gameObject;
				bulletScript.bulletControlVariable.gSpeed = tokenShotDetails.bulletSpeed;
				bulletScript.bulletControlVariable.homing = false;
				bulletScript.AddCircleCollider2D();
			}
			yield return new WaitForSeconds(tokenShotDetails.shotsCooldown);

		}

		yield return new WaitForSeconds(tokenShotDetails.delayBetweenSeries);
		flags.inShots = false;
	}
}
