﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class CuipScript : MonoBehaviour
{
    [SerializeField] private string playerTagName;
    [SerializeField] private LayerMask wallLayerMask;

    [System.Serializable]
    public class CuipEnemyStats
    {
        public float speed;
        public Rigidbody2D rb;
        public float numOfBullets;
        public float aggroRange;
        public float stopRange;
    }

	[TabGroup("Cuip Details")]
    public CuipEnemyStats cuipEnemyStats = new CuipEnemyStats();

	[System.Serializable]
    public class ControlVariables
    {
        public bool inSeries = false;
        public float cooldownBetweenBullets;
        public float delayBetweenSeries;
        public float delayBeforeSeries;
        public float slerpSpeed;
        public Vector2 rotation;
        public bool homingOn = false;
        public bool facingWall = false;
    }

	[TabGroup("Control Variables")]
    public ControlVariables controlVariables = new ControlVariables();

	[TabGroup("References")]
	public GameObject player;
	[TabGroup("References")]
	public GameObject bullet;
	private float drag = 1000.0f;
	[TabGroup("References")]
    public Stats cuipStats;

	// Use this for initialization
	void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        cuipStats = GetComponent<Stats>();
        cuipEnemyStats.rb = GetComponent<Rigidbody2D>();
        cuipEnemyStats.rb.drag = drag;
    }

	void FixedUpdate()
	{
		Vector3 directionVector3 = player.transform.position - transform.position;

		if (directionVector3.magnitude < cuipEnemyStats.aggroRange)
		{
			CheckInFront();

			// rotate towars player
			controlVariables.rotation = new Vector2
			(
				player.transform.position.x - transform.position.x,
				player.transform.position.y - transform.position.y
			);

			transform.up = Vector3.Slerp(transform.up, controlVariables.rotation, Time.deltaTime * controlVariables.slerpSpeed);

			if (controlVariables.facingWall)
			{
				return; 
			}


			if (directionVector3.magnitude <= cuipEnemyStats.stopRange)
			{
				cuipEnemyStats.rb.AddForce(transform.up * cuipEnemyStats.speed * drag * -1);
			}
			else
			{
				cuipEnemyStats.rb.AddForce(transform.up * cuipEnemyStats.speed * drag);
			}

			ControlSpawningEnumerator();
		}

	}

	public void ControlSpawningEnumerator()
    {
        if (controlVariables.inSeries)
            return;

        controlVariables.inSeries = true;
        StartCoroutine(EnumeratorTesting());

    }

	public IEnumerator EnumeratorTesting()
    {
        yield return new WaitForSeconds(controlVariables.delayBeforeSeries);
        //Debug.Log("enumerator...");

        for (float i = 0; i < cuipEnemyStats.numOfBullets / 100; i+=Time.deltaTime)
        {
            GameObject bulet = Instantiate(bullet, transform.position, Random.rotation) as GameObject;
            bulet.transform.eulerAngles = new Vector3(0, 0, bulet.transform.eulerAngles.z);

	        bulet.transform.parent = transform.parent.transform;


			BulletPartialHomingScript bulletScript = bulet.GetComponent<BulletPartialHomingScript>();

            bulletScript.parent = this.gameObject;
            bulletScript.setSlerpSpeed(controlVariables.slerpSpeed);
            bulletScript.bulletControlVariable.homing = controlVariables.homingOn;
            bulletScript.AddCircleCollider2D();
            bulletScript.bulletControlVariable.damage = cuipStats.EnemyDamage;

			yield return new WaitForSeconds(controlVariables.cooldownBetweenBullets);
        }

        yield return new WaitForSeconds(controlVariables.delayBetweenSeries);
        controlVariables.inSeries = false;

        //Debug.Log("end couroutine");
    }

	private void CheckInFront()
    {
        Debug.DrawLine(transform.position, player.transform.position);

        controlVariables.facingWall = Physics2D.Linecast(transform.position, player.transform.position, wallLayerMask);

    }

	// health system and whatnot

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag(playerTagName))
        {
            DoDamage();
        }
    }


    public void DoDamage()
    {
        player.GetComponent<PlayerStats>().UpdateHealth(-cuipStats.EnemyDamage);
    }
}
