﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class ItemDropScript : MonoBehaviour
{
	[TabGroup("References")] public ItemDatabase itemDatabase;
	[TabGroup("References")] public Item itemRef;
	[TabGroup("References")] public GameObject itemSpawnGameObject;


	[System.Serializable]
    public class ItemDetails
    {
        public string itemName;
	    public Item.ItemType itemType;
        public int itemDropRarity;
    }
	[TabGroup("Item List")]
    public List<ItemDetails> itemList = new List<ItemDetails>();

    public float dropRate;

    private void Start()
    {
	    itemDatabase = FindObjectOfType<ItemDatabase>();
    }

    private void CalculateSpawning()
    {
        int itemTotal = 0;

        foreach (ItemDetails i in itemList)
        {
            itemTotal += i.itemDropRarity;
        }

        int randomValue = Random.Range(0, itemTotal);

        foreach (ItemDetails i in itemList)
        {
            if (randomValue <= i.itemDropRarity)
            {
	            itemRef = itemDatabase.GetItem(i.itemType, i.itemName);

	            GameObject localItem = Instantiate(itemSpawnGameObject, transform.position, transform.rotation);

				localItem.transform.localScale = new Vector3(0.25f, 0.25f, 0.25f);

				localItem.transform.parent = transform.parent.transform;

				localItem.GetComponent<ItemContainerScript>().SetItem(itemRef);

	            //localItem.GetComponent<SpriteRenderer>().sprite = itemRef.image;

				Debug.Log(i.itemName);
                return;
            }

            randomValue -= i.itemDropRarity;
        }
    }

	private void OnDestroy()
	{
		int calcDropChance = Random.Range(0, 101);

		if (calcDropChance > dropRate)
		{
			Debug.Log("no Drop.... :D");
		}
		else
		{

			Debug.Log("Dropped something heh ...");
			CalculateSpawning();
		}
	}

	private void ReCalculateRarity()
    {
        int counter = itemList[0].itemDropRarity;
        for (int i = 0; i < itemList.Count; i++)
        {
            if (i != 0)
            {
                counter += itemList[i].itemDropRarity;
            }

            itemList[i].itemDropRarity = counter;
        }
    }
}
