﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class ArcShotScript : MonoBehaviour
{
	[System.Serializable]
	public class ArcEnemyDetails
	{
		public float speed;
		public Rigidbody2D rb;
		public float aggroRange;
		public float stopRange;
		public Vector2 rotation;
		public float rotationSlerpSpeed;
		public Vector3 directionVector3;
	}
	[TabGroup("Arc Enemy Details")]
	public ArcEnemyDetails arcEnemyDetails = new ArcEnemyDetails();
	
	[System.Serializable]
	public class ArcShotController
	{
		public float shotsCooldown;
		public float delayBetweenSeries;
		public int numberOfSeries;
		public int numberOfShots;
		public float bulletSpeed;
		public float frontRadius = 90f;
		public float waitTimeBeforeShot;
	}
	[TabGroup("Arc Shot Controller")]
	public ArcShotController arcShotController = new ArcShotController();

	[System.Serializable]
	public class Flags
	{
		public bool inRange;
		public bool wallBetween;
		public bool inSeries;
	}

	[TabGroup("Flags")]
	public Flags flags = new Flags();

	[TabGroup("References")] public GameObject player;
	[TabGroup("References")] public GameObject enemyHolder;
	[TabGroup("References")] public GameObject bullet;
	[TabGroup("References")] public Stats arcEnemyStats;
	public string playerTagName;
	public LayerMask wallLayerMask;
	
	// Use this for initialization
	void Start ()
	{
		arcEnemyStats = GetComponent<Stats>();

		if (arcEnemyStats.EnemyDamage <= 0f)
		{
			arcEnemyStats.InitializeStats();
		}

		player = GameObject.FindGameObjectWithTag(playerTagName);
		arcEnemyDetails.rb = GetComponent<Rigidbody2D>();
	}

	private void FixedUpdate ()
	{
		CheckAggroRange();

		if (!flags.inRange) return;
		CheckInFront();

		if (flags.wallBetween)
		{
			arcEnemyDetails.rb.velocity = new Vector2(0f, 0f);
			return;
		}

		Movement();
		ShotController();
	}

	public void CheckAggroRange()
	{
		arcEnemyDetails.directionVector3 = player.transform.position - transform.position;

		if (arcEnemyDetails.directionVector3.magnitude < arcEnemyDetails.aggroRange)
		{
			flags.inRange = true;
			return;
		}

		flags.inRange = false;
	}

	public void CheckInFront()
	{
		Debug.DrawLine(transform.position, player.transform.position);

		flags.wallBetween = Physics2D.Linecast(transform.position, player.transform.position, wallLayerMask);
	}

	public void Movement()
	{
		// rotate to face player
		arcEnemyDetails.rotation = new Vector2
		(
			player.transform.position.x - transform.position.x,
			player.transform.position.y - transform.position.y
		);

		transform.up = Vector3.Slerp(transform.up, arcEnemyDetails.rotation,
			Time.deltaTime * arcEnemyDetails.rotationSlerpSpeed);


		// moveing the enemy
		if (arcEnemyDetails.directionVector3.magnitude <= arcEnemyDetails.stopRange)
		{
			// too close reverse :D
			arcEnemyDetails.rb.velocity = transform.up * arcEnemyDetails.speed * -1;
		}
		else
		{
			arcEnemyDetails.rb.velocity = transform.up * arcEnemyDetails.speed;
		}
	}

	public void ShotController()
	{
		if (flags.inSeries)
			return;

		flags.inSeries = true;
		StartCoroutine(ArcShotCouroutine());
	}

	public IEnumerator ArcShotCouroutine()
	{
		yield return new WaitForSeconds(arcShotController.waitTimeBeforeShot);

		float angle = arcShotController.frontRadius / arcShotController.numberOfShots;

		for (int i = 0; i < arcShotController.numberOfSeries; i++)
		{
			Vector3 current = transform.eulerAngles;
			current.z -= arcShotController.frontRadius / 2;

			for (int x = 0; x < arcShotController.numberOfShots; x++)
			{
				GameObject bulletLocal = Instantiate(bullet, transform.position, Quaternion.Euler(current));
				current.z += angle;

				bulletLocal.transform.parent = enemyHolder.transform;

				BulletPartialHomingScript bulletScript = bulletLocal.GetComponent<BulletPartialHomingScript>();
				bulletScript.parent = this.gameObject;
				bulletScript.bulletControlVariable.gSpeed = arcShotController.bulletSpeed;
				bulletScript.bulletControlVariable.homing = false;
				bulletScript.bulletControlVariable.damage = arcEnemyStats.EnemyDamage;
				bulletScript.AddCircleCollider2D();
			}
			yield return new WaitForSeconds(arcShotController.shotsCooldown);
		}

		yield return new WaitForSeconds(arcShotController.delayBetweenSeries);
		flags.inSeries = false;
	}

	private void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.CompareTag(playerTagName))
			DoDamage();
	}

	public void DoDamage()
	{
		player.GetComponent<PlayerStats>().UpdateHealth(-arcEnemyStats.EnemyDamage);
	}

}
