﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotManagerScript : MonoBehaviour
{

	public GameObject parent;
	public BossBScript bossBScript;

	public void QuadShots()
	{
		// rotate towars playerGameObject
		bossBScript.quadShotDetails.rotation = new Vector2
		(
			bossBScript.playerGameObject.transform.position.x - transform.position.x,
			bossBScript.playerGameObject.transform.position.y - transform.position.y
		);

		transform.up = Vector3.Slerp(transform.up, bossBScript.quadShotDetails.rotation,
			Time.deltaTime * bossBScript.quadShotDetails.slerpRotationSpeed);
	}

	public IEnumerator StartQuadShot()
	{
		int total = bossBScript.quadShotDetails.numberOfShots;


		float angle = bossBScript.quadShotDetails.frontRadius / (float) total;

		//current.z -= frontRadius / 2;

		for (int i = 0; i < bossBScript.quadShotDetails.numberOfSeries; i++)
		{
			Vector3 current = transform.eulerAngles;
			current.z -= bossBScript.quadShotDetails.frontRadius / 2;

			for (int x = 0; x < bossBScript.quadShotDetails.numberOfShots; x++)
			{
				GameObject bullet = Instantiate(bossBScript.bulletGameObject, transform.position, Quaternion.Euler(current));
				current.z += angle;
				//bullet.transform.eulerAngles = new Vector3(0, 0, bullet.transform.eulerAngles.z);
				bullet.transform.parent = parent.transform;
				// ^instantiate and move to make it a child of the parent

				BulletPartialHomingScript bulletScript = bullet.GetComponent<BulletPartialHomingScript>();

				bulletScript.parent = parent.gameObject;
				bulletScript.bulletControlVariable.gSpeed = bossBScript.quadShotDetails.bulletSpeed;
				bulletScript.bulletControlVariable.homing = false;
				bulletScript.bulletControlVariable.damage = bossBScript.bossBStats.EnemyDamage;
				bulletScript.AddCircleCollider2D();
			}
			yield return new WaitForSeconds(bossBScript.quadShotDetails.shotsCooldown);

		}

		yield return new WaitForSeconds(bossBScript.quadShotDetails.delayBetweenSeries);
		bossBScript.flags.inShots = false;
		bossBScript.flags.quadShotsOn = false;
	}
}
