﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Permissions;
using UnityEngine;

public class SpiderAIScript : MonoBehaviour
{
    
    [System.Serializable]
    public class SpiderDetails
    {
        public LayerMask playerLayerMask;
        public float aggroDistance;
        public float doDamageDistance;
        public float Damage;
        public float stopDistance;
        public float reverseRange;
        public SphereCollider hitBoxSphereCollider;
        public Rigidbody2D rb;

    }
    
    public SpiderDetails spiderDetails = new SpiderDetails();
    public GameObject player;

    private class Flags
    {
        public Boolean damagedPlayer = false;
        public Boolean doDamage = false;
    }

    private Flags flags = new Flags();

    /*
     * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
     * temporary stats
     */
    public float speed = 1;
    private const float drag = 1000.0f;

    /*
     * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
     */

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        spiderDetails.rb = GetComponent<Rigidbody2D>();
        spiderDetails.rb.drag = drag;
    }

    void FixedUpdate()
    {
        if (!flags.damagedPlayer)
            AggroCheck();
        else
            ReverseMove();


    }

    private void AggroCheck()
    {
        //float distancefromPlayer = Vector2.Distance(playerGameObject.transform.position, transform.position);

        Vector3 directionVector2 = player.transform.position - transform.position;

        if (directionVector2.magnitude < spiderDetails.aggroDistance && directionVector2.magnitude > spiderDetails.doDamageDistance)
        {

            //Vector3 forward = transform.forward;
            //Vector3 localTarget = transform.InverseTransformPoint(playerGameObject.transform.position);

            Vector3 direction = player.transform.position - transform.position;
            Quaternion qRotation = Quaternion.LookRotation(direction);
            transform.rotation = Quaternion.Lerp(transform.rotation, qRotation, spiderDetails.rb.rotation * Time.deltaTime);



            spiderDetails.rb.AddForce(directionVector2.normalized * speed * drag);
        }

        //if (distancefromPlayer <= spiderDetails.doDamageDistance)
        //{
        //    DoDamage();
        //}
    }

    private void DoDamage()
    {
        // get the health from playerGameObject and decrease it 

        // maybe does overtime damage?

        flags.damagedPlayer = true;
    }

    private void ReverseMove()
    {

    }

    public void DecreaseCurrentHealth(float damage)
    {
        // call dictionary.....??
    }

    public void CheckHealth()
    {

    }

    public void DamageTaken(WeaponScript.Weapon weapon)
    {
        float roundedDamage = Mathf.Round(weapon.weaponDamage * weapon.weaponChargeMod * 100f) / 100f;
        DecreaseCurrentHealth(roundedDamage);
        CheckHealth();

    }

    public void DoDamage(float damagetoplayer)
    {

    }
}
