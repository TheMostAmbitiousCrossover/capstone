﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class OnDeathSpawnScript : MonoBehaviour
{
	[TabGroup("References")] public GameObject itemSpawnManager;
	[TabGroup("References")] public GameObject currencySpawnManager;


	private void OnDestroy()
	{
		// spawn the item drop
		Debug.Log("you dead");
		GameObject item = Instantiate(itemSpawnManager, transform);
		item.transform.parent = transform.parent.transform;

	}
}
