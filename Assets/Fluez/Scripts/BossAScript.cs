﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

public class BossAScript : MonoBehaviour
{
	[TabGroup("References")]
	public GameObject healthBar;
	[TabGroup("References")]
	public Stats bossAStats;
	[SerializeField] private string playerTagName;

	[System.Serializable]
	public class Flags
	{
		public bool inStage1 = false;
		public bool inStage2 = false;
		public bool stage1On = false;
		public bool stage2On = false;
		public float breakPoint;
		public float healthRatioUI;
	}

	[TabGroup("Flags")]
	public Flags flags = new Flags();

	[System.Serializable]
	public class Stage1Details
	{
		public float delayBetweenBarrages;
		public float delayBeforeNextWave;
		public float delayBeforeStartWave;

		public int numberOfBarrages;
		public int numberofBulletPerBarrage;

		public float bulletSlerpSpeed;
		public float bulletSpeed;
		public bool homingOn = false;
		public Vector2 rotation;
	}

	[TabGroup("Stage 1")]
	public Stage1Details stage1Details = new Stage1Details();

	[System.Serializable]
	public class Stage2Details
	{
		public float delayBetweenBarrages;
		public float delayBeforeNextWave;
		public float delayBeforeStartWave;

		public int numberOfBarrages;
		public int numberofBulletPerBarrage;

		public float timeBeforeSeek;
		public float defaultTimeBeforeSeek;
		public float initialSpeed;
		public float defaultInitialSpeed;

		public float bulletSlerpSpeed;
		public float bulletSpeed;
		public bool homingOn = true;
		public Vector2 rotation;
		[Range(0.1f, 1.5f)] public float scale;
		public Color color;
	}

	[TabGroup("Stage 2")]
	public Stage2Details stage2Details = new Stage2Details();

	[TabGroup("References")]
	public GameObject player;
	[TabGroup("References")]
	public GameObject bullet;

	private void Start()
	{
		//bossAStats = GetComponent<Stats>();

		if (bossAStats.MaxHealth <= 0f)
		{
			bossAStats.InitializeStats();
		}

		player = GameObject.FindGameObjectWithTag(playerTagName);


		flags.healthRatioUI = bossAStats.CurrentHealth / bossAStats.MaxHealth;

		// settign the default for the bullet prefab
		stage2Details.defaultTimeBeforeSeek = bullet.GetComponent<BulletPartialHomingScript>()
			.bulletControlVariable.timeBeforeSeekTarget;

		stage2Details.defaultInitialSpeed = bullet.GetComponent<BulletPartialHomingScript>()
			.bulletControlVariable.initialSpeed;

		// ui 
		healthBar = GameObject.Find("bar");
		healthBar.GetComponent<Image>().enabled = true;
		healthBar.GetComponentInChildren<Transform>().GetChild(0).GetComponent<Image>().enabled = true;
		healthBar.GetComponentInChildren<Transform>().GetChild(1).GetComponent<Image>().enabled = true;
	}

	public void StageController()
	{
		flags.breakPoint = bossAStats.MaxHealth / 2f;

		if (bossAStats.CurrentHealth <= flags.breakPoint)
		{
			flags.stage2On = true;
			flags.stage1On = true;
		}
		else
		{
			flags.stage1On = true;
			flags.stage2On = false;
		}
	}

	private void FixedUpdate()
	{
		StageController();

		if (flags.stage1On)
		{
			if (!flags.inStage1)
			{
				flags.inStage1 = true;
				StartCoroutine(Stage1Couroutine());
			}
		}
		
		if (flags.stage2On)
		{
			if (!flags.inStage2)
			{
				flags.inStage2 = true;
				StartCoroutine(Stage2Couroutine());
			}
		}

		UpdateUIHealthBar();
	}


	public IEnumerator Stage1Couroutine()
	{
		// the initial delay before start initialising the bulletGameObject
		yield return new WaitForSecondsRealtime(stage1Details.delayBeforeStartWave);

		int total = stage1Details.numberofBulletPerBarrage;

		float angle = 360f / (float) total;
		Vector3 current = Vector3.zero;

		// loop for the number of barrages per wave is there
		for (int i = 0; i < stage1Details.numberOfBarrages; i++)
		{
			current.z += angle / (float) Random.Range(2, 5);
			// the number of bulletGameObject per barrage
			for (int x = 0; x < stage1Details.numberofBulletPerBarrage; x++)
			{
				GameObject bulet = Instantiate(bullet, transform.position, Quaternion.Euler(current));
				current.z += angle;
				bulet.transform.eulerAngles = new Vector3(0, 0, bulet.transform.eulerAngles.z);
				bulet.transform.parent = gameObject.transform;
				BulletPartialHomingScript bulletScipt = bulet.GetComponent<BulletPartialHomingScript>();

				// changing the details in each bulletGameObject
				bulletScipt.parent = this.gameObject;
				bulletScipt.setSlerpSpeed(stage1Details.bulletSlerpSpeed);
				bulletScipt.bulletControlVariable.homing = stage1Details.homingOn;
				bulletScipt.bulletControlVariable.gSpeed = stage1Details.bulletSpeed;
				bulletScipt.AddCircleCollider2D();
				bulletScipt.bulletControlVariable.damage = bossAStats.EnemyDamage;

			}
			// the delay between each set of barrages
			yield return new WaitForSecondsRealtime(stage1Details.delayBetweenBarrages);
		}

		// the reload time or cooldown before the couroutine can be called again
		yield return new WaitForSecondsRealtime(stage1Details.delayBeforeNextWave);
		flags.inStage1 = false;

	}

	public IEnumerator Stage2Couroutine()
	{

		yield return new WaitForSecondsRealtime(stage2Details.delayBeforeStartWave);

		// loop for the number of barrages per wave is there
		for (int i = 0; i < stage2Details.numberOfBarrages; i++)
		{
			// the number of bulletGameObject per barrage
			for (int x = 0; x < stage2Details.numberofBulletPerBarrage; x++)
			{
				// settings intial speed
				bullet.GetComponent<BulletPartialHomingScript>().SetTimeBeforeSeek(stage2Details.timeBeforeSeek);

				bullet.GetComponent<BulletPartialHomingScript>().bulletControlVariable.initialSpeed =
					stage2Details.initialSpeed;



				GameObject bulet = Instantiate(bullet, transform.position, Random.rotation) as GameObject;
				bulet.transform.parent = gameObject.transform;

				bulet.transform.eulerAngles = new Vector3(0, 0, bulet.transform.eulerAngles.z);
				bulet.transform.localScale = new Vector3(stage2Details.scale, stage2Details.scale, stage2Details.scale);
				bulet.GetComponentInChildren<SpriteRenderer>().color = stage2Details.color;

				BulletPartialHomingScript bulletScipt = bulet.GetComponent<BulletPartialHomingScript>();

				// changing the details in each bulletGameObject
				bulletScipt.parent = this.gameObject;
				bulletScipt.SetTimeBeforeSeek(stage2Details.timeBeforeSeek);
				bulletScipt.setSlerpSpeed(stage2Details.bulletSlerpSpeed);
				bulletScipt.bulletControlVariable.homing = stage2Details.homingOn;
				bulletScipt.bulletControlVariable.gSpeed = stage2Details.bulletSpeed;
				bulletScipt.AddCircleCollider2D();
				bulletScipt.bulletControlVariable.damage = bossAStats.EnemyDamage;

			}
			// the delay between each set of barrages
			yield return new WaitForSecondsRealtime(stage2Details.delayBetweenBarrages);
		}

		// the reload time or cooldown before the couroutine can be called again
		yield return new WaitForSecondsRealtime(stage2Details.delayBeforeNextWave);
		flags.inStage2 = false;
	}

	public void ResetBulletPrefab()
	{
		bullet.GetComponent<BulletPartialHomingScript>().SetTimeBeforeSeek(stage2Details.defaultTimeBeforeSeek);
	}

	private void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.CompareTag(playerTagName))
		{
			DoDamage();
		}
	}

	public void UpdateUIHealthBar()
	{
		float tempHealthRatio = bossAStats.CurrentHealth / bossAStats.MaxHealth;

		if (tempHealthRatio != flags.healthRatioUI)
		{
			flags.healthRatioUI = tempHealthRatio;
			StartCoroutine(BlinkHealthBar(0.2f));
		}

		healthBar.GetComponent<Image>().fillAmount = flags.healthRatioUI;

		if (tempHealthRatio <= 0)
		{
			healthBar.GetComponent<Image>().enabled = (false);
			healthBar.GetComponentInChildren<Transform>().GetChild(0).GetComponent<Image>().enabled = false;
			healthBar.GetComponentInChildren<Transform>().GetChild(1).GetComponent<Image>().enabled = false;
		}
	}

	public IEnumerator BlinkHealthBar(float duration)
	{
		float time = 0;

		while (time < duration)
		{
			healthBar.GetComponent<Image>().color = Color.white;
			time += Time.deltaTime;
			yield return null;
		}

		healthBar.GetComponent<Image>().color = Color.red;
	}

	public void DoDamage()
	{
		player.GetComponent<PlayerStats>().UpdateHealth(-bossAStats.EnemyDamage);
	}
}
