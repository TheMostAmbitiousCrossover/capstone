﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class ChargieScript : MonoBehaviour
{

    [SerializeField] private string playerTagName;
    [SerializeField] private string wallTagName;
    [SerializeField] private LayerMask wallLayerMask;

    [System.Serializable]
    public class ChargieDetails
    {
        public Rigidbody2D rb;
        public float aggroRange;
        public float speed;

    }
	[TabGroup("Chargie Details")]
    public ChargieDetails chargieDetails = new ChargieDetails();

	[System.Serializable]
    public class ControlVariable
    {
        public Vector3 rotation;
        public float slerpSpeed;
        public float chargingTime;
        public Color32 defaultColor;
        public Color32 chargingColor;
        public float stunnedTime;
    }
	[TabGroup("Control Variable")]
    public ControlVariable controlVariable = new ControlVariable();

	[System.Serializable]
    public class Flags
    {
        public bool inCharging;
        public bool stuned;
        //public bool isWall;
        public bool hitPlayer;
        public bool wallInBetween;
    }
	[TabGroup("Flags")]
    public Flags flags = new Flags();
	
	[TabGroup("References")]
	public GameObject player;
	[TabGroup("References")]
	public Stats chargieStats;

	private const float drag = 1000.0f;
    private float localSpeed = 0;


    // Use this for initialization
    void Start()
    {
        chargieStats = GetComponent<Stats>();
        player = GameObject.FindGameObjectWithTag("Player");
        chargieDetails.rb = GetComponent<Rigidbody2D>();
        chargieDetails.rb.drag = drag;
    }

    private void FixedUpdate()
    {

        Vector3 directionVector3 = player.transform.position - transform.position;

        if (directionVector3.magnitude < chargieDetails.aggroRange)
        {
            CheckInFront();

            if (!flags.inCharging && !flags.wallInBetween)
            {
                if (!flags.stuned)
                {
                    flags.inCharging = true;
                    StartCoroutine(CHargingState());
                }
                else
                {
                    StartCoroutine(StunnedTime());
                }

            }

           
        }

        transform.up = Vector3.Slerp(transform.up, controlVariable.rotation,
            Time.deltaTime * controlVariable.slerpSpeed);
        chargieDetails.rb.AddForce(transform.up * localSpeed * drag);
    }

    public IEnumerator CHargingState()
    {
        localSpeed = 0;
        // rotate to face playerGameObject ??????????????????????
        controlVariable.rotation = new Vector2
        (
            player.transform.position.x - transform.position.x,
            player.transform.position.y - transform.position.y
        );

        this.gameObject.GetComponent<SpriteRenderer>().color = controlVariable.chargingColor;

        yield return new WaitForSeconds(controlVariable.chargingTime);

        if (flags.wallInBetween)
        { 
            flags.inCharging = false;
            flags.stuned = false;
            localSpeed = 0;
            yield break;
        }

        flags.inCharging = true;

        this.gameObject.GetComponent<SpriteRenderer>().color = controlVariable.defaultColor;
        localSpeed = chargieDetails.speed;

        yield break;
    }

    private void ResetFlags()
    {
        
    }

    private void CheckInFront()
    {
        Debug.DrawLine(transform.position, player.transform.position);

        flags.wallInBetween = Physics2D.Linecast(transform.position, player.transform.position, wallLayerMask);

    }


    public IEnumerator StunnedTime()
    {
        yield return new WaitForSeconds(controlVariable.stunnedTime);
        flags.stuned = false;
    }



    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag(playerTagName) && !flags.hitPlayer)
        {
            // hit playerGameObject do damage
            DoDamage();
            flags.hitPlayer = true;
        }


        if (other.gameObject.CompareTag(wallTagName))
        {
            // hit 
            localSpeed = 0;
            flags.inCharging = false;
            flags.stuned = true;
            controlVariable.stunnedTime = 3;
            flags.hitPlayer = false;
        }

    }

    public void DoDamage()
    {
        player.GetComponent<PlayerStats>().UpdateHealth(-
	        chargieStats.EnemyDamage);
    }
}