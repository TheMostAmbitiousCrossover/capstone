﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeamScript : MonoBehaviour
{
    private LineRenderer lR;
	// Use this for initialization
    public int range;
    public string wallTagName;
    public string playerTagName;
    public string bulletTagName;
	public LayerMask beamLayerMask;
	public LayerMask beamDamageLayerMask;
	public bool damageOn;
	public bool cooldownOn;
	public float cooldownTime;

	public Stats bossBStats;

	public BossBScript parent;

	void Start ()
	{
	    lR = GetComponent<LineRenderer>();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (damageOn)
		{
			DamageBeam();
		}
		else
		{
			BeamFunction();
		}
		
	}

	private void BeamFunction()
	{
		Vector3 myTransform = transform.position;
		myTransform.z = -1;


		lR.SetPosition(0, myTransform);
		RaycastHit2D hit = Physics2D.Raycast(myTransform, transform.up, range, beamLayerMask);

		Debug.DrawRay(myTransform, transform.up, Color.cyan);

		if (hit.collider)
		{
			Vector3 temp = hit.point;
			temp.z = -1;

			lR.SetPosition(1, temp);
		}

	}

	private void DamageBeam()
	{
		Vector3 myTransform = transform.position;
		//myTransform.z = -1;

		lR.SetPosition(0, myTransform);
		RaycastHit2D hit = Physics2D.Raycast(myTransform, transform.up, range, beamDamageLayerMask);
		
		Debug.DrawRay(myTransform, transform.up, Color.black);
		if (hit.collider)
		{
			if (!cooldownOn)
			{
				//Debug.Log("Hit Player");

				parent.DoDamage();
				cooldownOn = true;
				StartCoroutine(startCooldownEnumerator());
			}


			lR.SetPosition(1, hit.point);
			
		}
		else
		{
			lR.SetPosition(1, hit.point);
		}
	}

	IEnumerator startCooldownEnumerator()
	{
		yield return new WaitForSeconds(cooldownTime);
		cooldownOn = false;
	}
}
