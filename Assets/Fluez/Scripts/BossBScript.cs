﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEditor.Experimental.UIElements;
using UnityEngine;
using UnityEngine.UI;

public class BossBScript : MonoBehaviour
{
	[TabGroup("References")]
	public GameObject healthBar;
	[TabGroup("References")]
	public GameObject bulletGameObject;
	[TabGroup("References")]
    public Stats bossBStats;
	[TabGroup("References")]
	public HolderManagerScript holderManagerScript;
	[TabGroup("References")]
	public ShotManagerScript shotManagerScript;

	public string playerTagName;
	//public BossBVariables bossBVariables = new BossBVariables();

	[System.Serializable]
	public class QuadShotDetails
	{
		public float shotsCooldown;
		public float delayBetweenSeries;
		public int numberOfSeries;
		public int numberOfShots;
		public float bulletSpeed;
		public float frontRadius = 90f;
		public Vector2 rotation;
		public float slerpRotationSpeed;
	}
	[TabGroup("Quad Shot Details")]
	public QuadShotDetails quadShotDetails = new QuadShotDetails();

	[System.Serializable]
	public class BeamDetails
	{
		public bool startBeamPhase;
		public bool crossOn;
		public float crossAngle;
		public float defaultAngle;
		public float maxAngle;
		public float rotationSpeed;
		public float amountOfTimeForBeam;
		public bool reverseOn;
		public GameObject beamPrefab;

	}
	[TabGroup("Beam Details")]
	public BeamDetails beamDetails = new BeamDetails();


	[System.Serializable]
	public class BulletHellSpiralDetails
	{
		public int numberOfBarrages;
		public int numberofBulletPerBarrage;
		public float delayBetweenBarrages;
		public float degreeToCover;
		public float bulletSpeed;
		public bool reverseOn;

		public float rotationSpeed;

		public float amountOfTimesForSpiral;
		public GameObject bulletPrefab;

	}
	[TabGroup("Spiral Details")]
	public BulletHellSpiralDetails bulletHellSpiralDetails = new BulletHellSpiralDetails();


	[System.Serializable]
    public class Flags
    {
	    public bool inBeam;
	    public bool inShots;
	    public bool inSpiralShot;

		public bool stopBeam;

		public bool beamOn;
	    public bool quadShotsOn;
	    public bool bulletHellSpiralOn;

	    public bool breakPointBridge;
	    public float counterForBeam;
	    public float counterForSpiral;
	    public float healthRatioUI;

    }
	[TabGroup("Flags")]
    public Flags flags = new Flags();

	[TabGroup("References")]
	public GameObject playerGameObject;
	[TabGroup("References")]
	public GameObject holderGameObject;

	// not finalized
	public GameObject bulletHolder;
	public float bulletsToSpawn;
	public GameObject bulletToSpawnPrefab;

    // Use this for initialization
    void Start ()
    {
	    bossBStats = GetComponent<Stats>();

	    if (bossBStats.MaxHealth <= 0f)
	    {
		    bossBStats.InitializeStats();
	    }

		playerGameObject = GameObject.FindGameObjectWithTag(playerTagName);
	    flags.healthRatioUI = bossBStats.CurrentHealth / bossBStats.MaxHealth;

		// UI
	    healthBar = GameObject.Find("bar");
	    healthBar.GetComponent<Image>().enabled = true;
	    healthBar.GetComponentInChildren<Transform>().GetChild(0).GetComponent<Image>().enabled = true;
	    healthBar.GetComponentInChildren<Transform>().GetChild(1).GetComponent<Image>().enabled = true;
	}

	public void StageController()
	{
		float breakPoint = bossBStats.MaxHealth / 2;

		if (bossBStats.CurrentHealth <= breakPoint)
		{
			if (!flags.breakPointBridge)
			{
				flags.beamOn = true;
			}
			else
			{
				flags.bulletHellSpiralOn = true;
			}
		}
		else
		{
			flags.quadShotsOn = true;
		}

	}

	// Update is called once per frame
	void FixedUpdate ()
	{
		StageController();

		if (flags.beamOn)
		{
			CrossBeam();
		}

		if (flags.quadShotsOn)
		{
			QuadShot();
		}

		if (flags.bulletHellSpiralOn)
		{
			BulletHellSpiral();
		}

		UpdateUIHealthBar();
	}
	
	public void InitBullets()
	{
		Vector3 current = Vector3.zero;

		for (int i = 0; i < bulletsToSpawn; i++)
		{
			GameObject bulletSpawnPrefab =
				Instantiate(bulletToSpawnPrefab, transform.position, Quaternion.Euler(current));
			bulletSpawnPrefab.transform.parent = bulletHolder.transform;
			bulletSpawnPrefab.SetActive(false);

			SpawnableBulletScript spawnableBulletScript = bulletSpawnPrefab.GetComponent<SpawnableBulletScript>();

			spawnableBulletScript.parent = this.gameObject;
			spawnableBulletScript.bulletControlVariable.homing = false;
			spawnableBulletScript.bulletControlVariable.gSpeed = 0;		
			spawnableBulletScript.AddCircleCollider2D();
			spawnableBulletScript.bulletControlVariable.damage = 0;
		}
	}
	
	public void BulletHellSpiral()
	{
		holderManagerScript.BulletPhase();

		if (flags.inSpiralShot)
			return;

		StartCoroutine(holderManagerScript.RotatingBulletHellEnumerator());
		flags.inSpiralShot = true;

	}

	public void QuadShot()
	{
		shotManagerScript.QuadShots();

		if (flags.inShots)
			return;

		StartCoroutine(shotManagerScript.StartQuadShot());
		flags.inShots = true;
	}

    public void CrossBeam()
    {
	    if (flags.inBeam)
	    {
		    if (!flags.stopBeam) return;

		    holderGameObject.SetActive(false);
		    flags.inBeam = false;
		    flags.beamOn = false;
		    flags.stopBeam = false;
		    flags.counterForBeam++;

		    if (flags.counterForBeam >= beamDetails.amountOfTimeForBeam)
		    {
			    flags.breakPointBridge = true;
			    flags.counterForSpiral = 0;
		    }
				

		    return;
	    }

	    holderGameObject.SetActive(true);
	    holderManagerScript.BeamExecution(true, beamDetails.crossOn);

	    flags.inBeam = true;
	}

	private void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.CompareTag(playerTagName))
		{
			DoDamage();
		}
	}

	public void DoDamage()
	{
		playerGameObject.GetComponent<PlayerStats>().UpdateHealth(-bossBStats.EnemyDamage);
		
	}

	public void UpdateUIHealthBar()
	{
		float tempHealthRatio = bossBStats.CurrentHealth / bossBStats.MaxHealth;

		if (tempHealthRatio < flags.healthRatioUI)
		{
			flags.healthRatioUI = tempHealthRatio;
			StartCoroutine(BlinkHealthBar(0.2f));
		}

		healthBar.GetComponent<Image>().fillAmount = flags.healthRatioUI;

		if (tempHealthRatio <= 0)
		{
			healthBar.GetComponent<Image>().enabled = (false);
			healthBar.GetComponentInChildren<Transform>().GetChild(0).GetComponent<Image>().enabled = false;
			healthBar.GetComponentInChildren<Transform>().GetChild(1).GetComponent<Image>().enabled = false;
		}
	}

	public IEnumerator BlinkHealthBar(float duration)
	{
		float time = 0;

		while (time < duration)
		{
			healthBar.GetComponent<Image>().color = Color.white;
			time += Time.deltaTime;
			yield return null;
		}

		healthBar.GetComponent<Image>().color = Color.red;
	}

}
